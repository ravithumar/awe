@extends('admin.layouts.master')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('viewuser')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
        </div>

        <div class="col-12 text-right">
            <a href="{{ route('admin.user.edit',$user['id']) }}" class="btn btn-secondary btn-sm waves-effect m-l-5">Edit</a>
        </div>

    </div>
    @php
        $default = '/images/default.png';
    @endphp
    <div class="row">

        <div class="col-lg-4 col-xl-4">
            <div class="card-box text-center">
                <img src="{{$user['profile_picture_full_url'] }}" onerror="this.src='{{$default}}'"  class="rounded-circle avatar-lg img-thumbnail"
                    alt="profile-image">

                <div class="text-left mt-3">

                    <p class="text-muted mb-2 font-13"><strong>First Name :</strong> <span class="ml-2">{{$user['first_name']}}</span></p>
                    <p class="text-muted mb-2 font-13"><strong>Last Name :</strong> <span class="ml-2">{{!empty($user['last_name'])?$user['last_name']:''}}</span></p>
                    <p class="text-muted mb-2 font-13"><strong>Phone :</strong><span class="ml-2">{{ !empty($user['phone_formatted'])?$user['phone_formatted']:'' }}</span></p>
                    <p class="text-muted mb-2 font-13"><strong>Email :</strong> <span class="ml-2">{{$user['email']}}</span></p>
                    <p class="text-muted mb-2 font-13"><strong>Referral Code :</strong> <span class="ml-2">{{ !empty($user['reference_code'])?$user['reference_code']:'' }}</span></p>
                    
                    <?php if($user['parent_id']==0) { ?>
                        <p class="text-muted mb-2 font-13"><strong>User Type :</strong> <span class="ml-2"> Parent </span></p>
                    <?php }else{  ?> 
                        <p class="text-muted mb-2 font-13"><strong>User Type :</strong> <span class="ml-2"> Sub User </span></p>
                    <?php } ?>

                    <?php if($user['is_child']==1) { ?>
                        <p class="text-muted mb-2 font-13"><strong>Is Child? :</strong> <span class="ml-2"> Yes </span></p>
                    <?php }else{ ?>
                        <p class="text-muted mb-2 font-13"><strong>Is Child? :</strong> <span class="ml-2"> No </span></p>
                    <?php } ?>
                    
                    <?php if($user['parent_id']!=0) { ?>
                        <p class="text-muted mb-2 font-13"><strong>Parent User :</strong> <span class="ml-2"> 
                            <a href="{{route('admin.user.show', $user['parent']['id'])}}" >{{!empty($user['parent']['first_name'])?$user['parent']['first_name']:''}} {{!empty($user['parent']['last_name'])?$user['parent']['last_name']:''}} </a> </span></p>
                    <?php } ?>

                    <p class="text-muted mb-2 font-13"><strong>Status :</strong> <span class="ml-2">
                        <?php if($user['status']==0){ echo "Pending"; }elseif($user['status']==1){echo "Active"; }else{ echo "Inactive"; }?></span></p>

                    <?php 
                    $user_age_restricted = array();
                        if(!empty($user['useragerestriction']) && count($user['useragerestriction'])>0){
                            foreach ($user['useragerestriction'] as $key => $value) {
                                $user_age_restricted[$key] = "<a target='_blank' href='".route('admin.ratings.show',$value['ageratings']['id'])."'>".$value['ageratings']['name']['en']."</a>";
                            }
                        }
                    ?>
                    <p class="text-muted mb-2 font-13"><strong>User Age Restricted :</strong> <span class="ml-2"><?php echo implode(",",$user_age_restricted); ?></span></p>


                    <?php 
                    $user_age_restricted_content = array();
                        if(!empty($user['userrestrictedcontent']) && count($user['userrestrictedcontent'])>0){
                            foreach ($user['userrestrictedcontent'] as $key => $value) {
                                $user_age_restricted_content[$key] = "<a target='_blank' href='".route('admin.content.show',$value['content']['id'])."'>".$value['content']['name']['en']."</a>";
                            }
                        }
                    ?>
                    <p class="text-muted mb-2 font-13"><strong>User Restricted Content :</strong> <span class="ml-2"><?php echo implode(", ",$user_age_restricted_content); ?></span></p>


                </div>
            </div> <!-- end card-box -->
        </div> <!-- end col-->


        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-lg rounded-circle border">
                            <img src="{{$user['language']['image'] }}" onerror="this.src='{{$default}}'"  class="rounded-circle avatar-lg img-thumbnail" alt="profile-image">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark mt-1">{{ $user['language']['type'] }}</h3>
                            <p class="text-muted mb-1 text-truncate">Language</p>
                        </div>
                    </div>
                </div> <!-- end row-->
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->

        <?php  if(!empty($user['plan'])){ ?>
        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card-box" style="height: 120px;">
                <div class="row">
                    <div class="col-4">
                        <div class="avatar-lg rounded-circle  border-danger border">
                            <i class="fe-dollar-sign font-22 avatar-title text-danger"></i>
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="text-right">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{$user['plan']['price']}}</span></h3>
                            <p class="text-muted mb-1 text-truncate">{{ $user['plan']['name']['en'] }}</p>
                            <small class="text-muted mb-1 ">Expiration Date {{ date('m-d-Y',strtotime($user['expiration_date'])) }}</small>
                        </div>
                    </div>
                </div> <!-- end row-->
            </div> <!-- end widget-rounded-circle-->
        </div> <!-- end col-->
        <?php } ?>

    </div>

    <?php  if(!empty($user['subuser']) && count($user['subuser'])>0){ ?>
    <div class="row page-title-box">
        <h4 class="page-title ">Sub User</h4>
    </div>
    <?php } ?>

    <div class="row">
            <?php  if(!empty($user['subuser']) && count($user['subuser'])>0){
                foreach ($user['subuser'] as $key => $value) { ?>
                    <div class="col-lg-3 col-xl-3">
                        <div class="card-box text-center">
                            <img src="{{$value['profile_picture_full_url'] }}" onerror="this.src='{{$default}}'"  class="rounded-circle avatar-lg img-thumbnail"
                                alt="profile-image">

                            <h4 class="mb-0"><a target="_blank" href="{{route('admin.user.show', $value['id'])}}" >{{$value['first_name']}} {{$value['last_name']}}</a></h4>
                            <div class="text-left mt-3">
                                <p>Is child? : {{ ($value['is_child'] == 0)?"No":"Yes" }}</p>
                            </div>
                        </div> <!-- end card-box -->
                    </div> <!-- end col-->

        <?php } } ?>
    </div>


    <div class="row">
        
        <div class="col-md-6">
            <div class="card-box ">
                <div class="page-title-box text-left">
                    <h4 class="page-title">My List</h4>
                </div>
                
                <table class="table">
                <thead>
                  <tr>
                    <th>Content Name</th>
                  </tr>
                </thead>
                <tbody>
                    <?php if(!empty($user['mylist']) && count($user['mylist'])>0){ 
                        $user['mylist'] = array_reverse($user['mylist']);
                        foreach ($user['mylist'] as $key => $value) { ?>
                        <tr>
                          <td class="text-left"><a target="_blank" href="{{route('admin.content.show',$value['content']['id'])}}">{{ $value['content']['name']['en'] }}</a></td>
                        </tr>
                    <?php } }else{ ?> 
                        <tr>
                          <td class="text-center"><strong>No data found.</strong></td>
                        </tr>
                    <?php } ?>
                    
                </tbody>
              </table>      
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="card-box">
                <div class="page-title-box text-left">
                    <h4 class="page-title">Viewing Activity</h4>
                </div>



                
                <table class="table">
                <thead>
                  <tr>
                    <th>Date</th>
<<<<<<< HEAD
                    <th class="text-left">Content Name</th>
=======
                    <th >Content Name</th>
>>>>>>> feature/admin_panel_fix
                  </tr>
                </thead>
                <tbody>
                    <?php if(!empty($user['activity_log']) && count($user['activity_log'])>0){
                        $user['activity_log'] = array_reverse($user['activity_log']); 
                        foreach ($user['activity_log'] as $key => $value) { ?>
                        <tr>
                          <td>{{ date('m-d-Y H:i A',strtotime($value['created_at'])) }}</td>
                          <td><a target="_blank" href="{{route('admin.content.show',$value['content']['id'])}}">{{ $value['content']['name']['en'] }}</a>
                            <?php  if($value['content']['content_type'] == 2){ echo ' - <a href="'.route('admin.video.show',$value['content']['video']['id']).'">'.$value['content']['video']['episode_name']['en'].'</a>'; } ?>
                          </td>
                        </tr>
                    <?php } } else{ ?> 
                        <tr>
                          <td colspan="2" class="text-center"><strong>No data found.</strong></td>
                        </tr>
                    <?php } ?>
                    
                </tbody>
              </table>      
            </div>
        </div>

    </div>


</div>

@endsection
@section('script')

@endsection 
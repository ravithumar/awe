@extends('admin.layouts.master')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('adminviewfeedback')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>

    @php
    $question = $feedback->faq->getTranslations('question');
    @endphp

	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">User Name</th>
                                    <td colspan="5"><a href="{{ route('admin.user.show', $feedback->user->id) }}" class="mr-2">{{ $feedback->user->first_name." ".$feedback->user->last_name }}</a></td>
                                </tr>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Question</th>
                                    <td colspan="5"><a href="{{ route('admin.faq.show',$feedback->faq->id) }}" class="mr-2">{{ $question['en'] }}</a></td>
                                </tr>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Description</th>
                                    <td colspan="5">{{ $feedback->description }}</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
			</div>	
		</div>
	</div>
</div>
@endsection
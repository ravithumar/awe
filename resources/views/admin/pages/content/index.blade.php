@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('admincontent')}}
                </div>
                <h4 class="page-title">{{$dateTableTitle}}</h4>
            </div>
            <div class="btn-group float-right mt-2 mb-2">
                    <a href="{{$addUrl}}"  class="btn btn-sm btn-secondary waves-effect waves-light">
                    <span class="btn-label">
                        <i class="fa fa-plus"></i>
                    </span>
                    Add
                </a>
            </div>
		</div>
	</div>
    @include('admin.include.flash-message')
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
					@include('admin.include.table')
				</div>
			</div>	
		</div>
	</div>
</div>

<script type="text/javascript">
	
function adv_content_change(e)
{
  var site_url = location.origin;
  var id = $(e).data('id');
  var token = $(e).data('token');
  //var url = $(e).data('url');
  var method = $(e).data('method');
  
  Notiflix.Confirm.Show(
    'Confirm',
    'Are you sure that you want to change this record?',
    'Yes',
    'No',
    function(){
      $('#loader').show();
      $.ajax({
        url: site_url + '/admin/'+method,
        type: 'get',
        dataType: "JSON",
        data:{ "id":id },
        success: function (returnData) {
          $('#loader').hide();
          $('#{{$dataTableId}}').DataTable().draw();
        }
      }); 
  });
}

function child_adv_content_change(e)
{
  var site_url = location.origin;
  var id = $(e).data('id');
  var token = $(e).data('token');
  //var url = $(e).data('url');
  var method = $(e).data('method');
  
  Notiflix.Confirm.Show(
    'Confirm',
    'Are you sure that you want to change this record?',
    'Yes',
    'No',
    function(){
      $('#loader').show();
      $.ajax({
        url: site_url + '/admin/'+method,
        type: 'get',
        dataType: "JSON",
        data:{ "id":id },
        success: function (returnData) {
          $('#loader').hide();
          $('#{{$dataTableId}}').DataTable().draw();
        }
      }); 
  });

}


</script>

@endsection
@section('script')
@include('admin.include.table_script')
@endsection		
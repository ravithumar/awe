@extends('admin.layouts.master')
@section('content')

<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #D5B115;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('adminviewcontent')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>

    @php    
    $name = $content->getTranslations('name');
    $synopsis = $content->getTranslations('synopsis');
    $default = '/images/default.png';

        $artist_name = array();
        foreach ($content->content_cast as $key => $value){
            $artist_name[] = "<a href='".route('admin.artist.show',$value->artist->id)."'>".$value->artist->name."</a>";
        }

        $content_genres_name = array();
        foreach ($content->content_genres as $key => $value){
            
            $content_genres_name[] = "<a href='".route('admin.genre.show',$value->genre->id)."'>".$value->genre->name."</a>";
        }

        $content_tags_name = array();
        foreach ($content->content_tags as $key => $value){
            $content_tags_name[] = "<a href='".route('admin.tags.show',$value->tags->id)."'>".$value->tags->name."</a>";
        }

    @endphp

	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th class="text-nowrap" scope="row">Name English</th>
                                    <td colspan="5">{{$name['en']}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Name Mandarin</th>
                                    <td colspan="5">{{$name['md']}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Name Hindi</th>
                                    <td colspan="5">{{$name['hi']}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Name Spanish</th>
                                    <td colspan="5">{{$name['sp']}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Name French</th>
                                    <td colspan="5">{{$name['fr']}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Age Rating</th>
                                    <td colspan="5"><a href="{{route('admin.ratings.show',$content->ageratings->id)}}"> {{$content->ageratings->name}}</a></td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Year</th>
                                    <td colspan="5">{{$content->year}}</td>
                                </tr>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Synopsis English</th>
                                    <td colspan="5">{{$synopsis['en']}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Synopsis Mandarin</th>
                                    <td colspan="5">{{$synopsis['md']}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Synopsis Hindi</th>
                                    <td colspan="5">{{$synopsis['hi']}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Synopsis Spanish</th>
                                    <td colspan="5">{{$synopsis['sp']}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Synopsis French</th>
                                    <td colspan="5">{{$synopsis['fr']}}</td>
                                </tr>

                                <tr>
                                    <th class="text-nowrap" scope="row">Artist</th>
                                    <td colspan="5"><?php echo implode(", ",$artist_name); ?></td>
                                </tr>

                                <tr>
                                    <th class="text-nowrap" scope="row">Category</th>
                                    <td colspan="5"><?php echo implode(", ",$content_genres_name); ?></td>
                                </tr>

                                <tr>
                                    <th class="text-nowrap" scope="row">Tags</th>
                                    <td colspan="5"><?php echo implode(", ",$content_tags_name); ?></td>
                                </tr>

                                <tr>
                                    <th class="text-nowrap" scope="row">Content Type</th>
                                    <td colspan="5"><?php if($content->content_type == 1){ echo "Movie"; }elseif($content->content_type == 2){ echo "Tv Show"; 
                                                    }else{ echo ""; }  ?>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="text-nowrap" scope="row">Status</th>
                                    <td colspan="5"><?php if($content->status == 1){ echo "Active"; }
                                                    elseif($content->status == 2){ echo "Deactive"; }
                                                    elseif($content->status == 3){ echo "Upcoming"; }
                                                    else{echo ""; }  ?>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="text-nowrap" scope="row">Is free?</th>
                                    <td colspan="5"><?php if($content->is_free == 1){ echo "Yes"; }else{ echo "No"; } ?></td>
                                </tr>
                            
                                <tr>
                                    <th class="text-nowrap" scope="row">Poster</th>
                                    <td colspan="5"><img class="border rounded p-0"  src="{{!empty($content->poster)?$content->poster:''}}" onerror="this.src='{{$default}}'" alt="your image" style="height: 130px;width: 130px; object-fit: cover;" /></td>
                                </tr>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Logo</th>
                                    <td colspan="5"><img class="border rounded p-0"  src="{{!empty($content->logo)?$content->logo:''}}" onerror="this.src='{{$default}}'" alt="your image" style="height: 130px;width: 130px; object-fit: cover;" /></td>
                                </tr>

                                <tr>
                                    <th class="text-nowrap" scope="row">Trailer</th>
                                    <td colspan="5">
                                        <?php if(!empty($content->trailer)){ ?>
                                            <video src="{{ $content->trailer }}" controls style="width: 20%; height: auto"></video>
                                        <?php } ?>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>


                        <div>
                        <?php 

                        if($content->content_type == 2){
                            if(!empty($content->season)){
                                foreach ($content->season as $key => $value) {
                        ?>
                          <label class="text-nowrap colaps_label" data-toggle="collapse" data-target="#col_{{ $value->id }}"><i class="fa fa-angle-right right_icon"></i><a target="_blank" href="{{route('admin.season.show',$value->id)}}" class="mr-2"> {{ $value->season }} </a></label>
                          <div id="col_{{ $value->id }}" class="collapse">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Video Name</th>
                                            <th>Video</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            if(!empty($value->episode) && count($value->episode)>0){
                                                
                                                foreach ($value->episode as $vkey => $vvalue) {
                                        ?>
                                        <tr>
                                            <td><a target="_blank" href="{{route('admin.video.show',$vvalue->id)}}" class="mr-2">{{ $vvalue->episode_name }}</a></td>
                                            <td>
                                                <?php if(!empty($vvalue->video)){ ?>
                                                    <video src="{{ $vvalue->video }}" controls style="width: 20%; height: auto"></video>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php } }else{ ?> 
                                            <tr><td colspan="2">Video Not available</td></tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                          </div>

                        <?php } } }else{ ?>
                            <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Video</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            if(!empty($content->video)){
                                        ?>
                                        <tr>
                                            <td>
                                                <?php if(!empty($content->video->video)){ ?>
                                                    <video src="{{ $content->video->video }}" controls style="width: 20%; height: auto"></video>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php }else{ ?> 
                                            <tr><td>Video Not available</td></tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                        <?php } ?>

                        <div>


                    </div>



                </div>
			</div>	
		</div>
	</div>
</div>
<style type="text/css">
   .colaps_label{
        border: solid 1px;
        height: 40px;
        width: 100%;
        padding-top: 8px;
        padding-left: 12px;
        background-color: #f1f5f7;
    }
    .right_icon{
        float: left;
        font-size: 21px;
        padding-right: 10px;
    }
</style>
@endsection
@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('adminviewgenre')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>
    @php
    $name = $genre->getTranslations('name');
    @endphp
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                            <tr>
                                <th class="text-nowrap" scope="row">Name English</th>
                                <td colspan="5">{{$name['en']}}</td>
                            </tr>
                            <tr>
                                <th class="text-nowrap" scope="row">Name Mandarin</th>
                                <td colspan="5">{{$name['md']}}</td>
                            </tr>
                            <tr>
                                <th class="text-nowrap" scope="row">Name Hindi</th>
                                <td colspan="5">{{$name['hi']}}</td>
                            </tr>
                            <tr>
                                <th class="text-nowrap" scope="row">Name Spanish</th>
                                <td colspan="5">{{$name['sp']}}</td>
                            </tr>
                            <tr>
                                <th class="text-nowrap" scope="row">Name French</th>
                                <td colspan="5">{{$name['fr']}}</td>
                            </tr>

                            <tr>
                                <th class="text-nowrap" scope="row">Total Content</th>
                                <td colspan="5">{{ count($genre['content_genres']) }}</td>
                            </tr>

                            <tr>
                                <th class="text-nowrap" scope="row">Content</th>
                                <td colspan="5">
                                    @if(!empty($genre->content_data))
                                    <a target="_blank" href="{{ route('admin.content.show',$genre->content_data->id) }}">{{ $genre->content_data->name }}</a>
                                    @endif
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>	
		</div>
	</div>
</div>
@endsection
@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('admineditplan')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-6">
			<div class="card">
				<div class="card-body" >
                <form action="{{ route('admin.plan.attr_update',$plan_id) }}" method="POST">
                @csrf

                    @foreach($plan_attributes as $key => $value)
                    
                        <div class="row">

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="{{ $value->attribute->name }}">{{ $value->attribute->alias }}<span class="text-danger">*</span></label>
                                   
                                    <select class="form-control select2" data-parsley-errors-container="#{{ $value->attribute->name }}_error" required name="{{ $value->attribute->id }}" id="{{ $value->attribute->name }}" data-placeholder="Select {{ $value->attribute->alias}}">
                                        <option selected disabled></option>
                                        @if($value->attribute->value == 'boolean')
                                            <option value="1" {{ ($value->value == '1')?'selected':''; }} >YES</option>
                                            <option value="0" {{ ($value->value == '0')?'selected':''; }} >NO</option>
                                        @elseif($value->attribute->value == 'number')
                                            <option value="1" {{ ($value->value == '1')?'selected':''; }} >1</option>
                                            <option value="2" {{ ($value->value == '2')?'selected':''; }} >2</option>
                                            <option value="3" {{ ($value->value == '3')?'selected':''; }} >3</option>
                                        @endif
                                    </select>
                                    <div id="{{ $value->attribute->name }}_error"></div>
                                    @error('{{ $value->attribute->name }}')
                                        <div class="error">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    @endforeach
                    

                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                            Submit
                        </button>
                        <a href="{{ route('admin.plan.index') }}" class="btn btn-secondary waves-effect m-l-5">Cancel</a>
                        <!-- <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                            Cancel
                        </button> -->
                    </div>

                </form>

				</div>
			</div>	
		</div>
	</div>
</div>
@endsection
@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('adminviewagerating')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>

    @php
    $name = $ageRating->getTranslations('name');
    $description = $ageRating->getTranslations('description');
    @endphp

	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th class="text-nowrap" scope="row">Name English</th>
                                    <td colspan="5">{{$name['en']}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Name Mandarin</th>
                                    <td colspan="5">{{$name['md']}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Name Hindi</th>
                                    <td colspan="5">{{$name['hi']}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Name Spanish</th>
                                    <td colspan="5">{{$name['sp']}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Name French</th>
                                    <td colspan="5">{{$name['fr']}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Description English</th>
                                    <td colspan="5">{{!empty($description['en'])?$description['en']:''}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Description Mandarin</th>
                                    <td colspan="5">{{!empty($description['md'])?$description['md']:''}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Description Hindi</th>
                                    <td colspan="5">{{!empty($description['hi'])?$description['hi']:''}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Description Spanish</th>
                                    <td colspan="5">{{!empty($description['sp'])?$description['sp']:''}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Description French</th>
                                    <td colspan="5">{{!empty($description['fr'])?$description['fr']:''}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Age</th>
                                    <td colspan="5">{{$ageRating->age}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Is Children?</th>
                                    <td colspan="5"><?php if($ageRating->children == 1){ echo "Yes"; }else{ echo "No"; } ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>	
		</div>
	</div>
</div>
@endsection
@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('admineditagerating')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-6">
			<div class="card">
				<div class="card-body" >
                <form action="{{ route('admin.ratings.update',$ageRating->id) }}" method="POST">
                @csrf
                @method('PUT')

                @php
                $name = $ageRating->getTranslations('name');
                $description = $ageRating->getTranslations('description');
                @endphp
                
                    <div class="row">
                        

                        <div class="col-12">
                            <div class="form-group">
                                <label for="name_en">Name English<span class="text-danger">*</span></label>
                                <input type="text" name="name_en" parsley-trigger="change" value="{{$name['en']}}" required placeholder="Enter English Name" class="form-control" id="name_en">
                                @error('name_en')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="name_md">Name Mandarin<span class="text-danger">*</span></label>
                                <input type="text" name="name_md" parsley-trigger="change" value="{{$name['md']}}" required placeholder="Enter Mandarin Name" class="form-control" id="name_md">
                                @error('name_md')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="name_hi">Name Hindi<span class="text-danger">*</span></label>
                                <input type="text" name="name_hi" parsley-trigger="change" value="{{$name['hi']}}" required placeholder="Enter Hindi Name" class="form-control" id="name_hi">
                                @error('name_hi')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="name_sp">Name Spanish<span class="text-danger">*</span></label>
                                <input type="text" name="name_sp" parsley-trigger="change" value="{{$name['sp']}}" required placeholder="Enter Spanish Name" class="form-control" id="name_sp">
                                @error('name_sp')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="name_fr">Name French<span class="text-danger">*</span></label>
                                <input type="text" name="name_fr" parsley-trigger="change" value="{{$name['fr']}}" required placeholder="Enter French Name" class="form-control" id="name_fr">
                                @error('name_fr')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>




                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="description_en">Description English</label>
                                <textarea name="description_en" parsley-trigger="change" placeholder="Enter English Description" class="form-control" id="description_en">{{!empty($description['en'])?$description['en']:''}}</textarea>
                                @error('description_en')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="description_md">Description Mandarin</label>
                                <textarea name="description_md" parsley-trigger="change" placeholder="Enter Mandarin Description" class="form-control" id="description_md" >{{!empty($description['md'])?$description['md']:''}}</textarea>
                                @error('description_md')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="description_hi">Description Hindi</label>
                                <textarea name="description_hi" parsley-trigger="change" placeholder="Enter Hindi Description" class="form-control" id="description_hi" >{{!empty($description['hi'])?$description['hi']:''}}</textarea>
                                @error('description_hi')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="description_sp">Description Spanish</label>
                                <textarea name="description_sp" parsley-trigger="change" placeholder="Enter Spanish Description" class="form-control" id="description_sp">{{!empty($description['sp'])?$description['sp']:''}}</textarea>
                                @error('description_sp')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="description_fr">Description French</label>
                                <textarea name="description_fr" parsley-trigger="change" placeholder="Enter French Description" class="form-control" id="description_fr">{{!empty($description['fr'])?$description['fr']:''}}</textarea>
                                @error('description_fr')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>



                        <div class="col-12">
                            <div class="form-group">
                                <label for="name">Age</label>
                                <input type="text" name="age" parsley-trigger="change" value="{{$ageRating->age}}" placeholder="Enter Age" class="form-control" id="age">
                            </div>
                        </div>

                         <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">Is Children?</label>
                                <label class="switch">
                                  <input type="checkbox" name="children" class="form-control" <?php if($ageRating->children == 1){ echo "checked"; } ?> >
                                  <span class="slider round"></span>
                                </label>
                                @error('status')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>

                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                            Submit
                        </button>
                        <a href="{{ route('admin.ratings.index') }}" class="btn btn-secondary waves-effect m-l-5">Cancel</a>
                    </div>
                </form>
				</div>
			</div>	
		</div>
	</div>
</div>
@endsection
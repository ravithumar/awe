@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('admin-edit-product')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
            
		</div>
	</div>
    <?php
    $default_img_url = asset('images/logo.png');
    ?>
    <style type="text/css">
        .item-img{
            object-fit: cover;
            width: 150px;
        }
    </style>
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                <form action="{{ route('admin.product.update',$product->id) }}" method="POST" enctype="multipart/form-data" id="product">
                @csrf
                @method('PUT')

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="name">Name<span class="text-danger">*</span></label>
                                <input type="text" name="name" parsley-trigger="change" value="{{$product->name}}" required placeholder="Enter name" class="form-control" id="name">
                                @error('name')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="category_id">Select Category<span class="text-danger">*</span></label>
                               
                                <select class="form-control select2" data-parsley-errors-container="#category_error" required name="category_id" id="category_id" data-placeholder="Select Category">
                                    <option selected disabled></option>
                                    @foreach($category as $data)
                                    @if ($product->category_id == $data['id'])
                                        <option value="{{$data['id']}}" selected>{{$data['name']}}</option>
                                    @else
                                        <option value="{{$data['id']}}" >{{$data['name']}}</option>
                                    @endif
                                    @endforeach
                                </select>
                                <div id="category_error"></div>
                                @error('category_id')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="discount">Discount</label>
                                <input type="text" name="discount" parsley-trigger="change" value="{{$product->discount}}"  placeholder="Enter Discount" class="form-control perc discount" id="name" min="0" max="100">
                                @error('discount')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="business_discount">Business Discount</label>
                                <input type="text" name="business_discount" parsley-trigger="change" value="{{$product->business_discount}}" placeholder="Enter Business Discount" class="form-control perc discount" id="name" min="0" max="100">
                                @error('business_discount')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 mt-2 mb-1">
                            <h4 class="mt-0 mb-0 header-title">Inventory</h4>
                            <hr class="mt-1 mb-3">
                        </div>
                    </div>

                    @foreach($variant as $key => $value)
                    <div class="row varient-row">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Quantity Info<span class="text-danger">*</span></label>
                                        <div>
                                            <input required type="text" name="quantity_info_old[]" class="form-control quantity_info" placeholder="Ex: 500" value="{{$value['quantity']}}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Measurement<span class="text-danger">*</span></label>
                                        <div>
                                            <select class="form-control select2" data-parsley-errors-container="#measurement_error" required name="measurement_id_old[]" id="measurement_id" data-placeholder="Select Measurement" disabled>
                                                <option selected disabled></option>
                                                @if(isset($measurement) && count($measurement) > 0)
                                                    @foreach($measurement as $data)
                                                        @php
                                                        $selected = '';
                                                        @endphp
                                                        @if($data['id'] == $value['measurement_id'])
                                                            @php
                                                            $selected = 'selected';
                                                            @endphp
                                                        @endif
                                                        <option value="{{$data['id']}}" {{$selected}}>{{$data['name']}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <div id="measurement_error"></div>
                                            @error('measurement_id')
                                                <div class="error">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Price<span class="text-danger">*</span></label>
                                        <div>
                                            <input required type="text" name="price_old[]" class="form-control" placeholder="Ex: 20" value="{{$value['price']}}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Business Price<span class="text-danger">*</span></label>
                                        <div>
                                            <input required type="text" name="business_price_old[]" class="form-control" placeholder="Ex: 20" value="{{$value['business_price']}}" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- // work by rignesh -->
                            <div class="row">                                
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Age</label>
                                        <div>
                                            <input type="text" name="age[]" class="form-control number_pnt" placeholder="Ex: 10" value="{{$value['age']}}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Proof</label>
                                        <div>
                                            <input type="text" name="proof[]" class="form-control number_pnt" placeholder="Ex: 20" value="{{$value['proof']}}" disabled min="0" max="100">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- // work by rignesh -->
                            <div class="row">                                
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="required">Item Code<span class="text-danger">*</span></label>
                                        <div>
                                            <input required type="text" name="item_code[]" class="form-control" placeholder="Ex: 20" value="{{$value['item_code']}}" disabled>
                                        </div>
                                    </div>
                                </div>                                
                            </div>

                        </div>
                        <div class="col-md-2">
                            <div class="text-center">
                                <img class="rounded mx-auto d-block border item-img" src="{{$value['picture']}}" height="150">
                            </div>
                            
                        </div>
                        <div class="col-md-1">
                            <div class="d-flex h-100">
                                <div class="row justify-content-center align-self-center">
                                    <label class="btn btn-danger waves-effect waves-light remove-old-varient-btn remove" title="Remove" data-id="{{ $value['id'] }}"><i class="fas fa-times"></i></label>
                                </div>
                            </div>
                        </div>

                        
                    </div>
                    @endforeach

                    <div id="variants">
                                <!-- Dynmaic  -->
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <button type="button" name="add_variant" class="btn btn-primary waves-effect waves-light" id="add_variant" title="Add More Attribute">
                                    Add More Attribute
                                </button>
                            </div>
                        </div>
                    </div> 

                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                            Submit
                        </button>
                        <a href="{{ route('admin.product.index') }}" class="btn btn-secondary waves-effect m-l-5">Cancel</a>
                        <!-- <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                            Cancel
                        </button> -->
                    </div>

                </form>

				</div>
			</div>	
		</div>
	</div>

    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">Add Stores</h4>
            </div>
            
        </div>
    </div>

    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body" >
                    <form action="{{ route('admin.product.addstore',$product->id) }}" method="POST" id="add-store-form">
                        @csrf

                        <div class="bunch">
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label class="required">Board<span class="text-danger">*</span></label>
                                        <div>
                                            <select class="form-control select2" data-parsley-errors-container="#board_error" required name="board_id" id="board_id" data-placeholder="Select Board">
                                                <option selected disabled></option>
                                                @if(isset($board) && count($board) > 0)
                                                    @foreach($board as $data)
                                                        <option value="{{$data['id']}}">{{$data['first_name']." ".$data['last_name']}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <div id="board_error"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row store-row">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label class="required">Store<span class="text-danger">*</span></label>
                                        <div>
                                            <select class="form-control select2 store_id" data-parsley-errors-container="#store_error" required name="store_id[]" id="store_id" data-placeholder="Select Store">
                                                <option selected disabled></option>
                                            </select>
                                            <div id="store_error"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label class="required">Variant<span class="text-danger">*</span></label>
                                        <div>
                                            <select class="form-control select2 v-group" data-parsley-errors-container="#variant_error" required name="variant_id[]" id="variant_id" data-placeholder="Select Variant">
                                                <option selected disabled></option>
                                                @if(isset($variant) && count($variant) > 0)
                                                    @foreach($variant as $data)
                                                        <option value="{{$data['id']}}">{{$data['quantity']. " " .$data['measurement'] }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            <div id="variant_error"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label class="required">Stock<span class="text-danger">*</span></label>
                                        <div>
                                            <input required type="text" name="stock[]" class="form-control stock-touchspin" placeholder="Ex: 20" value="" data-parsley-errors-container="#stock-error">
                                        </div>
                                        <div id="stock-error"></div>
                                    </div>
                                </div>
                                <div class="col-lg-1">
                                </div>
                            </div>
                            <div class="stores">
                                        <!-- Dynmaic  -->
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <button type="button" name="add_store" class="btn btn-primary waves-effect waves-light add_store" data-id="add_store" title="Add More Store">
                                            Add More Store
                                        </button>
                                    </div>

                                </div>
                            </div> 
                        </div>
                        
                        <div id="dynamic-boards">
                        </div>

                        <div class="row">
                            <div class="col-lg-12 row">
                                <div class="form-group">
                                    <button type="button" name="add_board" class="btn btn-warning waves-effect waves-light add_board ml-2" data-id="add_board" title="Add More Board">
                                        Add More Board
                                    </button>
                                </div>

                            </div>
                        </div> 

                        

                        <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit" id="save-store-products-btn">
                            Submit
                        </button>
                        <a href="{{ route('admin.product.index') }}" class="btn btn-secondary waves-effect m-l-5">Cancel</a>
                        <!-- <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                            Cancel
                        </button> -->
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">Update & Remove Stores</h4>
            </div>
            
        </div>
    </div>

    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body" >

                    @if($store_variant)
                    @foreach($store_variant as $key => $value)
                        <div class="bunch">

                            <div class="row store-products" data-id="{{ $value->id }}">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label>Store</label>
                                        <div>
                                            <input type="text" class="form-control" readonly name="" value="{{ $value->store[0]->first_name.' '.$value->store[0]->last_name }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <label>Variant</label>
                                        <div>
                                            <input type="text" class="form-control" readonly name="" value="{{ $value->variant->quantity.' '.$value->variant->measurement }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label>Stock<span class="text-danger">*</span></label>
                                        <div>
                                            <input required type="number" min=0 name="stock" class="form-control stock stock-touchspin" placeholder="Ex: 20" value="{{$value->stock}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="d-flex h-100">
                                        <div class="row justify-content-center align-self-center mt-2">
                                            <label class="btn btn-primary waves-effect waves-light save-store mr-2" title="Save"><i class="fas fa-check"></i></label>
                                            <label class="btn btn-danger waves-effect waves-light remove-btn remove-store" title="Delete"><i class="fas fa-trash-alt"></i></label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    @endforeach
                    @endif


                        

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
var default_img_url = '<?php echo $default_img_url; ?>';
var measurement = '<?php echo (isset($measurement) && count($measurement) > 0)?json_encode($measurement):""; ?>';
var variant = '<?php echo (isset($variant) && count($variant) > 0)?json_encode($variant):""; ?>';
var board = '<?php echo (isset($board) && count($board) > 0)?json_encode($board):""; ?>';
var store = '<?php echo (isset($store) && count($store) > 0)?json_encode($store):""; ?>';
        
$(document).on('click', '.default-img', function(){

    $(this).next('input').click();
    //$(this).attr('src', default_img_url);
    return false;
});

var set_img;
$(document).on('change', '.upload-img', function() {
    readURL(this);
    set_img = '';
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            console.log($(input).closest('.text-center').find('.default-img'));

            $(input).closest('.text-center').find('.default-img').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

if(measurement != ''){

    measurement = $.parseJSON(measurement);
    var str = '';
    $.each(measurement, function(k, v) {
        str += '<option value='+v.id+'>'+v.name+'</option>';
    });
}

if(variant != ''){

    variant = $.parseJSON(variant);
    var v_str = '';
    $.each(variant, function(k, v) {
        v_str += '<option value='+v.id+'>'+v.quantity+' '+v.measurement+'</option>';
    });
}

if(board != ''){

    board = $.parseJSON(board);
    var b_str = '';
    $.each(board, function(k, v) {
        b_str += '<option value='+v.id+'>'+v.first_name+' '+v.last_name+'</option>';
    });
}

if(store != ''){

    var store_data = $.parseJSON(store);
    var s_str = '';
    $.each(store_data, function(k, v) {
        s_str += '<option value='+v.id+'>'+v.first_name+' '+v.last_name+'</option>';
    });
}

$(document).on('click','.remove-btn',function(){
    $(this).closest(".varient-row").remove();
});
$(document).on('click','.remove-store-btn',function(){
    $(this).closest(".store-row").remove();
});
$(document).on('click','.remove-board-btn',function(){
    $(this).closest(".bunch").remove();
});


var count = 1;
$(document).on('click','#add_variant',function(){

    count = count + 1;

    var variant_row = 
    '<div class="varient-row">'+
    '<hr>'+
        '<div class="row">'+
            '<div class="col-md-9">'+
                '<div class="row">'+
                    '<div class="col-lg-6">'+
                        '<div class="form-group">'+
                            '<label class="required">Quantity Info<span class="text-danger">*</span></label>'+
                            '<div>'+
                                '<input required type="text" name="quantity_info[]" class="form-control quantity_info" placeholder="Ex: 500">'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-lg-6">'+
                        '<div class="form-group">'+
                            '<label class="required">Measurement<span class="text-danger">*</span></label>'+
                            '<div>'+
                                '<select class="form-control select2" data-parsley-errors-container="#measurement_error'+count+'" required name="measurement_id[]" id="measurement_id'+count+'" data-placeholder="Select Measurement">'+
                                    '<option selected disabled></option>'+
                                    str+
                                '</select>'+
                                '<div id="measurement_error'+count+'"></div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="row">'+
                    '<div class="col-lg-6">'+
                        '<div class="form-group">'+
                            '<label class="required">Price<span class="text-danger">*</span></label>'+
                            '<div>'+
                                '<input required type="number" name="price[]" class="form-control price-touchspin" placeholder="Ex: 20" data-parsley-errors-container="#price-error'+count+'">'+
                            '</div>'+
                            '<div id="price-error'+count+'"></div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-lg-6">'+
                        '<div class="form-group">'+
                            '<label class="required">Business Price<span class="text-danger">*</span></label>'+
                            '<div>'+
                                '<input required type="number" name="business_price[]" class="form-control price-touchspin" placeholder="Ex: 20" data-parsley-errors-container="#b-price-error'+count+'">'+
                            '</div>'+
                            '<div id="b-price-error'+count+'"></div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                // work by rignesh
                '<div class="row">'+
                    '<div class="col-lg-6">'+
                        '<div class="form-group">'+
                            '<label class="required">Age</label>'+
                            '<div>'+
                                '<input type="number" name="age[]" class="form-control age" placeholder="Ex: 10">'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-lg-6">'+
                        '<div class="form-group">'+
                            '<label class="required">Proof</label>'+
                            '<div>'+
                                '<input type="number" name="proof[]" class="form-control perc" placeholder="Ex: 20" min="0" max="100">'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="row">'+
                    '<div class="col-lg-6">'+
                        '<div class="form-group">'+
                            '<label class="required">Item Code<span class="text-danger">*</span></label>'+
                            '<div>'+
                                '<input required type="text" name="item_code[]" class="form-control" placeholder="Ex: 10">'+
                            '</div>'+
                        '</div>'+
                    '</div>'+                    
                '</div>'+
                // work by rignesh
            '</div>'+
            '<div class="col-md-2">'+
                '<div class="text-center">'+
                    '<img class="rounded mx-auto d-block border item-img default-img" src="'+default_img_url+'" height="150">'+
                    '<input type="file" name="picture[]" accept="image/*" style="visibility:hidden; position: absolute;" class="input-file upload-img" required>'+
                '</div>'+
            '</div>'+
            '<div class="col-md-1">'+
                '<div class="d-flex h-100">'+
                    '<div class="row justify-content-center align-self-center">'+
                        '<label class="btn btn-danger waves-effect waves-light remove-btn remove" title="Remove"><i class="fas fa-times"></i></label>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'+
    '</div>';

    $('#variants').append(variant_row);
    $(".select2").select2();
    $(".age").TouchSpin({
        min: 0,
        max: 1000000000
    });
    $(".perc").TouchSpin({
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%'
    });
    $(".price-touchspin").TouchSpin({
        min: 0,
        max: 1000000000,
        step: 0.1,
        stepinterval: 50,
        decimals: 2,
        maxboostedstep: 10000000,
        prefix: '$'
    });

    $(document).ready(function() {
        //called when key is pressed in textbox
        $(".number_pnt").keypress(function(evt) {
            console.log('number_pnt');
            //if the letter is not digit then display error and don't type anything
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                return false;
            return true;
        });
    });
    
});

var store_count = 1;
$(document).on('click','.add_store',function(){

    var prev_store_div = $(this).parent().parent().parent().prev('.stores');
    store_count = store_count + 1;
    var board_id = $(this).closest('.bunch').find('#board_id :selected').val();

    var s_str = '';
    if(store != '' && board_id != ''){
        var store_data = $.parseJSON(store);
        $.each(store_data, function(k, v) {
            if(board_id == v.parent_id){
                s_str += '<option value='+v.id+'>'+v.first_name+' '+v.last_name+'</option>';
            }
        });
    }

    var variant_row = 
        '<div class="row store-row">'+
            '<div class="col-lg-5">'+
                '<div class="form-group">'+
                    '<label class="required">Store<span class="text-danger">*</span></label>'+
                    '<div>'+
                        '<select class="form-control select2 store_id" data-parsley-errors-container="#store_error'+store_count+'" required name="store_id[]" id="store_id'+store_count+'" data-placeholder="Select Store">'+
                            '<option selected disabled></option>'+
                            s_str+
                        '</select>'+
                        '<div id="store_error'+store_count+'"></div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col-lg-3">'+
                '<div class="form-group">'+
                    '<label class="required">Variant<span class="text-danger">*</span></label>'+
                    '<div>'+
                        '<select class="form-control select2 v-group" data-parsley-errors-container="#variant_error'+store_count+'" required name="variant_id[]" id="variant_id'+store_count+'" data-placeholder="Select Variant">'+
                            '<option selected disabled></option>'+
                            v_str+
                        '</select>'+
                        '<div id="variant_error'+store_count+'"></div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col-lg-2">'+
                '<div class="form-group">'+
                    '<label class="required">Stock<span class="text-danger">*</span></label>'+
                    '<div>'+
                        '<input required type="text" name="stock[]" class="form-control stock-touchspin" placeholder="Ex: 20" value="" data-parsley-errors-container="#stock-error'+store_count+'">'+
                    '</div>'+
                    '<div id="stock-error'+store_count+'">'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col-lg-1">'+
                '<div class="d-flex h-100">'+
                    '<div class="row justify-content-center align-self-center mt-2">'+
                        '<label class="btn btn-danger waves-effect waves-light remove-store-btn remove" title="Remove"><i class="fas fa-times"></i></label>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>';

    prev_store_div.append(variant_row);
    $(".select2").select2();
    $(".stock-touchspin, .age").TouchSpin({
        min: 0,
        max: 1000000000
    });
    $(".perc").TouchSpin({
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%'
    });
    $(".price-touchspin").TouchSpin({
        min: 0,
        max: 1000000000,
        step: 0.1,
        stepinterval: 50,
        decimals: 2,
        maxboostedstep: 10000000,
        prefix: '$'
    });

});

var board_count = 1;
$(document).on('click','.add_board',function(){

    board_count = board_count + 1;
    store_count = store_count + 1;

    var variant_row = 
        
        '<div class="bunch">'+
        '<hr>'+
            '<div class="row">'+
                '<div class="col-lg-5">'+
                    '<div class="form-group">'+
                        '<label class="required">Board<span class="text-danger">*</span></label>'+
                        '<div>'+
                            '<select class="form-control select2" data-parsley-errors-container="#board_error'+board_count+'" required name="board_id'+board_count+'" id="board_id'+board_count+'" data-placeholder="Select Board">'+
                                '<option selected disabled></option>'+
                                b_str+
                            '</select>'+
                            '<div id="board_error'+board_count+'"></div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="col-lg-1">'+
                    '<div class="d-flex h-100">'+
                        '<div class="row justify-content-center align-self-center mt-2">'+
                            '<label class="btn btn-danger waves-effect waves-light remove-board-btn remove" title="Remove"><i class="fas fa-times"></i></label>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>';

    variant_row += 
        '<div class="row store-row">'+
            '<div class="col-lg-5">'+
                '<div class="form-group">'+
                    '<label class="required">Store<span class="text-danger">*</span></label>'+
                    '<div>'+
                        '<select class="form-control select2 store_id" data-parsley-errors-container="#store_error'+store_count+'" required name="store_id[]" id="store_id'+store_count+'" data-placeholder="Select Store">'+
                            '<option selected disabled></option>'+
                        '</select>'+
                        '<div id="store_error'+store_count+'"></div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col-lg-3">'+
                '<div class="form-group">'+
                    '<label class="required">Variant<span class="text-danger">*</span></label>'+
                    '<div>'+
                        '<select class="form-control select2 v-group" data-parsley-errors-container="#variant_error'+store_count+'" required name="variant_id[]" id="variant_id'+store_count+'" data-placeholder="Select Variant">'+
                            '<option selected disabled></option>'+
                            v_str+
                        '</select>'+
                        '<div id="variant_error'+store_count+'"></div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col-lg-2">'+
                '<div class="form-group">'+
                    '<label class="required">Stock<span class="text-danger">*</span></label>'+
                    '<div>'+
                        '<input required type="number" name="stock[]" class="form-control stock-touchspin" placeholder="Ex: 20" value="">'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'+
        '<div class="stores">'+
        '</div>';

    variant_row += 
            '<div class="row">'+
                '<div class="col-lg-12">'+
                    '<div class="form-group">'+
                        '<button type="button" name="add_store" class="btn btn-primary waves-effect waves-light add_store" data-id="add_store" title="Add More Store">'+
                            'Add More Store'+
                        '</button>'+
                    '</div>'+
                '</div>'+
            '</div>';
    variant_row += '</div>';

    $('#dynamic-boards').append(variant_row);
    $(".select2").select2();
    $(".stock-touchspin, .age").TouchSpin({
        min: 0,
        max: 1000000000
    });
    $(".perc").TouchSpin({
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%'
    });
    $(".price-touchspin").TouchSpin({
        min: 0,
        max: 1000000000,
        step: 0.1,
        stepinterval: 50,
        decimals: 2,
        maxboostedstep: 10000000,
        prefix: '$'
    });
});

$(document).on('change','select[name="board_id"]',function(){
    var board_id = $(this).val();
    var stores = $(this).closest('.bunch').find('.store_id');

    stores.find('option').remove().end().append('<option selected disabled></option>').val('');

    if(store != ''){
        var store_data = $.parseJSON(store);
        var s_str = '';
        $.each(store_data, function(k, v) {
            if(board_id == v.parent_id){
                s_str += '<option value='+v.id+'>'+v.first_name+' '+v.last_name+'</option>';
            }
        });
        if(s_str != ''){
            stores.append(s_str);
        }
    }
});

$(document).on('click','.remove-store',function(){
    var row_id = $(this).closest('.store-products').attr('data-id');
    var store_product_row = $(this).closest('.store-products');

    Notiflix.Confirm.Show(
        'Confirm',
        'Are you sure that you want to delete this record?',
        'Yes',
        'No',
        function(){
          Notiflix.Loading.Standard();
          $.ajax({
            url: '/admin/remove_product_store/'+row_id,
            type: 'get',
            success: function (returnData) {
                store_product_row.remove();
                Notiflix.Loading.Remove();
                Notiflix.Notify.Success('Deleted');
            }
          }); 
      });
        
});
$(document).on('click','.save-store',function(){
    var row_id = $(this).closest('.store-products').attr('data-id');
    var store_product_row = $(this).closest('.store-products');
    var stock = store_product_row.find('.stock').val();
    if(stock == ''){
        Notiflix.Notify.Failure('Stock value is required');
        return false;
    }
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });
    Notiflix.Loading.Standard();
      $.ajax({
        url: '/admin/save_product_store',
        type: 'post',
        data: {
            "id": row_id,
            "stock": stock
        },
        success: function (returnData) {
            
            Notiflix.Loading.Remove();
            returnData = $.parseJSON(returnData);
            console.log(returnData);
            if(returnData.status == true){
                Notiflix.Notify.Success(returnData.message);
            }else{
                Notiflix.Notify.Failure(returnData.message);
            }
        }
      }); 
        
});
$(document).on('click','.remove-old-varient-btn',function(){
    var varient_id = $(this).attr('data-id');
    $('<input>').attr({
        type: 'hidden',
        id: '',
        name: 'remove_varients[]',
        value: varient_id
    }).appendTo('form#product');
    $(this).closest(".varient-row").remove();
});

$(document).on('click','#save-store-products-btn',function(e){

   console.log('prinnt ');

    e.preventDefault();
    var store_varient = []; 
    var submit = true;

    var count = 0;
    $(".store_id").each(function (key, val){
        count = key;
    });

    $(".store_id").each(function (key, val) {
    console.log('count'+count);
    var current = key;
    var temp = [];
    temp['store_id'] = $(val).val();
    var store_name = $(val).find(":selected").text();
    var v_name = '';

    
    $(".v-group").each(function (v_key, v_val) {
        if(v_key == key){
            temp['v_id'] = $(v_val).val();
            v_name = $(v_val).find(":selected").text();
        }
        
    });

    var duplicate = false;
    $.each(store_varient, function (s_key, s_val) {
        if(s_val.store_id == temp['store_id'] && s_val.v_id == temp['v_id']){
            
            if(store_name != '' && v_name != ''){
                duplicate = true;
                Notiflix.Notify.Failure('Duplicate entry for ' + store_name + ' - '+v_name);
                return false;
            }
            
        }
    });

    if(duplicate == false){
        store_varient.push(temp);

        if(count == current){
            $('#add-store-form').submit();
        }

    }else{
        return false;
    }
    
});

});


</script>
@endsection

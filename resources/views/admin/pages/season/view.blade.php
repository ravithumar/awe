@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('adminviewseason')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>
    @php
    $name = $seasonContentValue->getTranslations('season');
    @endphp

	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th class="text-nowrap" scope="row">Content Name</th>
                                    <td colspan="5"><a target="_blank" href="{{route('admin.content.show',$seasonContentValue->content_data->id)}}">{{$seasonContentValue->content_data->name}}</a></td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Season Name English</th>
                                    <td colspan="5">{{$name['en']}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Season Name Mandarin</th>
                                    <td colspan="5">{{$name['md']}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Season Name Hindi</th>
                                    <td colspan="5">{{$name['hi']}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Season Name Spanish</th>
                                    <td colspan="5">{{$name['sp']}}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Season Name French</th>
                                    <td colspan="5">{{$name['fr']}}</td>
                                </tr>
                            </tbody>
                        </table>

                        <?php if(!empty($seasonContentValue->episode) && count($seasonContentValue->episode)>0){ ?>
                            <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Video Name</th>
                                            <th>Video</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($seasonContentValue->episode as $vkey => $vvalue) { ?>
                                        <tr>
                                            <td><a target="_blank" href="{{route('admin.video.show',$vvalue->id)}}" class="mr-2">{{ $vvalue->episode_name }}</a></td>
                                            <td>
                                                <?php if(!empty($vvalue->video)){ ?>
                                                    <video src="{{ $vvalue->video }}" controls style="width: 20%; height: auto"></video>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                        <?php } ?>
                    </div>
                </div>
			</div>	
		</div>
	</div>
</div>
@endsection
@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('admineditseason')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>


	<div class="row">
		<div class="col-xl-6">
			<div class="card">
				<div class="card-body" >
                <form action="{{ route('admin.season.update',$seasonValue->id) }}" method="POST">
                @csrf
                @method('PUT')

                @php
                $name = $seasonValue->getTranslations('season');
                @endphp
                
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="name">Cast Type<span class="text-danger">*</span></label>
                                <select class="form-control select2" name="content_id" required data-parsley-errors-container="#content_id_error">
                                    <option disabled>Select Cast Type</option>
                                    @foreach ($contentRecord as $contentValue)
                                        <option value="{{ $contentValue['id'] }}" {{ ( $contentValue['id'] == $seasonValue->content_data->id) ? 'selected' : '' }}> {{ $contentValue['name'] }} </option>
                                    @endforeach
                                </select>
                                <div class="error" id="content_id_error"></div>
                                @error('content_id')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <?php /*<div class="col-12">
                            <div class="form-group">
                                <label for="name">Season<span class="text-danger">*</span></label>
                                <input type="text" name="season" parsley-trigger="change" value="{{$seasonValue->season}}" required placeholder="Enter Season name" class="form-control" id="season">
                                @error('season')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>*/ ?>



                        <div class="col-12">
                            <div class="form-group">
                                <label for="name_en">Season Name English<span class="text-danger">*</span></label>
                                <input type="text" name="name_en" parsley-trigger="change" value="{{$name['en']}}" required placeholder="Enter Season English Name" class="form-control" id="name_en">
                                @error('name_en')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="name_md">Season Name Mandarin<span class="text-danger">*</span></label>
                                <input type="text" name="name_md" parsley-trigger="change" value="{{$name['md']}}" required placeholder="Enter Season Mandarin Name" class="form-control" id="name_md">
                                @error('name_md')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="name_hi">Season Name Hindi<span class="text-danger">*</span></label>
                                <input type="text" name="name_hi" parsley-trigger="change" value="{{$name['hi']}}" required placeholder="Enter Season Hindi Name" class="form-control" id="name_hi">
                                @error('name_hi')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="name_sp">Season Name Spanish<span class="text-danger">*</span></label>
                                <input type="text" name="name_sp" parsley-trigger="change" value="{{$name['sp']}}" required placeholder="Enter Season Spanish Name" class="form-control" id="name_sp">
                                @error('name_sp')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="name_fr">Season Name French<span class="text-danger">*</span></label>
                                <input type="text" name="name_fr" parsley-trigger="change" value="{{$name['fr']}}" required placeholder="Enter Season French Name" class="form-control" id="name_fr">
                                @error('name_fr')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>

                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                            Submit
                        </button>
                        <a href="{{ route('admin.season.index') }}" class="btn btn-secondary waves-effect m-l-5">Cancel</a>
                    </div>
                </form>
				</div>
			</div>	
		</div>
	</div>
</div>
@endsection
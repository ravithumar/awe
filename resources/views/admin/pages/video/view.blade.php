@extends('admin.layouts.master')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('adminviewvideo')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>
    @php
    $default = '/images/default.png';
    if($video->content->content_type == 2){
        $episode_name = $video->getTranslations('episode_name');
        $episode_synopsis = $video->getTranslations('episode_synopsis');    
    }

    $name = $video->content->getTranslations('name');
    @endphp
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tbody>

                                <tr>
                                    <th class="text-nowrap" scope="row">Content Name</th>
                                    <td colspan="5"><a target="_blank" href="{{ route('admin.content.show',$video->content->id) }}">{{ $name['en'] }}</a></td>
                                </tr>

                                <?php if($video->content->content_type == 2){ ?>

                                <tr>
                                    <th class="text-nowrap" scope="row">Season Name</th>
                                    <td colspan="5">
                                        <?php if(!empty($season->season)){ ?>
                                                <a target="_blank" href="{{ route('admin.season.show',$video->season->id) }}">{{ $season->season }}</a>           
                                        <?php } ?>
                                    </td>
                                </tr>

                                <tr>
                                    <th class="text-nowrap" scope="row">Video Name English</th>
                                    <td colspan="5">{{ !empty($episode_name['en'])?$episode_name['en']:'' }}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Video Name Mandarin</th>
                                    <td colspan="5">{{ !empty($episode_name['md'])?$episode_name['md']:'' }}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Video Name Hindi</th>
                                    <td colspan="5">{{ !empty($episode_name['hi'])?$episode_name['hi']:'' }}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Video Name Spanish</th>
                                    <td colspan="5">{{ !empty($episode_name['sp'])?$episode_name['sp']:'' }}</td>
                                </tr>
                                <tr>
                                    <th class="text-nowrap" scope="row">Video Name French</th>
                                    <td colspan="5">{{ !empty($episode_name['fr'])?$episode_name['fr']:'' }}</td>
                                </tr>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Video Synopsis English</th>
                                    <td colspan="5">{{ !empty($episode_synopsis['en'])?$episode_synopsis['en']:'' }}</td>
                                </tr>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Video Synopsis Mandarin</th>
                                    <td colspan="5">{{ !empty($episode_synopsis['md'])?$episode_synopsis['md']:'' }}</td>
                                </tr>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Video Synopsis Hindi</th>
                                    <td colspan="5">{{ !empty($episode_synopsis['hi'])?$episode_synopsis['hi']:'' }}</td>
                                </tr>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Video Synopsis Spanish</th>
                                    <td colspan="5">{{ !empty($episode_synopsis['sp'])?$episode_synopsis['sp']:'' }}</td>
                                </tr>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Video Synopsis French</th>
                                    <td colspan="5">{{ !empty($episode_synopsis['fr'])?$episode_synopsis['fr']:'' }}</td>
                                </tr>
                                
                                <?php } ?>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Duration</th>
                                    <td colspan="5">{{ $video->duration }}</td>
                                </tr>


                                <tr>
                                    <th class="text-nowrap" scope="row">Subtitle English</th>
                                    <td colspan="5"><?php if(!empty($video->video_subtitle)){ 
                                    foreach ($video->video_subtitle as $key => $value) { 
                                        if($value->language_id == 1){ ?>
                                        <div><a href="{{ $value->file }}">Download</a></div>
                                <?php } } } ?></td>
                                </tr>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Subtitle Mandarin</th>
                                    <td colspan="5"><?php if(!empty($video->video_subtitle)){ 
                                    foreach ($video->video_subtitle as $key => $value) { 
                                        if($value->language_id == 2){ ?>
                                        <div><a href="{{ $value->file }}">Download</a></div>
                                <?php } } } ?></td>
                                </tr>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Subtitle Hindi</th>
                                    <td colspan="5"><?php if(!empty($video->video_subtitle)){ 
                                    foreach ($video->video_subtitle as $key => $value) { 
                                        if($value->language_id == 3){ ?>
                                        <div><a href="{{ $value->file }}">Download</a></div>
                                <?php } } } ?></td>
                                </tr>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Subtitle Spanish</th>
                                    <td colspan="5"><?php if(!empty($video->video_subtitle)){ 
                                    foreach ($video->video_subtitle as $key => $value) { 
                                        if($value->language_id == 5){ ?>
                                        <div><a href="{{ $value->file }}">Download</a></div>
                                <?php } } } ?></td>
                                </tr>
                                
                                <tr>
                                    <th class="text-nowrap" scope="row">Subtitle French</th>
                                    <td colspan="5"><?php if(!empty($video->video_subtitle)){ 
                                    foreach ($video->video_subtitle as $key => $value) { 
                                        if($value->language_id == 4){ ?>
                                        <div><a href="{{ $value->file }}">Download</a></div>
                                <?php } } } ?></td>
                                </tr>

                                <tr>
                                    <th class="text-nowrap" scope="row">Thumbnail image</th>
                                    <td colspan="5"><img class="border rounded p-0"  src="{{!empty($video->thumbnail_image)?env('AWS_S3_URL').'video/'.$video->thumbnail_image:''}}" onerror="this.src='{{$default}}'" alt="your image" style="height: 130px;width: 130px; object-fit: cover;" /></td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>

                    <?php if(!empty($video->video)){ ?>
                        <video src="{{ $video->video }}" controls style="width: 100%; height: auto"></video>
                    <?php } ?>

                </div>
			</div>	
		</div>
	</div>


</div>
@endsection
@extends('admin.layouts.master')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('adminaddvideo')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
        </div>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                
                <form action="{{ route('admin.video.store') }}" method="POST" id="video_add_form" enctype="multipart/form-data">
                <input type="hidden" name="content_type" id="content_type" value="">
                @csrf
                @method('POST')

                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">Content<span class="text-danger">*</span></label>
                                <select class="form-control select2" name="content_id" required id="video_content_id" onchange="sessionval(this)" data-parsley-errors-container="#video_content_id_error">
                                    <option disabled selected>Select Content</option>
                                    @foreach ($content_data as $value)
                                        <option value="{{ $value['id'] }}"> {{ $value['name'] }} </option>
                                    @endforeach
                                </select>
                                <div class="error" id="video_content_id_error"></div>
                                @error('content_id')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name" id="video_session_label">Season</label>
                                <select class="form-control select2" name="season_id" id="video_session_id" data-parsley-errors-container="#video_session_id_error">
                                    <option disabled selected>Select season</option>
                                </select>
                                <div class="error" id="video_session_id_error"></div>
                                @error('season_id')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                <div class="tv_show_display_section">

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="episode_name_en">Video Name English<span class="text-danger">*</span></label>
                                <input type="text" name="episode_name_en" required placeholder="Enter video name english" class="form-control" id="episode_name_en">
                                @error('episode_name_en')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="episode_synopsis_en">Video Synopsis English<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="episode_synopsis_en" placeholder="Enter video synopsis english" id="episode_synopsis_en" required></textarea>
                                @error('episode_synopsis_en')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="episode_name_md">Video Name Mandarin<span class="text-danger">*</span></label>
                                <input type="text" name="episode_name_md" required placeholder="Enter video name mandarin" class="form-control" id="episode_name_md">
                                @error('episode_name_md')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="episode_synopsis_md">Video Synopsis Mandarin<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="episode_synopsis_md" placeholder="Enter video synopsis mandarin" id="episode_synopsis_md" required></textarea>
                                @error('episode_synopsis_md')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="episode_name_hi">Video Name Hindi<span class="text-danger">*</span></label>
                                <input type="text" name="episode_name_hi" required placeholder="Enter video name hindi" class="form-control" id="episode_name_hi">
                                @error('episode_name_hi')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="episode_synopsis_hi">Video Synopsis Hindi<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="episode_synopsis_hi" id="episode_synopsis_hi" placeholder="Enter video synopsis hindi"  required></textarea>
                                @error('episode_synopsis_hi')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="episode_name_sp">Video Name Spanish<span class="text-danger">*</span></label>
                                <input type="text" name="episode_name_sp" required placeholder="Enter video name spanish" class="form-control" id="episode_name_sp">
                                @error('episode_name_sp')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="episode_synopsis_sp">Video Synopsis Spanish<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="episode_synopsis_sp" id="episode_synopsis_sp" placeholder="Enter video synopsis spanish" required></textarea>
                                @error('episode_synopsis_sp')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="episode_name_fr">Video Name French<span class="text-danger">*</span></label>
                                <input type="text" name="episode_name_fr" required placeholder="Enter video name french" class="form-control" id="episode_name_fr">
                                @error('episode_name_fr')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="episode_synopsis_fr">Video Synopsis French<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="episode_synopsis_fr" id="episode_synopsis_fr" placeholder="Enter video synopsis french" required></textarea>
                                @error('episode_synopsis_fr')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>

                </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="subtitle_en">Subtitle English</label>
                                <input type="file" name="subtitle_en" class="form-control" placeholder="select file">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="subtitle_md">Subtitle Mandarin</label>
                                <input type="file" name="subtitle_md" class="form-control" placeholder="select file">
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="subtitle_hi">Subtitle Hindi</label>
                                <input type="file" name="subtitle_hi" class="form-control" placeholder="select file">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="subtitle_sp">Subtitle Spanish</label>
                                <input type="file" name="subtitle_sp" class="form-control" placeholder="select file">
                            </div>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="subtitle_fr">Subtitle French</label>
                                <input type="file" name="subtitle_fr" class="form-control" placeholder="select file">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="subtitle_en">Video File </label>
                                <input type="file" required id="browseFile" class="form-control" data-method = "chunk_file_upload" placeholder="select file" accept="video/*">
                                <input type="hidden" name="video_file" id="video_link" class="required" required value="" data-parsley-errors-container="#video_link_error">
                                <input type="hidden" name="duration" id="video_duration" value="">

                                <div class="error" id="video_link_error"></div>
                                @error('video_link')
                                    <div class="error">{{ $message }}</div>
                                @enderror

                                <div  style="display: none" class="progress mt-3" style="height: 25px">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%; height: 100%">75%</div>
                                </div>
                            
                                

                                

                            </div>

                        </div>
                    </div>
                    <div class="row">


                        @php
                            $default = '/images/default.png';
                        @endphp
                        
                        <div class="col-md-6 tv_show_display_section">
                            <div class="form-group">
                                <label for="name">Thumbnail image</label>
                                <input type="file" name="thumbnail_image"  class="form-control" id="thumbnail_image" data-parsley-trigger="change"  data-parsley-max-file-size="5" data-parsley-filemimetypes="image/jpeg, image/png" accept="image/*" data-parsley-file-mime-types-message="Only allowed jpeg & png files" onchange="readURL1(this);">
                                @error('thumbnail_image')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <img class="border rounded p-0"  src="" onerror="this.src='{{$default}}'" alt="your image" style="height: 130px;width: 130px; object-fit: cover;" id="blah1"/>
                                </div>
                            </div>

                        </div>

                        <div class="col-lg-6">
                            <input type="button" class="btn btn-primary mt-2" id="upToggle" disabled value="Pause OR Continue"/>
                            <div class="card-footer p-4 mt-2" style="display: none">
                                <video id="videoPreview" src="" controls style="width: 100%; height: auto;"></video>
                            </div>  
                        </div>
                    </div>

                    </div>
                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" id="video_upload_btn" type="submit">
                            Submit
                        </button>
                        <a href="{{ route('admin.video.index') }}" class="btn btn-secondary waves-effect m-l-5">Cancel</a>
                    </div>

                </form>

				</div>
			</div>	
		</div>
	</div>
</div>


<script type="text/javascript">

function sessionval(sel)
{
    var content_id = sel.value;
    var site_url = location.origin;
    $('#loader').show();
        $.ajax({
              url: site_url+'/admin/get_season_data',
              type: "GET",
              dataType: "JSON",
              data:{
                "id":content_id
              },
              success: function (data) {
                
                //var result = JSON.parse(JSON.stringify(data))

                $('#video_content_id_error').empty();
                $('#video_session_id').attr('disabled',false); 
                $('#video_upload_btn').attr('disabled',false); 
                $('#video_session_id').removeAttr('required');
                $('#video_session_label').html('Season');
                

                var select_option = '<option disabled selected>Select Season</option>';
                
                if(data.status == true){
                    
                    $('#content_type').val(data.data.content_type);

                    if(data.data.content_type == 1){
                        $('#video_session_id').attr('disabled',true); 
                        $('.tv_show_display_section').css('display','none');

                        $('#episode_name_en').removeAttr('required');
                        $('#episode_synopsis_en').removeAttr('required');
                        $('#episode_name_md').removeAttr('required');
                        $('#episode_synopsis_md').removeAttr('required');
                        $('#episode_name_hi').removeAttr('required');
                        $('#episode_synopsis_hi').removeAttr('required');
                        $('#episode_name_sp').removeAttr('required');
                        $('#episode_synopsis_sp').removeAttr('required');
                        $('#episode_name_fr').removeAttr('required');
                        $('#episode_synopsis_fr').removeAttr('required');

                    }else{
                        $.each(data.data.season, function( index, value ) {
                          select_option += '<option value="'+value.id+'">'+value.season['en']+'</option>';
                        });
                        
                        $('#video_session_id').attr('required','required');
                        $('#video_session_label').html('Season<span class="text-danger">*</span>');

                        $('.tv_show_display_section').css('display','');

                        $('#episode_name_en').attr('required','required');
                        $('#episode_synopsis_en').attr('required','required');
                        $('#episode_name_md').attr('required','required');
                        $('#episode_synopsis_md').attr('required','required');
                        $('#episode_name_hi').attr('required','required');
                        $('#episode_synopsis_hi').attr('required','required');
                        $('#episode_name_sp').attr('required','required');
                        $('#episode_synopsis_sp').attr('required','required');
                        $('#episode_name_fr').attr('required','required');
                        $('#episode_synopsis_fr').attr('required','required');
                         
                    }

                }else{
                    if(data.message != ''){
                        $('#video_content_id_error').html(data.message);
                        $('#video_upload_btn').attr('disabled',true); 
                        $('#video_session_id').attr('disabled',true);
                        

                        $('.tv_show_display_section').css('display','none');

                        $('#episode_name_en').removeAttr('required');
                        $('#episode_synopsis_en').removeAttr('required');
                        $('#episode_name_md').removeAttr('required');
                        $('#episode_synopsis_md').removeAttr('required');
                        $('#episode_name_hi').removeAttr('required');
                        $('#episode_synopsis_hi').removeAttr('required');
                        $('#episode_name_sp').removeAttr('required');
                        $('#episode_synopsis_sp').removeAttr('required');
                        $('#episode_name_fr').removeAttr('required');
                        $('#episode_synopsis_fr').removeAttr('required');
                        
                    }else{
                        $('#video_content_id_error').empty();
                        $('#video_session_id').attr('disabled',false);
                        $('#video_upload_btn').attr('disabled',false);
                        
                    }
                }
               
                $('#video_session_id').empty();    
                $('#video_session_id').append(select_option);
                
                $('#loader').hide();

              }
          });  
        
}



var myVideos = [];

window.URL = window.URL || window.webkitURL;

document.getElementById('browseFile').onchange = setFileInfo;

function setFileInfo() {
  var files = this.files;
  myVideos.push(files[0]);
  var video = document.createElement('video');
  video.preload = 'metadata';

  video.onloadedmetadata = function() {
    window.URL.revokeObjectURL(video.src);
    var duration = video.duration;
    myVideos[myVideos.length - 1].duration = duration;
    updateInfos();
  }  
  video.src = URL.createObjectURL(files[0]);;
}

</script>

@endsection

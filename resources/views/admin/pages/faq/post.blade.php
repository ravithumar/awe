@extends('admin.layouts.master')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('adminaddfaq')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
        </div>
	</div>
	<div class="row">
		<div class="col-xl-12">
			<div class="card">
				<div class="card-body" >
                
                <form action="{{ route('admin.faq.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('POST')

                    <div class="row">

                        <div class="col-6">
                            <div class="form-group">
                                <label for="question_en">Question English<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="question_en" id="question_en" placeholder="enter question in english" required></textarea>
                                @error('question_en')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-6">
                            <div class="form-group">
                                <label for="answer_en">Answer English<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="answer_en" id="answer_en" placeholder="enter answer in english" required></textarea>
                                @error('answer_en')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="question_md">Question Mandarin<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="question_md" id="question_md" placeholder="enter question in mandarin" required></textarea>
                                @error('question_md')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-6">
                            <div class="form-group">
                                <label for="answer_md">Answer Mandarin<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="answer_md" id="answer_md" placeholder="enter answer in mandarin" required></textarea>
                                @error('answer_md')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="question_hi">Question Hindi<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="question_hi" id="question_hi" required placeholder="enter question in hindi"></textarea>
                                @error('question_hi')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-6">
                            <div class="form-group">
                                <label for="answer_hi">Answer Hindi<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="answer_hi" id="answer_hi" required placeholder="enter answer in hindi"></textarea>
                                @error('answer_hi')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="question_sp">Question Spanish<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="question_sp" id="question_sp" required placeholder="enter question in spanish"></textarea>
                                @error('question_sp')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-6">
                            <div class="form-group">
                                <label for="answer_sp">Answer Spanish<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="answer_sp" id="answer_sp" required placeholder="enter answer in spanish"></textarea>
                                @error('answer_sp')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label for="question_fr">Question French<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="question_fr" id="question_fr" required placeholder="enter question in french"></textarea>
                                @error('question_fr')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-6">
                            <div class="form-group">
                                <label for="answer_fr">Answer French<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="answer_fr" id="answer_fr" required placeholder="enter answer in french"></textarea>
                                @error('answer_fr')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>
                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                            Submit
                        </button>
                        <a href="{{ route('admin.faq.index') }}" class="btn btn-secondary waves-effect m-l-5">Cancel</a>
                    </div>

                </form>

				</div>
			</div>	
		</div>
	</div>
</div>

@endsection

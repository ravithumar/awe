<html lang="en">
<head>
  <title>AWE</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
  <style type="text/css">
  	.top-padding{
  		padding-top: 100px;
    	padding-bottom: 100px;
  	}
  </style>
</head>
<body>
	<section class="top-padding">
		<div class="container">
			<div class="row mx-2">
				<div class="col-md-12">
					<?php 
						echo !empty($terms_conditions)?$terms_conditions:'';
					?>
				</div>
			</div>
		</div>
	</section>
</body>
</html>
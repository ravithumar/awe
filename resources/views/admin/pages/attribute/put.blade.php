@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                {{ Breadcrumbs::render('admineditattribute')}}
                </div>
                <h4 class="page-title">{{$pageTittle}}</h4>
            </div>
		</div>
	</div>
	<div class="row">
		<div class="col-xl-6">
			<div class="card">
				<div class="card-body" >
                <form action="{{ route('admin.attribute.update',$attribute->id) }}" method="POST"  enctype="multipart/form-data">
                @csrf
                @method('PUT')
                @php
                $name = $attribute->getTranslations('alias');
                @endphp
                    <div class="row">

                        <div class="col-12">
                            <div class="form-group">
                                <label for="name_en">Name English<span class="text-danger">*</span></label>
                                <input type="text" name="name_en" parsley-trigger="change" value="{{ !empty($name['en'])?$name['en']:'' }}" required placeholder="Enter attribute English Name" class="form-control" id="name_en">
                                @error('name_en')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="name_md">Name Mandarin<span class="text-danger">*</span></label>
                                <input type="text" name="name_md" parsley-trigger="change" value="{{ !empty($name['md'])?$name['md']:'' }}" required placeholder="Enter attribute Mandarin Name" class="form-control" id="name_md">
                                @error('name_md')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="name_hi">Name Hindi<span class="text-danger">*</span></label>
                                <input type="text" name="name_hi" parsley-trigger="change" value="{{ !empty($name['hi'])?$name['hi']:'' }}" required placeholder="Enter attribute Hindi Name" class="form-control" id="name_hi">
                                @error('name_hi')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="name_sp">Name Spanish<span class="text-danger">*</span></label>
                                <input type="text" name="name_sp" parsley-trigger="change" value="{{ !empty($name['sp'])?$name['sp']:'' }}" required placeholder="Enter attribute Spanish Name" class="form-control" id="name_sp">
                                @error('name_sp')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label for="name_fr">Name French<span class="text-danger">*</span></label>
                                <input type="text" name="name_fr" parsley-trigger="change" value="{{ !empty($name['fr'])?$name['fr']:'' }}" required placeholder="Enter attribute French Name" class="form-control" id="name_fr">
                                @error('name_fr')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-xl-12">
                            <div class="form-group">
                                <label for="value">Value<span class="text-danger">*</span></label>
                                <select class="form-control select2" data-parsley-errors-container="#value_error" required name="value" id="value" data-placeholder="Select Value">
                                    <option selected disabled></option>
                                    <option value="boolean" <?php echo ($attribute->value == 'boolean')?'selected':''; ?> >Boolean | Ex. TRUE, FALSE</option>
                                    <option value="number" <?php echo ($attribute->value == 'number')?'selected':''; ?>>Number | Ex: 2, 3</option>
                                </select>
                                <div id="value_error"></div>
                                @error('price')
                                    <div class="error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>

                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                            Submit
                        </button>
                        <a href="{{ route('admin.attribute.index') }}" class="btn btn-secondary waves-effect m-l-5">Cancel</a>
                        <!-- <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                            Cancel
                        </button> -->
                    </div>

                </form>

				</div>
			</div>	
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $(".price-touchspin").TouchSpin({
            min: 0,
            max: 1000000000,
            step: 0.1,
            stepinterval: 50,
            decimals: 2,
            maxboostedstep: 10000000,
            prefix: '$'
        });
    });

</script>
@endsection
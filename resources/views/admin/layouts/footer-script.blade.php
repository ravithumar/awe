        <!-- Vendor js -->
        <script src="{{ URL::asset('assets/js/vendor.min.js')}}"></script>

        @yield('script')

        <!-- App js -->
        <script src="{{ URL::asset('assets/js/app.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/parsleyjs/parsleyjs.min.js')}}"></script>
        <script src="{{ URL::asset('assets/js/common.js')}}"></script>
        
        <script src="{{ URL::asset('assets/js/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/jquery-mask-plugin/jquery-mask-plugin.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/notiflix/notiflix-2.1.2.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/select2/select2.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js')}}"></script>

        <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
        <script src="{{ URL::asset('assets/js/pages/form-pickers.init.js')}}"></script>
        <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>

        
       
       
        <script type="text/javascript">
        $(document).ready(function() {
                $('form').parsley();
                $('.select2').select2();
                $('.select2.artist_ids').select2({
                    placeholder: "Select Artist"
                });
                $('.select2.genre_ids').select2({
                    placeholder: "Select Category"
                });
                $('.select2.tag_ids').select2({
                    placeholder: "Select Tags"
                });
                $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                    $(".alert").slideUp(500);
                });

                $(".perc.discount").TouchSpin({
                    min: 0,
                    max: 100,
                    step: 0.1,
                    decimals: 2,
                    boostat: 5,
                    maxboostedstep: 10,
                    postfix: '%'
                });
                $(".stock-touchspin").TouchSpin();


                $("#content_year").datepicker({
                    format: "yyyy",
                    viewMode: "years", 
                    minViewMode: "years",
                    autoclose:true
                });
                
        });

        var path = window.location.pathname;
        $(".metismenu li a[href*='"+path.split('/')[1]+'/'+path.split('/')[2] +"']").addClass("active").closest('li').addClass("active").closest('ul').addClass('in');
        window.Parsley.addValidator('maxFileSize', {
        validateString: function(_value, maxSize, parsleyInstance) {
                if (!window.FormData) {
                alert('You are making all developpers in the world cringe. Upgrade your browser!');
                return true;
                }
                var files = parsleyInstance.$element[0].files;
                return files.length != 1  || files[0].size <= maxSize * 1024 * 1024;
        },
        requirementType: 'integer',
        messages: {
                en: 'This file should not be larger than %s MB',
        }
        });      
        
        </script>
        @yield('script-bottom')

       <div id="loader" style="width: 100%; height: 100%; position: fixed;display: block;top: 0;left: 0;text-align: center;opacity: 1;background-color: #ffffff73;z-index: 99; display: none;">
            <lottie-player src="https://assets10.lottiefiles.com/packages/lf20_vqa0ahvi.json" background="transparent" speed="1" style="width: 250px; height: 250px; position: absolute;top: 36%;left: 46%;z-index: 100;" autoplay loop></lottie-player>
       </div>
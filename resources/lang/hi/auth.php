<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'पासवर्ड और कन्फर्म पासवर्ड एक ही होना चाहिए।',
    'password' => 'दिया गया पासवर्ड गलत है।',
    'throttle' => 'बहुत अधिक लॉगिन प्रयास। कृपया पुनः प्रयास करें :seconds सेकंड।',

];

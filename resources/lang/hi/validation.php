<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'यह :attribute स्वीकार किया जाना चाहिए।',
    'active_url' => 'यह :attribute मान्य URL नहीं है।',
    'after' => 'यह :attribute के बाद की तारीख :date होनी चाहिए।',
    'after_or_equal' => 'यह :attribute के बाद या उसके बराबर की तारीख :date होनी चाहिए।',
    'alpha' => 'यह :attribute केवल अक्षर होने चाहिए।',
    'alpha_dash' => 'यह :attribute केवल अक्षर, संख्याएं, डैश और अंडरस्कोर होना चाहिए।',
    'alpha_num' => 'यह :attribute केवल अक्षर और संख्याएँ होनी चाहिए।',
    'array' => 'यह :attribute एक सरणी होना चाहिए।',
    'before' => 'यह :attribute पहले की तारीख होनी चाहिए :date.',
    'before_or_equal' => 'यह :attribute तारीख से पहले या उसके बराबर :date होनी चाहिए।',
    'between' => [
        'numeric' => 'यह :attribute के बीच :min और :max होनी चाहिए।',
        'file' => 'यह :attribute के बीच :min और :max किलोबाइट होनी चाहिए।',
        'string' => 'यह :attribute के बीच :min और :max पात्र होनी चाहिए।',
        'array' => 'यह :attribute के बीच :min और :max आइटम होनी चाहिए।',
    ],
    'boolean' => 'यह :attribute फ़ील्ड सही या गलत होना चाहिए।',
    // 'confirmed' => 'The :attribute confirmation does not match.',
    'confirmed' => 'पासवर्ड और कन्फर्म पासवर्ड एक ही होना चाहिए',
    'date' => 'यह :attribute मान्य तिथि नहीं है।',
    'date_equals' => 'यह :attribute तारीख बराबर :date होनी चाहिए।',
    'date_format' => 'यह :attribute :format प्रारूप से मेल नहीं खाता',
    'different' => 'यह :attribute और :other अलग होना चाहिए।',
    'digits' => 'यह :attribute अंक :digits होना चाहिए.',
    'digits_between' => 'यह :attribute के बीच :min और :max अंक होना चाहिए।',
    'dimensions' => 'यह :attribute अमान्य छवि आयाम हैं।',
    'distinct' => 'यह :attribute फ़ील्ड में डुप्लिकेट मान है।',
    'email' => 'यह :attribute एक वैध ई - मेल पता होना चाहिए।',
    'ends_with' => 'यह :attribute इनमें से किसी एक के साथ समाप्त following: :values होना चाहिए।',
    'exists' => 'यह चुने गए :attribute अमान्य है।',
    'file' => 'यह :attribute एक फाइल होनी चाहिए।',
    'filled' => 'यह :attribute फ़ील्ड का मान होना चाहिए।',
    'gt' => [
        'numeric' => 'यह :attribute से बड़ा :value होना चाहिए।',
        'file' => 'यह :attribute से बड़ा :value किलोबाइट होना चाहिए।',
        'string' => 'यह :attribute से बड़ा :value पात्र होना चाहिए।',
        'array' => 'यह :attribute से अधिक :value आइटम होना चाहिए।',
    ],
    'gte' => [
        'numeric' => 'यह :attribute से बड़ा or equal :value होना चाहिए।',
        'file' => 'यह :attribute से बड़ा or equal :value किलोबाइट होना चाहिए।',
        'string' => 'यह :attribute से बड़ा or equal :value पात्र होना चाहिए।',
        'array' => 'यह :attribute :value आइटम या अधिक होना आवश्यक है।',
    ],
    'image' => 'यह :attribute एक छवि होनी चाहिए।',
    'in' => 'यह चुने गए :attribute अमान्य है।',
    'in_array' => 'यह :attribute फ़ील्ड :other मौजूद नहीं है।',
    'integer' => 'यह :attribute पूर्णांक होना चाहिए।',
    'ip' => 'यह :attribute एक वैध आईपी पता होना चाहिए।',
    'ipv4' => 'यह :attribute एक मान्य IPv4 पता होना चाहिए।',
    'ipv6' => 'यह :attribute एक मान्य IPv6 पता होना चाहिए।',
    'json' => 'यह :attribute एक वैध JSON स्ट्रिंग होना चाहिए।',
    'lt' => [
        'numeric' => 'यह :attribute से कम :value होना चाहिए।',
        'file' => 'यह :attribute से कम :value किलोबाइट होना चाहिए।',
        'string' => 'यह :attribute से कम :value पात्र होना चाहिए।',
        'array' => 'यह :attribute :value आइटम से कम होना चाहिए।',
    ],
    'lte' => [
        'numeric' => 'यह :attribute से कम या बराबर :value होना चाहिए।',
        'file' => 'यह :attribute से कम या बराबर :value किलोबाइट होना चाहिए।',
        'string' => 'यह :attribute से कम या बराबर :value पात्र होना चाहिए।',
        'array' => 'यह :attribute से अधिक नहीं :value आइटम होना चाहिए।',
    ],
    'max' => [
        'numeric' => 'यह :attribute से बड़ा नहीं :max होना चाहिए।',
        'file' => 'यह :attribute से बड़ा नहीं :max किलोबाइट होना चाहिए।',
        'string' => 'यह :attribute से बड़ा नहीं :max पात्र होना चाहिए।',
        'array' => 'यह :attribute से अधिक नहीं :max आइटम होना चाहिए।',
    ],
    'mimes' => 'यह :attribute type: की फाइल :values होनी चाहिए।',
    'mimetypes' => 'यह :attribute type: की फाइल :values होनी चाहिए।',
    'min' => [
        'numeric' => 'यह :attribute कम से कम :min होना चाहिए।',
        'file' => 'यह :attribute कम से कम :min किलोबाइट होना चाहिए।',
        'string' => 'यह :attribute कम से कम :min पात्र होना चाहिए।',
        'array' => 'यह :attribute कम से कम :min आइटम होना चाहिए।',
    ],
    'multiple_of' => 'यह :attribute बहुत :value होना चाहिए।',
    'not_in' => 'यह चुने हुए :attribute अमान्य है।',
    'not_regex' => 'यह :attribute प्रारूप अमान्य है।',
    'numeric' => 'यह :attribute एक संख्या होनी चाहिए।',
    'password' => 'यह पासवर्ड गलत है।',
    'present' => 'यह :attribute क्षेत्र मौजूद होना चाहिए।',
    'regex' => 'यह :attribute प्रारूप अमान्य है।',
    'required' => 'यह :attribute ये स्थान भरा जाना है।',
    'required_if' => 'यह :attribute फ़ील्ड की आवश्यकता होती है जब :other :value है।',
    'required_unless' => 'यह :attribute फ़ील्ड आवश्यक है जब तक :other :values में है।',
    'required_with' => 'यह :attribute फ़ील्ड की आवश्यकता तब होती है जब :values उपस्थित है।',
    'required_with_all' => 'यह :attribute फ़ील्ड की आवश्यकता होती है जब :values मौजूद हैं।',
    'required_without' => 'यह :attribute फ़ील्ड की आवश्यकता होती है जब :values मौजूद नहीं है।',
    'required_without_all' => 'यह :attribute फ़ील्ड की आवश्यकता होती है जब इनमें से कोई :values मौजूद नहीं हैं।',
    'prohibited' => 'यह :attribute क्षेत्र निषिद्ध है।',
    'prohibited_if' => 'यह :attribute क्षेत्र निषिद्ध है जब :other :value है।',
    'prohibited_unless' => 'यह :attribute क्षेत्र निषिद्ध है जब तक :other :values में है।',
    'same' => 'यह :attribute और :other मेल खाना चाहिए।',
    'size' => [
        'numeric' => 'यह :attribute :size होना चाहिए।',
        'file' => 'यह :attribute :size किलोबाइट होना चाहिए।',
        'string' => 'यह :attribute :size पात्र होना चाहिए।',
        'array' => 'यह :attribute :size आइटम शामिल होना चाहिए।',
    ],
    'starts_with' => 'यह :attribute following: :values इनमें से किसी एक से शुरू होना चाहिए।',
    'string' => 'यह :attribute एक स्ट्रिंग होना चाहिए।',
    'timezone' => 'यह :attribute एक वैध क्षेत्र होना चाहिए।',
    'unique' => 'यह :attribute पहले से ही लिया जा चुका है।',
    'uploaded' => 'यह :attribute अपलोड करने में विफल।',
    'url' => 'यह :attribute प्रारूप अमान्य है।',
    'uuid' => 'यह :attribute एक वैध यूयूआईडी होना चाहिए।',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => '这 :attribute 必须接受。',
    'active_url' => '这 :attribute 不是有效的网址。',
    'after' => '这 :attribute 必须是之后的日期 :date.',
    'after_or_equal' => '这 :attribute 必须是晚于或等于的日期 :date.',
    'alpha' => '这 :attribute 只能包含字母。',
    'alpha_dash' => '这 :attribute 只能包含字母、数字、破折号和下划线。',
    'alpha_num' => '这 :attribute 只能包含字母和数字。',
    'array' => '这 :attribute 必须是一个数组。',
    'before' => '这 :attribute 必须是之前的日期 :date.',
    'before_or_equal' => '这 :attribute 必须是早于或等于的日期 :date.',
    'between' => [
        'numeric' => '这 :attribute 必须介于 :min 和 :max.',
        'file' => '这 :attribute 必须介于 :min 和 :max 千字节。',
        'string' => '这 :attribute 必须介于 :min 和 :max 人物。',
        'array' => '这 :attribute 必须介于 :min 和 :max 项目。',
    ],
    'boolean' => '这 :attribute 字段必须为真或假。',
    // 'confirmed' => '这 :attribute confirmation does not match.',
    'confirmed' => '密码和确认密码应该相同',
    'date' => '这 :attribute 不是有效日期。',
    'date_equals' => '这 :attribute 必须是等于的日期 :date.',
    'date_format' => '这 :attribute 与格式不匹配 :format.',
    'different' => '这 :attribute 和 :other 必须不同。',
    'digits' => '这 :attribute 必须是 :digits 位数。',
    'digits_between' => '这 :attribute 必须介于 :min 和 :max 位数。',
    'dimensions' => '这 :attribute 图片尺寸无效。',
    'distinct' => '这 :attribute 字段具有重复值。',
    'email' => '这 :attribute 必须是一个有效的E-mail地址。',
    'ends_with' => '这 :attribute 必须以其中之一结尾 following: :values.',
    'exists' => '这 选择 :attribute 是无效的。',
    'file' => '这 :attribute 必须是一个文件。',
    'filled' => '这 :attribute 字段必须有值。',
    'gt' => [
        'numeric' => '这 :attribute 必须大于 :value.',
        'file' => '这 :attribute 必须大于 :value 千字节。',
        'string' => '这 :attribute 必须大于 :value 人物。',
        'array' => '这 :attribute 必须有超过 :value 项目。',
    ],
    'gte' => [
        'numeric' => '这 :attribute 必须大于或等于 :value.',
        'file' => '这 :attribute 必须大于或等于 :value 千字节。',
        'string' => '这 :attribute 必须大于或等于 :value 人物。',
        'array' => '这 :attribute 一定有 :value 项目或更多。',
    ],
    'image' => '这 :attribute 必须是图像。',
    'in' => '这 选择 :attribute 是无效的。',
    'in_array' => '这 :attribute 字段不存在于 :other.',
    'integer' => '这 :attribute 必须是整数。',
    'ip' => '这 :attribute 必须是有效的 IP 地址。',
    'ipv4' => '这 :attribute 必须是有效的 IPv4 地址。',
    'ipv6' => '这 :attribute 必须是有效的 IPv6 地址。',
    'json' => '这 :attribute 必须是有效的 JSON 字符串。',
    'lt' => [
        'numeric' => '这 :attribute 必须小于 :value.',
        'file' => '这 :attribute 必须小于 :value 千字节。',
        'string' => '这 :attribute 必须小于 :value 人物。',
        'array' => '这 :attribute 必须少于 :value 项目。',
    ],
    'lte' => [
        'numeric' => '这 :attribute 必须小于或等于 :value.',
        'file' => '这 :attribute 必须小于或等于 :value 千字节。',
        'string' => '这 :attribute 必须小于或等于 :value 人物。',
        'array' => '这 :attribute 不得超过 :value 项目。',
    ],
    'max' => [
        'numeric' => '这 :attribute 不得大于 :max.',
        'file' => '这 :attribute 不得大于 :max 千字节。',
        'string' => '这 :attribute 不得大于 :max 人物。',
        'array' => '这 :attribute 不得超过 :max 项目。',
    ],
    'mimes' => '这 :attribute 必须是一个文件 type: :values.',
    'mimetypes' => '这 :attribute 必须是一个文件 type: :values.',
    'min' => [
        'numeric' => '这 :attribute 必须至少 :min.',
        'file' => '这 :attribute 必须至少 :min 千字节。',
        'string' => '这 :attribute 必须至少 :min 人物。',
        'array' => '这 :attribute 必须至少有 :min 项目。',
    ],
    'multiple_of' => '这 :attribute 必须是的倍数 :value.',
    'not_in' => '这 选择 :attribute 是无效的。',
    'not_regex' => '这 :attribute 格式无效。',
    'numeric' => '这 :attribute 必须是一个数字。',
    'password' => '这 密码不正确。',
    'present' => '这 :attribute 字段必须存在。',
    'regex' => '这 :attribute 格式无效。',
    'required' => '这 :attribute 字段是必需的。',
    'required_if' => '这 :attribute 字段是必需的 :other 是 :value.',
    'required_unless' => '这 :attribute 字段是必需的，除非 :other 在 :values.',
    'required_with' => '这 :attribute 字段是必需的 :values 存在。',
    'required_with_all' => '这 :attribute 字段是必需的 :values 存在。',
    'required_without' => '这 :attribute 字段是必需的 :values 不存在。',
    'required_without_all' => '这 :attribute 当没有 :values 存在。',
    'prohibited' => '这 :attribute 字段被禁止。',
    'prohibited_if' => '这 :attribute 字段被禁止时 :other 是 :value.',
    'prohibited_unless' => '这 :attribute 字段被禁止，除非 :other 在 :values.',
    'same' => '这 :attribute 和 :other 必须匹配。',
    'size' => [
        'numeric' => '这 :attribute 必须是 :size.',
        'file' => '这 :attribute 必须是 :size 千字节。',
        'string' => '这 :attribute 必须是 :size 人物。',
        'array' => '这 :attribute 必须包含 :size 项目。',
    ],
    'starts_with' => '这 :attribute 必须从其中之一开始 following: :values.',
    'string' => '这 :attribute 必须是字符串。',
    'timezone' => '这 :attribute 必须是有效区域。',
    'unique' => '这 :attribute 已有人带走了。',
    'uploaded' => '这 :attribute 上传失败。',
    'url' => '这 :attribute 格式无效。',
    'uuid' => '这 :attribute 必须是有效的 UUID。',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];

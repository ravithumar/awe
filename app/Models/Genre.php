<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;


class Genre extends Model
{

    use HasFactory, SoftDeletes, HasTranslations;
    protected $table = 'genre';
    protected $fillable = [
        'name',
        'content_id'
    ];

    public $translatable = ['name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];


    public function content()
    {
        return $this->belongsToMany(Content::class,'content_genres','genre_id','content_id');
    }

    public function content_data() {
        return $this->belongsTo('App\Models\Content', 'content_id', 'id');
    }

    public function user_genres() {
        return $this->hasOne('App\Models\UserGenres','genre_id','id');
    }

    public function content_genres() {
        return $this->hasMany('App\Models\ContentGenres','genre_id','id');
    }

}

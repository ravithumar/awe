<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContentCast extends Model
{

    use HasFactory;

    public $timestamps = false;
    protected $table = 'content_cast';
    protected $fillable = [
        'content_id',
        'artist_id'
    ];    

    
    public function artist()
    {
        return $this->belongsTo('App\Models\Artist', 'artist_id', 'id');
    }

}

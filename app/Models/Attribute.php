<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Attribute extends Model
{

    use HasFactory,SoftDeletes, HasTranslations;
    protected $table = 'attribute';
    protected $fillable = [
        'name',
        'alias',
        'value'

    ];

    public $translatable = ['alias'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    

    protected $appends = array('alias_name');

    public function getAliasNameAttribute()
    {
        return $this->alias;
    }

}

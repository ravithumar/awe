<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use CommonHelper;

class User extends Authenticatable {
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'first_name',
        'last_name',
        'phone',
        'parent_id',
        'status',
        'lock_password',
        'profile_picture',        
        'remember_token',        
        'social_type',
        'social_id',
        'language_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function hasRole() {
        $groups = DB::table('users_group')->whereUserId($this->id)->first();
        return !empty($groups) ? $groups->group_id : false;
    }
    public function isActive() {
        if($this->status == 1){
            return true;
        }else{
            return false;
        }
    }

    public function getProfilePictureFullUrlAttribute() {
        return $this->profile_picture == null ? url('/images/default.png') : env('AWS_S3_URL').env('AWS_S3_MODE').'/users/'.$this->profile_picture;
    }

    public function devices() {
        return $this->hasOne('App\Models\Devices', 'user_id', 'id');
    }

    public function preferred_languages() {
        return $this->hasOne('App\Models\PreferredLanguage', 'user_id', 'id');
    }

    /**
     *  revokes all login tokens
     */
    public function revokeAllTokens()
    {
        foreach ($this->tokens as $token) {
                $token->revoke();
        }
    }

    public function accessTokens()
    {
        return $this->hasMany('App\OauthAccessToken');
    }

    protected $appends = array('group','phone_formatted', 'profile_picture_full_url');

   
    public function getPhoneFormattedAttribute()
    {
        if (isset($this->phone) && $this->phone != "") {
            $phone = CommonHelper::SetPhoneFormat($this->phone);
            return $phone;
        }else{
            return '';
        }
    }
    public function getGroupAttribute()
    {
        $user = UsersGroup::where('user_id',$this->id)->first(); 
        return $user->group_id;
    }


    public function language() {
        return $this->belongsTo('App\Models\Language', 'language_id', 'id');
    }

    public function plan() {
        return $this->belongsTo('App\Models\Plan', 'plan_id', 'id');
    }

    public function mylist() {
        return $this->hasMany('App\Models\MyList', 'user_id', 'id');
    }
    
    public function activity_log() {
        return $this->hasMany('App\Models\ActivityLog', 'user_id', 'id');
    }

    public function usergenres() {
        return $this->hasMany('App\Models\UserGenres', 'user_id', 'id');
    }

    public function useragerestriction() {
        return $this->hasMany('App\Models\UserAgeRestriction', 'user_id', 'id');
    }
    
    public function userrestrictedcontent() {
        return $this->hasMany('App\Models\UserRestrictedContent', 'user_id', 'id');
    }
    
    public function subuser() {
        return $this->hasMany('App\Models\User', 'parent_id', 'id');
    }

    public function parent() {
        return $this->belongsTo('App\Models\User', 'parent_id', 'id');
    }

}

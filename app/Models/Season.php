<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Season extends Model
{

    use HasFactory, SoftDeletes, HasTranslations;
    public $timestamps = false;

    protected $table = 'seasons';
    protected $fillable = [
        'content_id',
        'season'
    ];
    
    public $translatable = ['season'];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public function content_data()
    {
        return $this->belongsTo(Content::class, 'content_id');
    }
    
    public function episode()
    {
        return $this->hasMany(Video::class, 'season_id');
    }
    
}

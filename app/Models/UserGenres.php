<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserGenres extends Model
{

    use HasFactory;
    public $timestamps = false;
    protected $table = 'user_genres';
    protected $fillable = [
        'user_id',
        'genre_id'
    ];


    public function genre()
    {
        return $this->belongsTo('App\Models\Genre', 'genre_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

}

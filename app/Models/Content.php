<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Content extends Model
{

    use HasFactory, SoftDeletes, HasTranslations;
    public $timestamps = false;
    protected $table = 'content';
    protected $fillable = [
        'name',
        'is_free',
        'age_rating_id',
        'poster',
        'likes',
        'logo',
        'year',
        'synopsis',
        'trailer',
        'content_type'
    ];

    public $translatable = ['name','synopsis'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function getPosterAttribute($poster) {
        return $poster == null ? url('/images/default.png') : env('AWS_S3_URL').'content/'.$poster;
    }

    public function getLogoAttribute($logo) {
        return $logo == null ? url('/images/default.png') : env('AWS_S3_URL').'content/'.$logo;
    }
    
    /*protected $appends = array('content_name','content_synopsis');


    public function getContentNameAttribute()
    {   
        return $this->name;
    }
    public function getContentSynopsisAttribute()
    {   
        return $this->synopsis;
    }*/

    
    public function content_cast()
    {
        return $this->hasMany('App\Models\ContentCast', 'content_id', 'id');
    }


    public function content_tags()
    {
        return $this->hasMany('App\Models\ContentTags', 'content_id', 'id');
    }

    public function content_genres()
    {
        return $this->hasMany('App\Models\ContentGenres', 'content_id', 'id');
    }

    public function season()
    {
        return $this->hasMany('App\Models\Season', 'content_id', 'id');
    }


    public function video()
    {
        return $this->hasOne('App\Models\Video', 'content_id', 'id');
    }

    public function episode()
    {
        return $this->hasMany('App\Models\Video', 'content_id', 'id');
    }

    public function genres()
    {
        return $this->belongsToMany(Genre::class,'content_genres','content_id','genre_id');
    }
    

    public function ageratings()
    {
        return $this->belongsTo('App\Models\AgeRatings', 'age_rating_id', 'id');
    }
    
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Faq extends Model
{

    use HasFactory, SoftDeletes, HasTranslations;
    public $timestamps = false;
    protected $table = 'faq';
    protected $fillable = [
        'question',
        'answer'
    ];

    public $translatable = ['question','answer'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    
}

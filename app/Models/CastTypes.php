<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class CastTypes extends Model
{
    use HasFactory, SoftDeletes, HasTranslations;
    public $timestamps = false;
    protected $table = 'cast_types';
    protected $fillable = [
        'name',
    ];

    public $translatable = ['name'];
    
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    // public function artists() {
    //     return $this->hasMany('App\Models\Artists', 'cast_type_id', 'id');
    // }

    public function artists()
    {
        return $this->hasMany(Artists::class);
    }

}

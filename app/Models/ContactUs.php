<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactUs extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'contact_us';

    public function user_name(){
        return $this->belongsTo(User::class, 'user_id', 'id'); 
    }

    public function getDocumentAttribute($document) {
        return $document == null ? '' : 'images/contact_us/'.$document;
    }


}

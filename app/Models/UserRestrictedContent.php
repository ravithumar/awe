<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class UserRestrictedContent extends Model {
    
    use HasFactory;

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    protected $table = 'user_restricted_content';
    protected $fillable = [
        'user_id',
        'content_id'
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function content() {
        return $this->hasOne('App\Models\Content', 'id', 'content_id');
    }
    
}

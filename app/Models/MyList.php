<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class MyList extends Model {
    
    use HasFactory, SoftDeletes;

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    protected $table = 'my_list';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    public function content() {
        return $this->hasOne('App\Models\Content', 'id', 'content_id');
    }
    
}

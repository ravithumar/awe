<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Feedback extends Model
{

    use HasFactory, SoftDeletes;
    public $timestamps = false;
    protected $table = 'feedback';
    protected $fillable = [
        'user_id',
        'faq_id',
        'description'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];


    public function user() {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function faq() {
        return $this->hasOne('App\Models\Faq', 'id', 'faq_id');
    }
    
}

<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class AgeRatings extends Model
{
    use HasFactory, SoftDeletes, HasTranslations;
    public $timestamps = false;
    protected $table = 'age_ratings';
    protected $fillable = [
        'name',
        'age',
        'children',
        'description'
    ];

    public $translatable = ['name','description'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}

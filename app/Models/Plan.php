<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

class Plan extends Model
{

    use HasFactory,SoftDeletes, HasTranslations;
    protected $table = 'plan';
    protected $fillable = [
        'name',
        'price'

    ];

    public $translatable = ['name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class,'plan_attribute', 'attribute_id', 'plan_id')->withPivot('value');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Video extends Model
{

    use HasFactory, SoftDeletes, HasTranslations;
    public $timestamps = false;
    protected $table = 'videos';
    protected $fillable = [
        'content_id',
        'season_id',
        'video',
        'thumbnail_image',
        'episode_name',
        'episode_synopsis',
        'duration'
    ];
    
    public $translatable = ['episode_name','episode_synopsis','name'];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function video_subtitle()
    {
        return $this->hasMany('App\Models\VideoSubtitle', 'video_id', 'id');
    }
    public function content()
    {
        return $this->belongsTo('App\Models\Content', 'content_id', 'id');
    }
    public function season()
    {
        return $this->belongsTo('App\Models\Season', 'season_id', 'id');
    }

}

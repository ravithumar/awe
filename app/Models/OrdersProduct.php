<?php

namespace App\Models;



use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;



class OrdersProduct extends Model

{

    use HasFactory;

    protected $table = 'order_products';

    protected $fillable = [

        'order_id',

        'date_id',

        'product_id',

        'kitchen_id',

        'category_id'

    ];







    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */

    protected $hidden = [

        'created_at',

        'updated_at'

    ];



    protected $appends = array('category_name','product_name','image','kitchen_id');

    public function varient()
    {
        return $this->belongsTo('App\Models\Variant', 'variant_id', 'id');
    }   

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }  

    public function getCategoryNameAttribute()

    {

        $category = Category::find($this->category_id);  

        return $category == null ? '' : $category->name;  

    }



    public function getProductNameAttribute()

    {

        $product = Product::find($this->product_id);  

        return $product == null ? '' : $product->name;  

    }

    public function getImageAttribute()

    {

        $product = Product::find($this->product_id);  

        return $product == null ? '' : $product->image;  

    }



    public function getkitchenIdAttribute()

    {

        $product = Product::find($this->product_id);  

        return $product == null ? '' : $product->kitchen_id;  

    }





}


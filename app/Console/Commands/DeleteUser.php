<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use Carbon\Carbon;


class DeleteUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deleteuser:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete parent user with sub user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $deleted_user_ids = User::select('id','deactivated_time')->where('parent_id',0)
                            ->where('deactivated_time', '!=',NULl)
                            ->where('status',0)
                            ->get()->toArray();
        
        
        $user_ids = array();
        if(!empty($deleted_user_ids) && count($deleted_user_ids)>0){
            foreach ($deleted_user_ids as $key => $value) {
                if(date('Y-m-d',strtotime(Carbon::now()->addDays(30))) <= date('Y-m-d',strtotime($value['deactivated_time']))){
                    $user_ids[$key] = $value['id'];
                }
            }
        }

        if(!empty($user_ids) && count($user_ids)>0){
            User::whereIn('id', $user_ids)->orWhereIn('parent_id', $user_ids)->delete();
        }
        

        return true;
    }
}
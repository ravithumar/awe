<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Models\FavoriteVideo;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FavoriteVideoController extends Controller {

    use ApiResponser;

    public function index(Request $request) {

        return response([
            'status' => true,
            'message' => '',
            'data' => '',
        ]);

    }

    /**
     *  makes a service provider favourite or removes him from favourite
     */


    public function like(Request $request) {

        $validator = Validator::make(request()->all(), [
            'content_id' => 'required',
            'status' => 'required|integer|between:0,1',
        ]);

        if (!$validator->fails()) {
            if (Content::whereId($request->content_id)->where('status','!=','2')->first()) {
                if($request->status == '1'){

                    if (FavoriteVideo::whereContentId($request->content_id)->whereUserId($request->user()->id)->first()) {
                        /*return response([
                            'status' => false,
                            'message' => __('message.favorite_video_exits'), //'Video already exits',
                        ]);*/

                        FavoriteVideo::whereContentId($request->content_id)->whereUserId($request->user()->id)->update(['status'=>1]);
                        
                        return response([
                            'status' => true,
                            'message' => __('message.favorite_video_success'), //'Video already exits',
                        ]);

                    } else {
                        $favorite_video = new FavoriteVideo;
                        $favorite_video->user_id = $request->user()->id;
                        $favorite_video->content_id = $request->content_id;
                        $favorite_video->status = 1;
                        $favorite_video->save();

                        return response([
                            'status' => true,
                            'message' => __('message.favorite_video_success'), // 'Video add successfully',
                        ]);

                    }
                }else{

                    FavoriteVideo::whereContentId($request->content_id)->whereUserId($request->user()->id)->where('status',1)->delete();
                    
                    return response([
                        'status' => true,
                        'message' => __('message.favorite_video_remove'), //'Video remove successfully',
                    ]);

                }
            } else {
                return response([
                    'status' => false,
                    'message' => __('message.video_not_found'), //'Video not found',
                ]);
            }
        }
        return $this->errorResponse($validator->messages(), true);

    }


    

    public function dislike(Request $request) {

        $validator = Validator::make(request()->all(), [
            'content_id' => 'required',
            'status' => 'required|integer|between:0,1',
        ]);

        if (!$validator->fails()) {
            if (Content::whereId($request->content_id)->where('status','!=','2')->first()) {
                if($request->status == '1'){
                    
                    if (FavoriteVideo::whereContentId($request->content_id)->whereUserId($request->user()->id)->first()) {
                        
                        FavoriteVideo::whereContentId($request->content_id)->whereUserId($request->user()->id)->update(['status'=>2]);

                        return response([
                            'status' => true,
                            'message' => __('message.favorite_video_remove'), //'Video already exits',
                        ]);
                    } else {
                        $favorite_video = new FavoriteVideo;
                        $favorite_video->user_id = $request->user()->id;
                        $favorite_video->content_id = $request->content_id;
                        $favorite_video->status = 2;
                        $favorite_video->save();

                        return response([
                            'status' => true,
                            'message' => __('message.favorite_video_remove'), // 'Video add successfully',
                        ]);

                    }

                }else{

                    FavoriteVideo::whereContentId($request->content_id)->whereUserId($request->user()->id)->where('status',2)->delete();
                    
                    return response([
                        'status' => true,
                        'message' => __('message.favorite_video_remove'), //'Video remove successfully',
                    ]);

                }
            } else {
                return response([
                    'status' => false,
                    'message' => __('message.video_not_found'), //'Video not found',
                ]);
            }
        }
        return $this->errorResponse($validator->messages(), true);

    }

}

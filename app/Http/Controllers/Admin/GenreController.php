<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\CommonHelper;

use Illuminate\Http\Request;
use App\Models\Genre;
use App\Models\Content;
use App\Models\Product;
use App\Models\User;
use DataTables;
use Illuminate\Support\Facades\Auth;

class GenreController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		

	   if ($request->ajax())
		{
			$data = Genre::with(['content_data','content_genres'])->get();
			
			return Datatables::of($data)
			->addIndexColumn()
			->editColumn('action', function ($row){
				$btn = '<a href="'.route('admin.genre.edit',$row['id']).'" class="mr-2"><i class="fa fa-edit"></i></a>';
				$btn .= '<a href="'.route('admin.genre.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
				$btn .= '<a href="'.route('admin.genre.destroy', $row['id']).'" data-url="genre" data-id="'.$row["id"].'" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="'.csrf_token().'" ><i class="fa fa-trash"></i></a>';
				return $btn;
			})
			->editColumn('name', function ($row){
				return $row['name'];
			})
			->editColumn('total_content', function ($row){
				return count($row['content_genres']);
			})
			->editColumn('content_name', function ($row){
				
				if(!empty($row['content_data']['name'])){
                    $content_name = strlen($row['content_data']['name']) > 50 ? substr($row['content_data']['name'],0,50)."..." : $row['content_data']['name'];
                  return '<a target="_blank" href="'.route('admin.content.show',$row['content_data']['id']).'" class="mr-2">'.$content_name.'</a>';
                }else{
                    $content_name = '';
                  return '';
                }  

			})
			->rawColumns(['name','content_name','total_content', 'action'])
			->make(true);
	   }
	   else
	   {
		   $columns = [
			   ['data' => 'id','name' => 'id','title' => "Id"], 
			   ['data' => 'name','name' => 'name', 'title' => __("Name")],
			   ['data' => 'content_name','name' => 'content_name', 'title' => __("Content Name")],
			   ['data' => 'total_content','name' => 'total_content', 'title' => __("Total Content")],
			   ['data' => 'action','name' => 'action', 'title' => "Action",'searchable'=>false,'orderable'=>false]];
		   $params['dateTableFields'] = $columns;
		   $params['dateTableUrl'] = route('admin.genre.index');
		   $params['dateTableTitle'] = "Genre Management";
		   $params['dataTableId'] = time();
		   $params['addUrl'] = route('admin.genre.create');
		   return view('admin.pages.genre.index',$params);
	   }
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$params['pageTittle'] = "Add Genre" ;
		$params['content'] = Content::select('id','name')->where('status','!=',2)->get();
		$params['backUrl'] = route('admin.genre.index');

		return view('admin.pages.genre.post',$params);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		
		$request->validate([
			'name_en' => 'required',
			'name_hi' => 'required',
			'name_fr' => 'required',
			'name_md' => 'required',
			'name_sp' => 'required'
		]);
		 
		$genre = Genre::create([
			'name' => [
					'en' => $request->name_en,
	      			'hi' => $request->name_hi,
	      			'fr' => $request->name_fr,
	      			'md' => $request->name_md,
	      			'sp' => $request->name_sp
				],
			'content_id' =>$request->content_id,
		]);

		// redirect
		return redirect()->route('admin.genre.index')->with('success','Genre created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$params['pageTittle'] = "View Genre" ;
		$params['genre'] = Genre::with(['content_data','content_genres'])->find($id);
		$params['backUrl'] = route('admin.genre.index');

		return view('admin.pages.genre.view',$params);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$params['pageTittle'] = "Edit Genre";
		$params['genre'] = Genre::find($id);
		$params['content'] = Content::select('id','name')->where('status','!=',2)->get();
		$params['backUrl'] = route('admin.genre.index');
		return view('admin.pages.genre.put',$params);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
	
		$request->validate([
			'name_en' => 'required',
			'name_hi' => 'required',
			'name_fr' => 'required',
			'name_md' => 'required',
			'name_sp' => 'required'
		]);

		$genre['name'] =array(
				'en' => $request->name_en,
      			'hi' => $request->name_hi,
      			'fr' => $request->name_fr,
      			'md' => $request->name_md,
      			'sp' => $request->name_sp
			);

		$genre['content_id'] = !empty($request->content_id)?$request->content_id:NULL;
		
		Genre::whereId($id)->update($genre);
	
		return redirect()->route('admin.genre.index')
						->with('success','Genre updated successfully');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		// Genre::whereId($id)->delete();
		$products = Product::whereGenreId($id)->get();		
		if (isset($products) && !empty($products) && count($products) > 0) {
            $data['status'] = false;
			$data['message'] = 'You can not delete as Product(s) already exist for this genre';        
		} else
		{
			Genre::whereId($id)->delete();            
			$data['status'] = TRUE;        
			$data['message'] = 'Deleted';
		}
        echo json_encode($data);
		// return redirect()->route('admin.genre.index')->with('success','Genre deleted successfully');

	}

	public function active_deactive_genre()
	{
		if($_POST['table'] == 'genre'){
			if($_POST['status'] == 0){
				$status = 1;
			}else{
				$status = 0;
			}
			Genre::where('id', $_POST['id'])->update(['status' => $status]);
		}
		echo $status;
	}
}

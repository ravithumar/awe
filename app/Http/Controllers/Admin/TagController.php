<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\CastTypes;
use App\Models\Tag;
use Illuminate\Http\Request;
use DataTables;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

       if ($request->ajax())
       {
            $data = Tag::get();
            return Datatables::of($data)
            ->editColumn('action', function ($row){
				$btn = '<a href="'.route('admin.tags.edit',$row['id']).'" class="mr-2"><i class="fa fa-edit"></i></a>';
				$btn .= '<a href="'.route('admin.tags.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
				$btn .= '<a href="'.route('admin.tags.destroy', $row['id']).'" data-url="tags" data-id="'.$row["id"].'" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="'.csrf_token().'" ><i class="fa fa-trash"></i></a>';
				return $btn;
			})
            ->editColumn('name', function ($row){
                return $row['name'];
            })
			->rawColumns([ 'action'])
            ->make(true);
       }
       else
       {
           $columns = [
            ['data' => 'id','name' => 'id','title' => "Id"], 
            ['data' => 'name','name' => 'name', 'title' => __("Name")],
            ['data' => 'action','name' => 'action', 'title' => "Action",'searchable'=>false,'orderable'=>false]];
           $params['dateTableFields'] = $columns;
           $params['dateTableUrl'] = route('admin.tags.index');
           $params['dateTableTitle'] = "Tags";
           $params['dataTableId'] = time();
           $params['addUrl'] = route('admin.tags.create');
           return view('admin.pages.tags.index',$params);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
	{
		$params['pageTittle'] = "Add Tag" ;
		$params['backUrl'] = route('admin.tags.index');
		return view('admin.pages.tags.post',$params);
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
	{	
		$request->validate([
			'name_en' => 'required',
            'name_hi' => 'required',
            'name_fr' => 'required',
            'name_md' => 'required',
            'name_sp' => 'required'
		]);

		Tag::create([
			'name' => [
                'en' => $request->name_en,
                'hi' => $request->name_hi,
                'fr' => $request->name_fr,
                'md' => $request->name_md,
                'sp' => $request->name_sp
            ]
		]);

		return redirect()->route('admin.tags.index')->with('success','Tag created successfully.');
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
	{
		$params['pageTittle'] = "View Tag" ;
		$params['tagValue'] = Tag::find($id);
		$params['backUrl'] = route('admin.tags.index');
		return view('admin.pages.tags.view',$params);
	}


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
	{
		$params['pageTittle'] = "Edit Tag";
		$params['tagValue'] = Tag::find($id);
		$params['backUrl'] = route('admin.tags.index');
		return view('admin.pages.tags.put',$params);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
	{
		$request->validate([
			'name_en' => 'required',
            'name_hi' => 'required',
            'name_fr' => 'required',
            'name_md' => 'required',
            'name_sp' => 'required'
		]);
		$tagValue['name'] = array(
                'en' => $request->name_en,
                'hi' => $request->name_hi,
                'fr' => $request->name_fr,
                'md' => $request->name_md,
                'sp' => $request->name_sp
            );
        
		Tag::whereId($id)->update($tagValue);
		return redirect()->route('admin.tags.index')->with('success','Tag updated successfully');
	}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
	{	
        Tag::whereId($id)->delete();
        return redirect()->route('admin.tags.index')->with('success','Tag deleted successfully');
	}
}

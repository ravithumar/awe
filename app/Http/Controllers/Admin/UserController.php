<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str; 
use App\Models\UsersGroup;
use App\Models\Devices;
use App\Models\Group;
use App\Models\User;
use App\Models\AppleDetail;
use Carbon\Carbon;
use CommonHelper;
use DataTables;

// use App\Traits\DatatableTraits;

class UserController extends Controller {
    // use DatatableTraits;

    public function index(Request $request) 
    {

        // event(new UserNotify(User::find(1)));
        if ($request->ajax()) {
            //    $data = User::select('*')->where('id','!=',1)->get();
            $data = User::select('users.*', 'users_group.group_id')->join('users_group', 'users.id', '=', 'users_group.user_id')->where('users_group.group_id',4)->get();
            $datat = Datatables::of($data);

            if ($request->has('groups')) {
                $datat->filter(function ($instance) use ($request) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        if ($request->get('groups') == "all") {
                            return true;
                        }
                        return $row['group_id'] == $request->get('groups') ? true : false;
                    });
                    
                });
            }

            return $datat->addIndexColumn()
            ->editColumn('first_name', function ($row)
            {
                return strlen($row['first_name']) > 50 ? substr($row['first_name'],0,50)."..." : $row['first_name'];
            })
            ->editColumn('last_name', function ($row)
            {
                return strlen($row['last_name']) > 50 ? substr($row['last_name'],0,50)."..." : $row['last_name'];
            })
            ->editColumn('is_parent', function ($row)
            {
                if($row['parent_id'] == 0){
                    return "Main";
                }else{
                    return "Sub";
                }
            })
            ->editColumn('action', function ($row)
            {
               
               $btn = '<a href="'.route('admin.user.show', $row['id']) . '" class="mr-2"><i class="fa fa-eye"></i></a>';
               $btn .= '<a href="' . route('admin.user.edit', $row['id']) . '" class="mr-2"><i class="fa fa-edit"></i></a>';
               return $btn;
            })


            ->editColumn('status', function ($row) {
                    if ($row['group'] == 6) {
                        if ($row['status'] == 0) {
                            return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '" data-token="' . csrf_token() . '"   class="btn btn-danger btn-xs waves-effect waves-light" data-table="users" data-status="2">In Active</button>';
                        }
                    }
                    if ($row['status'] == 0) {
                        return '<span class="badge bg-soft-warning text-warning">Pending</span>';
                    } elseif ($row['status'] == 1) {
                        return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '" data-token="' . csrf_token() . '"   class="btn btn-success btn-xs waves-effect waves-light" data-table="users" data-status="' . $row['status'] . '">Active</button>';
                    } else {
                        return '<button onclick="active_deactive(this);" data-id="' . $row['id'] . '" data-token="' . csrf_token() . '"  class="btn btn-danger btn-xs waves-effect waves-light" data-table="users" data-status="' . $row['status'] . '">Inactive</button>';
                    }
                })
                ->rawColumns(['status','user_type' ,'action', 'group','is_parent'])
                ->make(true);

        } else {
            $columns = [
                ['data' => 'id', 'name' => 'id', 'title' => "Id"],
                ['data' => 'first_name', 'name' => 'first_name', 'title' => __("First Name")],
                ['data' => 'last_name', 'name' => 'last_name', 'title' => __("Last Name")],
                ['data' => 'email', 'name' => 'email', 'title' => __("Email")],
                ['data' => 'is_parent', 'name' => 'is_parent', 'title' => __("Type"),'searchable'=>false],
                ['data' => 'status', 'title' => __("Status"), 'searchable' => false],
                ['data' => 'action', 'name' => 'action', 'title' => "Action", 'searchable' => false, 'orderable' => false]];
            $groups = Group::get();
            $params['dateTableFields'] = $columns;
            $params['dateTableUrl'] = route('admin.users.index');
            $params['dateTableTitle'] = "User Management";
            $params['dataTableId'] = time();
            $params['groups'] = $groups;
            return view('admin.pages.users.index', $params);
        }
    }

    public function show($id) {
        $params['pageTittle'] = "View User";

        $user = User::with(['language','plan','usergenres.genre','useragerestriction.ageratings','userrestrictedcontent.content','mylist.content','subuser.usergenres.genre','activity_log.content.video','parent'])->where('id', $id)->first();

        $user_group = UsersGroup::where('user_id', $id)->first();

        $params['user'] = $user->toArray();
        //dd($params); 
        $params['user_group'] = $user_group->toArray();
        $params['backUrl'] = route('admin.users.index');
        return view('admin.pages.users.view', $params);
    }

    public function edit($id)
    {
        
        $params['pageTittle'] = "Edit User";
        $params['user'] = User::find($id);
        $params['backUrl'] = route('admin.users.index');
        return view('admin.pages.users.put',$params);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'image' => 'mimes:jpeg,png,jpg|max:2048',
            // 'email' => 'required|email|unique:users,email,'.$id,
            // 'phone' => 'required|min:14|max:16|unique:users,phone,'.$id,
        ]);        

        if(isset($request->image) && $request->image != ''){
            //$dir ="images/users";
            //$image = CommonHelper::imageUpload($request->image,$dir);
            
            $dir = "users"; 
            $image = CommonHelper::s3Upload($request->image, $dir);   
            $user['profile_picture'] = $image;
        }
        
        $user['first_name'] = $request->first_name;
        $user['last_name'] = $request->last_name;
        // $user['email'] = $request->email;
        // $user['phone'] = $request->phone;

        User::whereId($id)->update($user);

        return redirect()->route('admin.users.index')
                        ->with('success','User updated successfully');
    }


    public function destroy($id)
    {
        $user = User::where('id', $id)->first();                
        if (isset($user)) {
            $user->revokeAllTokens();
        }
        User::whereId($id)->delete();
        AppleDetail::whereEmail($user->email)->delete();
        return redirect()->route('admin.store.index')
                        ->with('success','User deleted successfully');
    }



    public function active_deactive() {
        if ($_POST['table'] == 'users') {
            if ($_POST['status'] == 1) {
                $status = 2;
            } else {
                $status = 1;
            }

            $user = User::where('id', $_POST['id'])->first();
            if (isset($user)) {
                if($status == '2'){
                    // $devices = Devices::where('user_id', $_POST['id'])->update([
                    //     'name' => '',
                    //     'token' => '',
                    //     'type' => '',
                    // ]);
                    $user->revokeAllTokens();
                }
                User::where('id', $_POST['id'])->update(['status' => $status]);

                if($user->deactivated_time != NULL){
                    User::where('id', $_POST['id'])->update(['deactivated_time'=>NULL]);

                    $reactive_user_ids = User::where('parent_id', $_POST['id'])->where('deactivated_time','=',NULL)->get()->pluck('id')->toArray();   
                    if(!empty($reactive_user_ids)){
                        User::whereIn('id', $reactive_user_ids)->update(['status' => 1]);
                    }

                }
            }
        }
        echo $status;
    }

}

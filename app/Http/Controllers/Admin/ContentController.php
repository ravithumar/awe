<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Models\Content;
use App\Models\AgeRatings;
use App\Models\Artist;
use App\Models\Tag;
use App\Models\Genre;
use App\Models\ContentCast;
use App\Models\ContentGenres;
use App\Models\ContentTags;
use App\Models\SystemConfig;
use App\Models\Video;
use App\Models\Season;
use DataTables;
use Illuminate\Support\Facades\Auth;


class ContentController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {        
       if ($request->ajax())
       {
            $data = Content::with(['ageratings'])->get();
        
            return Datatables::of($data)
            ->editColumn('is_free', function ($row)
            {
                if($row['is_free'] == 0){
                    return 'No';
                }elseif($row['status'] == 1){
                    return 'Yes';
                }else{
                    return null;
                }
            })
            ->editColumn('content_type', function ($row)
            {
                if($row['content_type'] == 1){
                    return 'Movie';
                }elseif($row['content_type'] == 2){
                    return 'Tv Show';
                }else{
                    return '';
                }
            })
            ->editColumn('status', function ($row)
            {
                if($row['status'] == 1){
                    return 'Active';
                }elseif($row['status'] == 2){
                    return 'Deactivated';
                }else{
                    return 'Upcoming';
                }
            })
            ->editColumn('advertisement', function ($row)
            {
                $is_adv = SystemConfig::where('path','adv_content_id')->where('value',$row['id'])->first();

                 if($is_adv){
                     return '<div class="col-md-6">
                            <div class="form-group">
                                <label class="switch">
                                  <input type="checkbox" name="adv_content_id" checked class="form-control" data-url="content" data-method="adv_content_change" data-token="'.csrf_token().'" data-id="'.$row["id"].'" onchange="adv_content_change(this); return false;">
                                  <span class="slider round"></span>
                                </label>
                            </div>
                        </div>';
                 }else{
                     return '<div class="col-md-6">
                            <div class="form-group">
                                <label class="switch">
                                  <input type="checkbox" name="adv_content_id" class="form-control" data-url="content" data-method="adv_content_change" data-token="'.csrf_token().'" data-id="'.$row["id"].'" onchange="adv_content_change(this); return false;">
                                  <span class="slider round"></span>
                                </label>
                            </div>
                        </div>';
                 }
            })
            ->editColumn('child_advertisement', function ($row)
            {   
                if($row['ageratings']['age'] <= 14){
                    $is_child_adv = SystemConfig::where('path','child_adv_content_id')->where('value',$row['id'])->first();

                    if($is_child_adv){
                        return '<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="switch">
                                          <input type="checkbox" name="adv_content_id" checked class="form-control" data-url="content" data-method="child_adv_content_change" data-token="'.csrf_token().'" data-id="'.$row["id"].'" onchange="child_adv_content_change(this); return false;">
                                          <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>';
                     }else{
                        return '<div class="col-md-6">
                                    <div class="form-group">
                                        <label class="switch">
                                          <input type="checkbox" name="adv_content_id" class="form-control" data-url="content" data-method="child_adv_content_change" data-token="'.csrf_token().'" data-id="'.$row["id"].'" onchange="child_adv_content_change(this); return false;">
                                          <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>';
                     }
                }else{
                    return null;
                }
                
            })
            ->editColumn('name', function ($row){
                return strlen($row['name']) > 50 ? substr($row['name'],0,50)."..." : $row['name'];
            })
            ->editColumn('action', function ($row){
                $btn = '<a href="'.route('admin.content.edit',$row['id']).'" class="mr-2"><i class="fa fa-edit"></i></a>';
                $btn .= '<a href="'.route('admin.content.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
                $btn .= '<a href="'.route('admin.content.destroy', $row['id']).'" data-url="content" data-id="'.$row["id"].'" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="'.csrf_token().'" ><i class="fa fa-trash"></i></a>';
                
                return $btn;
            })
            ->rawColumns(['action','id','name','is_free','content_type','status','advertisement','child_advertisement'])
            
            ->make(true);


       }
       else
       {
           $columns = [
               ['data' => 'id','name' => 'id','title' => "Id"],
               ['data' => 'name', 'name' => 'name','title' => __("Name"),'searchable'=>true ], 
               ['data' => 'is_free', 'name' => 'is_free','title' => __("Is Free"),'searchable'=>true ], 
               ['data' => 'content_type', 'name' => 'content_type','title' => __("Content Type"),'searchable'=>true ], 
               ['data' => 'status','title' => __("Status"),'searchable'=>false],
               ['data' => 'advertisement','title' => __("Advertisement"),'searchable'=>false],
               ['data' => 'child_advertisement','title' => __("Child Advertisement"),'searchable'=>false],
               ['data' => 'action','name' => 'action', 'title' => "Action",'searchable'=>false,'orderable'=>false]];
           $params['dateTableFields'] = $columns;
           $params['dateTableUrl'] = route('admin.content.index');
           $params['dateTableTitle'] = "Content Management";
           $params['dataTableId'] = time();
           $params['addUrl'] = route('admin.content.create');
           return view('admin.pages.content.index',$params);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $age_rating = AgeRatings::select('id', 'name','age')->get();
        $artist = Artist::select('id', 'name')->get();
        $tag = Tag::select('id', 'name')->get();
        $genre = Genre::select('id', 'name')->get();

        $params['age_rating'] = $age_rating;
        $params['artist'] = $artist;
        $params['tag'] = $tag;
        $params['genre'] = $genre;

        $params['content_status'] = config('app.content_status');
        $params['content_type'] = config('app.content_type');

        $params['pageTittle'] = "Add Content" ;
        $params['backUrl'] = route('admin.content.index');

        //dd($params);

        return view('admin.pages.content.post',$params);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

        $request->validate([
            'name_en' => 'required',
            'name_hi' => 'required',
            'name_fr' => 'required',
            'name_md' => 'required',
            'name_sp' => 'required',
            'age_rating_id' => 'required',
            'poster' => 'required|mimes:jpeg,png,jpg|max:2048',
            'logo' => 'required|mimes:jpeg,png,jpg|max:2048',
            'year' => 'required',
            'synopsis_en' => 'required',
            'synopsis_hi' => 'required',
            'synopsis_fr' => 'required',
            'synopsis_md' => 'required',
            'synopsis_sp' => 'required',
            'content_type' => 'required',
            'status' => 'required',
        ]);
    

        if(!empty($request['is_free']) && $request['is_free'] == 'on'){
            $request['is_free'] = 1;
        }else{
            $request['is_free'] = 0;
        }

        if(!empty($request->trailer_link)){
            $trailer_link = $request->trailer_link;
        }elseif (!empty($_COOKIE['trailer_link'])) {
            $trailer_link = $_COOKIE['trailer_link'];
        }else{
            $trailer_link = '';
        }

        $content = Content::create([
            'name' => [
                'en' => $request->name_en,
                'hi' => $request->name_hi,
                'fr' => $request->name_fr,
                'md' => $request->name_md,
                'sp' => $request->name_sp
            ],
            'age_rating_id' => $request->age_rating_id,
            'year' => $request->year,
            'synopsis' => [
                'en' => $request->synopsis_en,
                'hi' => $request->synopsis_hi,
                'fr' => $request->synopsis_fr,
                'md' => $request->synopsis_md,
                'sp' => $request->synopsis_sp
            ],
            'status' => $request->status,
            'is_free' => $request->is_free,
            'content_type' => $request->content_type,
            'trailer' => $trailer_link
        ]);

        $content_id = $content->id;       

        if(!empty($request['artist_ids']) && count($request['artist_ids']) > 0){
            foreach ($request['artist_ids'] as $key => $value) {                
                $insert_data[] = [
                    'content_id' => $content_id,
                    'artist_id' => $value
                ];
            }
            ContentCast::insert($insert_data);
        }

        if(!empty($request['genre_ids']) && count($request['genre_ids'])>0){
            foreach ($request['genre_ids'] as $key => $value) {
                $genre_insert_data[] = [
                    'content_id' => $content_id,
                    'genre_id' => $value,
                ];
            }
            ContentGenres::insert($genre_insert_data);
        }

        if(!empty($request['tag_ids']) && count($request['tag_ids'])>0){
            
            foreach ($request['tag_ids'] as $key => $value) {
                $tag_insert_data[] = [
                    'content_id' => $content_id,
                    'tag_id' => $value,
                ];
            }

            ContentTags::insert($tag_insert_data);
        }

        $dir = "content";
            
            if(!empty($request->poster)) {
                $pimage = CommonHelper::s3Upload($request->poster,$dir);
                $poster_image = $pimage;
            }   

            if(!empty($request->logo)) {
                $limage = CommonHelper::s3Upload($request->logo,$dir);
                $logo_image = $limage;
            }

            $content_image = Content::where('id', $content_id)->update([
                'poster' => !empty($poster_image)?$poster_image:'',
                'logo' => !empty($logo_image)?$logo_image:'',
            ]);


        // redirect
        return redirect()->route('admin.content.index')->with('success','Content created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $params['pageTittle'] = "View Content";
        $params['content'] = Content::with(['ageratings','content_cast.artist','content_genres.genre','content_tags.tags','season.episode','video'])->find($id);
        $params['content_status'] = config('app.content_status');

        $params['backUrl'] = route('admin.content.index');
        return view('admin.pages.content.view',$params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        
        $params['pageTittle'] = "Edit Content";
        $params['content'] = Content::with(['content_cast','content_genres','content_tags'])->find($id);
        $age_rating = AgeRatings::select('id', 'name','age')->get();
        $params['age_rating'] = $age_rating;
        
        $artist = Artist::select('id', 'name')->get();
        $tag = Tag::select('id', 'name')->get();
        $genre = Genre::select('id', 'name')->get();
        $params['artist'] = $artist;
        $params['tag'] = $tag;
        $params['genre'] = $genre;
        $params['content_status'] = config('app.content_status');
        $params['content_type'] = config('app.content_type');
    
        $params['backUrl'] = route('admin.content.index');
        return view('admin.pages.content.put',$params);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {

        $request->validate([
            'name_en' => 'required',
            'name_hi' => 'required',
            'name_fr' => 'required',
            'name_md' => 'required',
            'name_sp' => 'required',
            'synopsis_en' => 'required',
            'synopsis_hi' => 'required',
            'synopsis_fr' => 'required',
            'synopsis_md' => 'required',
            'synopsis_sp' => 'required',
            'age_rating_id' => 'required',
            'year' => 'required',
            'content_type' => 'required',
            'status' => 'required',
            'poster' => 'mimes:jpeg,png,jpg|max:2048',
            'logo' => 'mimes:jpeg,png,jpg|max:2048',
        ]);

        $update = array(
            'name' => [
                'en' => $request->name_en,
                'hi' => $request->name_hi,
                'fr' => $request->name_fr,
                'md' => $request->name_md,
                'sp' => $request->name_sp
            ],
            'age_rating_id' => $request->age_rating_id,
            'year' => $request->year,
            'synopsis' => [
                'en' => $request->synopsis_en,
                'hi' => $request->synopsis_hi,
                'fr' => $request->synopsis_fr,
                'md' => $request->synopsis_md,
                'sp' => $request->synopsis_sp
            ],
            'status' => $request->status,
            'content_type' => $request->content_type,
            'is_free' => $request->is_free
        );

        if(!empty($request['is_free']) && $request['is_free'] == 'on'){
            $request['is_free'] = 1;
        }else{
            $request['is_free'] = 0;
        }


        if(!empty($request->trailer_link)){
            $update['trailer'] = $request->trailer_link;
        }

        
        $dir = "content";

        if(!empty($request->poster)) {
            $pimage = CommonHelper::s3Upload($request->poster,$dir);
            $poster_image = $pimage;
            $update['poster'] = $poster_image;
        }   

        if(!empty($request->logo)) {
            $limage = CommonHelper::s3Upload($request->logo,$dir);
            $logo_image = $limage;
            $update['logo'] = $logo_image;
        }
        
         
        $content = Content::where('id', $id)->update($update);

        if(!empty($request['artist_ids']) && count($request['artist_ids'])>0){
            ContentCast::where('content_id', $id)->delete();
            foreach ($request['artist_ids'] as $key => $value) {                
                $insert_data[] = [
                    'content_id' => $id,
                    'artist_id' => $value
                ];
            }

            ContentCast::insert($insert_data);

        }

        if(!empty($request['genre_ids']) && count($request['genre_ids'])>0){
            ContentGenres::where('content_id', $id)->delete();
            foreach ($request['genre_ids'] as $key => $value) {
                $genre_insert_data[] = [
                    'content_id' => $id,
                    'genre_id' => $value
                ];
            }
            
            ContentGenres::insert($genre_insert_data);
        }

        if(!empty($request['tag_ids']) && count($request['tag_ids'])>0){
            ContentTags::where('content_id', $id)->delete();
            foreach ($request['tag_ids'] as $key => $value) {
                $tag_insert_data[] = [
                    'content_id' => $id,
                    'tag_id' => $value
                ];
            }
            ContentTags::insert($tag_insert_data);
        }

        return redirect()->route('admin.content.index')->with('success','Content updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        Content::whereId($id)->delete();
        Video::where('content_id',$id)->delete();
        Season::where('content_id',$id)->delete();

        return redirect()->route('admin.content.index')
                        ->with('success','Content deleted successfully');
    }

    public function adv_content_change(Request $request)
    {
        if(isset($request->id) && !empty($request->id)){
            
            
            $data = SystemConfig::where('path','adv_content_id')->where('value',$request->id)->first();
            if(empty($data)){
                $value = array('value'=>$request->id);
                $is_adv = SystemConfig::where('path','adv_content_id')->update($value);    
            }else{
                $value = array('value'=>'');
                $is_adv = SystemConfig::where('path','adv_content_id')->update($value);    
            }
            

            return response()->json(['status'=>'sucess']);
            //return redirect()->route('admin.content.index')->with('success','Advertisement content change successfully');
        }
    }

    public function child_adv_content_change(Request $request)
    {
        if(isset($request->id) && !empty($request->id)){
        
            $data = SystemConfig::where('path','child_adv_content_id')->where('value',$request->id)->first();
            if(empty($data)){
                $value = array('value'=>$request->id);
                $is_adv = SystemConfig::where('path','child_adv_content_id')->update($value);    
            }else{
                $value = array('value'=>'');
                $is_adv = SystemConfig::where('path','child_adv_content_id')->update($value);    
            }
            

            return response()->json(['status'=>'sucess']);
            //return redirect()->route('admin.content.index')->with('success','Advertisement content change successfully');
        }
    }

    public function active_deactive_product()
    {
       if($_POST['table'] == 'content'){
            if($_POST['status'] == 0){
                $status = 1;
            }else{
                $status = 0;
            }
            Content::where('id', $_POST['id'])->update(['status' => $status]);
        }
        echo $status; 
    }

    public function trailer_file_upload(Request $request)
    {   
        return CommonHelper::file_upload($request);
    }

}

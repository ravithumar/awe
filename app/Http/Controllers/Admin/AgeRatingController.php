<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Helpers\CommonHelper;
use App\Models\AgeRatings;
use App\Models\CastTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\ContactUs;
use DataTables;

class AgeRatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

       if ($request->ajax())
       {
            $data = AgeRatings::get();
            return Datatables::of($data)
            ->editColumn('action', function ($row){
				$btn = '<a href="'.route('admin.ratings.edit',$row['id']).'" class="mr-2"><i class="fa fa-edit"></i></a>';
				$btn .= '<a href="'.route('admin.ratings.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
				$btn .= '<a href="'.route('admin.ratings.destroy', $row['id']).'" data-url="ratings" data-id="'.$row["id"].'" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="'.csrf_token().'" ><i class="fa fa-trash"></i></a>';
				return $btn;
			})
            ->editColumn('name', function ($row){
                return $row['name'];
            })
            ->editColumn('children', function ($row){
                if($row['children'] == 0){
                    return 'No';
                }elseif($row['children'] == 1){
                    return 'Yes';
                }else{
                    return null;
                }
                
            })
			->rawColumns([ 'action'])
            ->make(true);
       }
       else
       {
           $columns = [
            ['data' => 'id','name' => 'id','title' => "Id"], 
            ['data' => 'name','name' => 'name', 'title' => __("Name")],
            ['data' => 'age','name' => 'age','title' => "Age"], 
            ['data' => 'children','name' => 'children','title' => "children"], 
            ['data' => 'action','name' => 'action', 'title' => "Action",'searchable'=>false,'orderable'=>false]];
           $params['dateTableFields'] = $columns;
           $params['dateTableUrl'] = route('admin.ratings.index');
           $params['dateTableTitle'] = "Age Rating";
           $params['dataTableId'] = time();
           $params['addUrl'] = route('admin.ratings.create');
           return view('admin.pages.ageratings.index',$params);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
	{
		$params['pageTittle'] = "Add Age Rating" ;
		$params['backUrl'] = route('admin.ratings.index');
		return view('admin.pages.ageratings.post',$params);
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
	{	
		$request->validate([
			'name_en' => 'required',
            'name_hi' => 'required',
            'name_fr' => 'required',
            'name_md' => 'required',
            'name_sp' => 'required',
            'age' => 'nullable'
		]);

        if(!empty($request['children']) && $request['children'] == 'on'){
            $request['children'] = 1;
        }else{
            $request['children'] = 0;
        }

		$category = AgeRatings::create([
			'name' => [
                'en' => $request->name_en,
                'hi' => $request->name_hi,
                'fr' => $request->name_fr,
                'md' => $request->name_md,
                'sp' => $request->name_sp
            ],
            'description' => [
                'en' => !empty($request->description_en)?$request->description_en:'',
                'hi' => !empty($request->description_hi)?$request->description_hi:'',
                'fr' => !empty($request->description_fr)?$request->description_fr:'',
                'md' => !empty($request->description_md)?$request->description_md:'',
                'sp' => !empty($request->description_sp)?$request->description_sp:''
            ],
            'age' => $request->age,
            'children' =>$request['children']
		]);

		// redirect
		return redirect()->route('admin.ratings.index')->with('success','Age Rating created successfully.');
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
	{
		$params['pageTittle'] = "View Age Rating" ;
		$params['ageRating'] = AgeRatings::find($id);
		$params['backUrl'] = route('admin.ratings.index');
		return view('admin.pages.ageratings.view',$params);
	}


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
	{
		$params['pageTittle'] = "Edit Age Rating";
		$params['ageRating'] = AgeRatings::find($id);
		$params['backUrl'] = route('admin.ratings.index');
		return view('admin.pages.ageratings.put',$params);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
	{
		$request->validate([
			'name_en' => 'required',
            'name_hi' => 'required',
            'name_fr' => 'required',
            'name_md' => 'required',
            'name_sp' => 'required',
            'age' => 'nullable'
		]);

        if(!empty($request['children']) && $request['children'] == 'on'){
            $request['children'] = 1;
        }else{
            $request['children'] = 0;
        }
        
		$ageRating['name'] = array(
                'en' => $request->name_en,
                'hi' => $request->name_hi,
                'fr' => $request->name_fr,
                'md' => $request->name_md,
                'sp' => $request->name_sp
            );

        $ageRating['description'] = array(
                'en' => !empty($request->description_en)?$request->description_en:'',
                'hi' => !empty($request->description_hi)?$request->description_hi:'',
                'fr' => !empty($request->description_fr)?$request->description_fr:'',
                'md' => !empty($request->description_md)?$request->description_md:'',
                'sp' => !empty($request->description_sp)?$request->description_sp:''
            );
        
		$ageRating['age'] = $request->age;
        $ageRating['children'] = $request->children;

		AgeRatings::whereId($id)->update($ageRating);
		return redirect()->route('admin.ratings.index')->with('success','Age Rating updated successfully');
	}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
	{	
        AgeRatings::whereId($id)->delete();
        return redirect()->route('admin.ratings.index')->with('success','Age Rating deleted successfully');
	}
}

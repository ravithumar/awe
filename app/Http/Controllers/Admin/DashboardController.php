<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\User;
use App\Models\Content;
use App\Models\Plan;
use App\Models\ActivityLog;
use CommonHelper;
use PaymentHelper;
use DB;


class DashboardController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $login_id =  Auth::user()->id;
        $params['users'] = User::select('users.*')->join('users_group', 'users.id', '=', 'users_group.user_id')->where('users.status',1)->where('users_group.group_id',4)->count();

        $main_user = User::select('users.id','users.parent_id','users.plan_id')->join('users_group', 'users.id', '=', 'users_group.user_id')->where('users.parent_id',0)->where('users.status',1)->where('users_group.group_id',4)->get();

        $sub_user = $non_sub_user = $total_sub = $total_non_sub = 0;
        if(!empty($main_user) && count($main_user)>0){
            $total_main_user = count($main_user);
            foreach ($main_user as $key => $value) {
                if(!empty($value->plan_id) && $value->plan_id != NULL){
                    $sub_user = $sub_user+1;
                }else{
                    $non_sub_user = $non_sub_user+1;
                }
            }
            
            if($sub_user != 0){
                $total_sub = round(($sub_user*100)/$total_main_user);
            }

            if($non_sub_user != 0){
                $total_non_sub = round(($non_sub_user*100)/$total_main_user);
            }

        }
        
        $params['total_sub'] = $total_sub;
        $params['total_non_sub'] = $total_non_sub;

        $params['content'] = Content::where('status',1)->count();
        $params['plan'] = Plan::where('status',1)->count();

        $params['most_viewed'] = ActivityLog::select('*',DB::raw("count('content_id') as 'total_content'"))->with(['content'])->groupBy('content_id')->orderBy('total_content','desc')->limit(10)->get();
        
        return view('admin.pages.dashboard',$params);
    }
}

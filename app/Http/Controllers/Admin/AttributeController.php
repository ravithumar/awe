<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\CommonHelper;

use Illuminate\Http\Request;
use App\Models\Attribute;
use App\Models\Product;
use App\Models\User;
use DataTables;
use Illuminate\Support\Facades\Auth;

class AttributeController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
	

	   if ($request->ajax())
		{
			$data = Attribute::get();
			return Datatables::of($data)
			->addIndexColumn()
			->editColumn('action', function ($row){
				$btn = '<a href="'.route('admin.attribute.edit',$row['id']).'" class="mr-2"><i class="fa fa-edit"></i></a>';
				$btn .= '<a href="'.route('admin.attribute.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
				$btn .= '<a href="'.route('admin.attribute.destroy', $row['id']).'" data-url="attribute" data-id="'.$row["id"].'" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="'.csrf_token().'" ><i class="fa fa-trash"></i></a>';
				return $btn;
			})
			->editColumn('alias', function ($row){
				return $row['alias'];
			})
			->editColumn('value', function ($row){
				return ucfirst($row['value']);
			})
			// ->editColumn('status', function ($row)
			// {
			// 	if($row['status'] == 0){
			// 		return '<button onclick="active_deactive_attribute(this);" data-id="' . $row['id'] . '" data-token="' . csrf_token() . '"  class="btn btn-danger btn-xs waves-effect waves-light" data-table="attribute" data-status="' . $row['status']. '">Inactive</button>';
			// 	}else{
			// 		return '<button onclick="active_deactive_attribute(this);" data-id="' . $row['id'] . '" data-token="' . csrf_token() . '"   class="btn btn-success btn-xs waves-effect waves-light" data-table="attribute" data-status="' . $row['status']. '">Active</button>';
			// 	}
			// })
			->rawColumns(['image', 'action','status'])
			->make(true);
	   }
	   else
	   {
		   $columns = [
			   ['data' => 'id','name' => 'id','title' => "Id"], 
			   ['data' => 'alias','name' => 'alias', 'title' => __("Alice")],
			   ['data' => 'value','name' => 'value', 'title' => __("Value")],
			   // ['data' => 'status','title' => __("Status"),'searchable'=>false],
			   ['data' => 'action','name' => 'action', 'title' => "Action",'searchable'=>false,'orderable'=>false]];
		   $params['dateTableFields'] = $columns;
		   $params['dateTableUrl'] = route('admin.attribute.index');
		   $params['dateTableTitle'] = "Attribute Management";
		   $params['dataTableId'] = time();
		   $params['addUrl'] = FALSE;
		   return view('admin.pages.attribute.index',$params);
	   }
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$params['pageTittle'] = "Add Attribute" ;
		$params['backUrl'] = route('admin.attribute.index');
		return view('admin.pages.attribute.post',$params);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		
		
		$request->validate([
			'name_en' => 'required',
			'name_hi' => 'required',
			'name_fr' => 'required',
			'name_md' => 'required',
			'name_sp' => 'required',
			'value' => 'required'
		]);
		 

		$attribute = Attribute::create([
			'alias' => [
				'en' => $request->name_en,
      			'hi' => $request->name_hi,
      			'fr' => $request->name_fr,
      			'md' => $request->name_md,
      			'sp' => $request->name_sp
			],
			'value' => $request->value
		]);

		// redirect
		return redirect()->route('admin.attribute.index')->with('success','Attribute created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$params['pageTittle'] = "View Attribute" ;
		$params['attribute'] = Attribute::find($id);
		$params['backUrl'] = route('admin.attribute.index');
		return view('admin.pages.attribute.view',$params);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$params['pageTittle'] = "Edit Attribute";
		$params['attribute'] = Attribute::find($id);
		$params['backUrl'] = route('admin.attribute.index');
		return view('admin.pages.attribute.put',$params);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
	
		$request->validate([
			'name_en' => 'required',
			'name_hi' => 'required',
			'name_fr' => 'required',
			'name_md' => 'required',
			'name_sp' => 'required',
			'value' => 'required'
			
		]);

		$attribute['alias'] = array(
				'en' => $request->name_en,
      			'hi' => $request->name_hi,
      			'fr' => $request->name_fr,
      			'md' => $request->name_md,
      			'sp' => $request->name_sp
			);
		$attribute['value'] = $request->value;
	
		Attribute::whereId($id)->update($attribute);
	
		return redirect()->route('admin.attribute.index')
						->with('success','Attribute updated successfully');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		// Attribute::whereId($id)->delete();
		$products = Product::whereAttributeId($id)->get();		
		if (isset($products) && !empty($products) && count($products) > 0) {
            $data['status'] = false;
			$data['message'] = 'You can not delete as Product(s) already exist for this attribute';        
		} else
		{
			Attribute::whereId($id)->delete();            
			$data['status'] = TRUE;        
			$data['message'] = 'Deleted';
		}
        echo json_encode($data);
		// return redirect()->route('admin.attribute.index')->with('success','Attribute deleted successfully');

	}

	public function active_deactive_attribute()
	{
		if($_POST['table'] == 'attribute'){
			if($_POST['status'] == 0){
				$status = 1;
			}else{
				$status = 0;
			}
			Attribute::where('id', $_POST['id'])->update(['status' => $status]);
		}
		echo $status;
	}
}

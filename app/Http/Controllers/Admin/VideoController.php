<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Models\Content;
use App\Models\Season;
use App\Models\Video;
use App\Models\Language;
use App\Models\VideoSubtitle;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Storage;


class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
            
       if ($request->ajax())
       {
            //$data = Video::join('content','videos.content_id','=','content.id')->select('videos.*','content.name')->get();
            
            $data = Video::with(['content','season'])->whereHas('content')->get();
            
            return Datatables::of($data)
            ->editColumn('action', function ($row){
                $btn = '<a href="'.route('admin.video.edit',$row['id']).'" class="mr-2"><i class="fa fa-edit"></i></a>';
                $btn .= '<a href="'.route('admin.video.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
                $btn .= '<a href="'.route('admin.video.destroy', $row['id']).'" data-url="video" data-id="'.$row["id"].'" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="'.csrf_token().'" ><i class="fa fa-trash"></i></a>';
                
                return $btn;
            })
            ->editColumn('episode_name', function ($row){
                return $row['episode_name'];
            })
            ->editColumn('season_name', function ($row){
                
                if(!empty($row['season'])){
                    return '<a target="_blank" href="'.route('admin.season.show',$row['season']['id']).'" class="mr-2">'.$row['season']['season'].'</a>';
                }else{
                    return '';
                }

            })
            ->editColumn('content_name', function ($row){

                if(!empty($row['content']['name'])){
                    $content_name = strlen($row['content']['name']) > 50 ? substr($row['content']['name'],0,50)."..." : $row['content']['name'];
                  return '<a target="_blank" href="'.route('admin.content.show',$row['content']['id']).'" class="mr-2">'.$content_name.'</a>';
                }else{
                    $content_name = '';
                  return '';
                }   
                
            })
            ->rawColumns(['action','id','episode_name','content_name','season_name'])
            
            ->make(true);
       }
       else
       {
           $columns = [
               ['data' => 'id','name' => 'id','title' => "Id"],
               ['data' => 'content_name', 'name' => 'content_name','title' => __("Content Name"),'searchable'=>true ], 
               ['data' => 'season_name', 'name' => 'season_name','title' => __("Season Name"),'searchable'=>true ], 
               ['data' => 'episode_name', 'name' => 'episode_name','title' => __("Video Name"),'searchable'=>true ], 
               ['data' => 'action','name' => 'action', 'title' => "Action",'searchable'=>false,'orderable'=>false]];
           $params['dateTableFields'] = $columns;
           $params['dateTableUrl'] = route('admin.video.index');
           $params['dateTableTitle'] = "Video Management";
           $params['dataTableId'] = time();
           $params['addUrl'] = route('admin.video.create');
           return view('admin.pages.video.index',$params);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $content_data = Content::select('id', 'name')->get();
        $params['content_data'] = $content_data;

        $params['pageTittle'] = "Add Video" ;
        $params['backUrl'] = route('admin.video.index');

        //dd($params); 

        return view('admin.pages.video.post',$params);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'content_id' => 'required',
            'video_file' => 'required'
        ]);

        if($request->content_type == 2){
            $request->validate([
                'episode_name_en' => 'required',
                'episode_name_md' => 'required',
                'episode_name_hi' => 'required',
                'episode_name_sp' => 'required',
                'episode_name_fr' => 'required',
                'episode_synopsis_en' => 'required',
                'episode_synopsis_md' => 'required',
                'episode_synopsis_hi' => 'required',
                'episode_synopsis_sp' => 'required',
                'episode_synopsis_fr' => 'required',
            ]);

            $episode_name =array(
                'en' => $request->episode_name_en,
                'md' => $request->episode_name_md,
                'hi' => $request->episode_name_hi,
                'sp' => $request->episode_name_sp,
                'fr' => $request->episode_name_fr
            );

            $episode_synopsis =array(
                'en' => $request->episode_synopsis_en,
                'md' => $request->episode_synopsis_md,
                'hi' => $request->episode_synopsis_hi,
                'sp' => $request->episode_synopsis_sp,
                'fr' => $request->episode_synopsis_fr
            );

        }else{
            $episode_name = $episode_synopsis = '';
        }
           
        if(!empty($request->video_file)){
            $video_link = $request->video_file;
        }elseif(!empty($_COOKIE['file_path_name'])) {
            $video_link = $_COOKIE['file_path_name'];
        }else{
            $video_link = '';
        }

        $dir = "video";
        if(!empty($request->thumbnail_image)) {
            
            $request->validate([
                'thumbnail_image' => 'image|mimes:jpeg,png,jpg|max:2048'
            ]);

            $thumbnail = CommonHelper::s3Upload($request->thumbnail_image,$dir);
            $thumbnail_image = $thumbnail;
        }

        $video = Video::create([
            'content_id' => $request->content_id,
            'season_id' => !empty($request->season_id)?$request->season_id:0,
            'episode_name' => $episode_name,
            'episode_synopsis' => $episode_synopsis,
            'video' => $video_link,
            'thumbnail_image' => !empty($thumbnail_image)?$thumbnail_image:'',
            'duration' => $request->duration
        ]);


         if(!empty($request->subtitle_en)) {
                $subtitle_en_file = CommonHelper::SrtsaveFile($request->subtitle_en);
                $subtitle_en = $subtitle_en_file;

                $language_id = Language::where('id',1)->first();
                $subtitle_insert_data[] = [
                    'video_id' => $video->id,
                    'language_id' => $language_id->id,
                    'file'=>$subtitle_en,
                ];
            }

            if(!empty($request->subtitle_md)) {
                $subtitle_md_file = CommonHelper::SrtsaveFile($request->subtitle_md);
                $subtitle_md = $subtitle_md_file;

                $language_id = Language::where('id',2)->first();
                $subtitle_insert_data[] = [
                    'video_id' => $video->id,
                    'language_id' => $language_id->id,
                    'file'=>$subtitle_md,
                ];
            }

            if(!empty($request->subtitle_hi)) {
                $subtitle_hi_file = CommonHelper::SrtsaveFile($request->subtitle_hi);
                $subtitle_hi = $subtitle_hi_file;

                $language_id = Language::where('id',3)->first();
                $subtitle_insert_data[] = [
                    'video_id' => $video->id,
                    'language_id' => $language_id->id,
                    'file'=>$subtitle_hi,
                ];
            }

            if(!empty($request->subtitle_sp)) {
                $subtitle_sp_file = CommonHelper::SrtsaveFile($request->subtitle_sp);
                $subtitle_sp = $subtitle_sp_file;

                $language_id = Language::where('id',5)->first();
                $subtitle_insert_data[] = [
                    'video_id' => $video->id,
                    'language_id' => $language_id->id,
                    'file'=>$subtitle_sp,
                ];
            }

            if(!empty($request->subtitle_fr)) {
                $subtitle_fr_file = CommonHelper::SrtsaveFile($request->subtitle_fr);
                $subtitle_fr = $subtitle_fr_file;

                $language_id = Language::where('id',4)->first();
                $subtitle_insert_data[] = [
                    'video_id' => $video->id,
                    'language_id' => $language_id->id,
                    'file'=>$subtitle_fr,
                ];
            }

            if(isset($subtitle_insert_data) && count($subtitle_insert_data)>0){
                VideoSubtitle::insert($subtitle_insert_data);    
            }
            

        return redirect()->route('admin.video.index')->with('success','Video created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $params['pageTittle'] = "View Video";
        $params['video'] = Video::with(['video_subtitle'=>function($q){$q->select('id','video_id','language_id','file');},'content'])->find($id);
        
        //dd($params['video']);

        $params['season'] = Season::find($params['video']['season_id']);
        $params['backUrl'] = route('admin.video.index');
        
        //dd($params);

        return view('admin.pages.video.view',$params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $params['pageTittle'] = "Edit Video";
        $params['video'] = Video::with(['video_subtitle'=>function($q){$q->select('id','video_id','language_id','file');},'content'])->find($id);
        $content_data = Content::select('id', 'name')->get();
        $params['content_data'] = $content_data;
        
        $season_data = Season::select('id', 'season')->where('content_id',$params['video']->content_id)->get();
        $params['season_data'] = $season_data;


        $params['backUrl'] = route('admin.video.index');
        return view('admin.pages.video.put',$params);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {   

        if($request->content_type == 2){
            $request->validate([
                'episode_name_en' => 'required',
                'episode_name_md' => 'required',
                'episode_name_hi' => 'required',
                'episode_name_sp' => 'required',
                'episode_name_fr' => 'required',
                'episode_synopsis_en' => 'required',
                'episode_synopsis_md' => 'required',
                'episode_synopsis_hi' => 'required',
                'episode_synopsis_sp' => 'required',
                'episode_synopsis_fr' => 'required',
            ]);
            
            $episode_name =array(
                'en' => $request->episode_name_en,
                'md' => $request->episode_name_md,
                'hi' => $request->episode_name_hi,
                'sp' => $request->episode_name_sp,
                'fr' => $request->episode_name_fr
            );

            $episode_synopsis =array(
                'en' => $request->episode_synopsis_en,
                'md' => $request->episode_synopsis_md,
                'hi' => $request->episode_synopsis_hi,
                'sp' => $request->episode_synopsis_sp,
                'fr' => $request->episode_synopsis_fr
            );

        }else{
            $episode_name = $episode_synopsis = '';
        }

            
            $update_data = array(
                'season_id' => !empty($request->season_id)?$request->season_id:0,
                'episode_name' => $episode_name,
                'episode_synopsis' => $episode_synopsis
            );

        if(!empty($request->video_file)){
            $update_data['video'] = $request->video_file;
            $update_data['duration'] = $request->duration;

        }

        $dir = "video";
        if(!empty($request->thumbnail_image)) {
            
            $request->validate([
                'thumbnail_image' => 'image|mimes:jpeg,png,jpg|max:2048'
            ]);

            $thumbnail = CommonHelper::s3Upload($request->thumbnail_image,$dir);
            $update_data['thumbnail_image'] = $thumbnail;
        }


        $video = Video::where('id', $id)->update($update_data);
            $subtitle_insert_data = array();

            $language_id = Language::where('id',1)->first();
            if(!empty($request->subtitle_en)) {
                $subtitle_en_file = CommonHelper::SrtsaveFile($request->subtitle_en);
                $subtitle_en = $subtitle_en_file;

                $subtitle_insert_data[] = [
                    'video_id' => $id,
                    'language_id' => $language_id->id,
                    'file'=>$subtitle_en,
                ];
            }elseif (!empty($request->subtitle_en_old)) {
                $subtitle_insert_data[] = [
                    'video_id' => $id,
                    'language_id' => $language_id->id,
                    'file'=>$request->subtitle_en_old,
                ];
            }

            $language_id = Language::where('id',2)->first();
            if(!empty($request->subtitle_md)) {
                $subtitle_md_file = CommonHelper::SrtsaveFile($request->subtitle_md);
                $subtitle_md = $subtitle_md_file;

                $subtitle_insert_data[] = [
                    'video_id' => $id,
                    'language_id' => $language_id->id,
                    'file'=>$subtitle_md,
                ];
            }elseif (!empty($request->subtitle_md_old)) {
                $subtitle_insert_data[] = [
                    'video_id' => $id,
                    'language_id' => $language_id->id,
                    'file'=>$request->subtitle_md_old,
                ];
            }

            $language_id = Language::where('id',3)->first();
            if(!empty($request->subtitle_hi)) {
                $subtitle_hi_file = CommonHelper::SrtsaveFile($request->subtitle_hi);
                $subtitle_hi = $subtitle_hi_file;

                $subtitle_insert_data[] = [
                    'video_id' => $id,
                    'language_id' => $language_id->id,
                    'file'=>$subtitle_hi,
                ];
            }elseif (!empty($request->subtitle_hi_old)) {
                $subtitle_insert_data[] = [
                    'video_id' => $id,
                    'language_id' => $language_id->id,
                    'file'=>$request->subtitle_hi_old,
                ];
            }

            $language_id = Language::where('id',5)->first();
            if(!empty($request->subtitle_sp)) {
                $subtitle_sp_file = CommonHelper::SrtsaveFile($request->subtitle_sp);
                $subtitle_sp = $subtitle_sp_file;

                $subtitle_insert_data[] = [
                    'video_id' => $id,
                    'language_id' => $language_id->id,
                    'file'=>$subtitle_sp,
                ];
            }elseif (!empty($request->subtitle_sp_old)) {
                $subtitle_insert_data[] = [
                    'video_id' => $id,
                    'language_id' => $language_id->id,
                    'file'=>$request->subtitle_sp_old,
                ];
            }

            $language_id = Language::where('id',4)->first();
            if(!empty($request->subtitle_fr)) {
                $subtitle_fr_file = CommonHelper::SrtsaveFile($request->subtitle_fr);
                $subtitle_fr = $subtitle_fr_file;

                $subtitle_insert_data[] = [
                    'video_id' => $id,
                    'language_id' => $language_id->id,
                    'file'=>$subtitle_fr,
                ];
            }elseif (!empty($request->subtitle_fr_old)) {
                $subtitle_insert_data[] = [
                    'video_id' => $id,
                    'language_id' => $language_id->id,
                    'file'=>$request->subtitle_fr_old,
                ];
            }

            if(isset($subtitle_insert_data) && count($subtitle_insert_data)>0){
                VideoSubtitle::where('video_id', $id)->delete();
                VideoSubtitle::insert($subtitle_insert_data);
            }
             
            
        return redirect()->route('admin.video.index')->with('success','Video updated successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        Video::whereId($id)->delete();
        return redirect()->route('admin.video.index')
                        ->with('success','Video deleted successfully');
    }

    public function active_deactive_product()
    {
       if($_POST['table'] == 'video'){
            if($_POST['status'] == 0){
                $status = 1;
            }else{
                $status = 0;
            }
            Video::where('id', $_POST['id'])->update(['status' => $status]);
        }
        echo $status; 
    }

    public function get_season_data()
    {
        if(!empty($_GET['id'])){
        
        $video_content = Video::select('id','content_id')->with(['content'])->where('content_id',$_GET['id'])->get()->toArray();
        
        if(!empty($video_content) && $video_content[0]['content']['content_type'] == 1){
            return response()->json(['status'=>false,'data' => array(),'message'=>'Video already exist']);
        }else{
            $data = Content::with(['season'])->where('id',$_GET['id'])->first();
            return response()->json(['status'=>true,'data' => $data,'message'=>'Season data']);
            
        }
            
        }else{
            return response()->json(['status'=>false,'data' => array(),'message'=>'']);
        }
    }


    public function chunk_file_upload(Request $request)
    {
        return CommonHelper::file_upload($request);
    }

}
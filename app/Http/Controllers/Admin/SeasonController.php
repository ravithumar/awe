<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Helpers\CommonHelper;
use App\Models\Artist;
use App\Models\CastTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\ContactUs;
use App\Models\Content;
use App\Models\Season;
use App\Models\Video;
use DataTables;

class SeasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
       if ($request->ajax())
       {
            $data = Season::with('content_data')->get();
            
            return Datatables::of($data)
            ->editColumn('action', function ($row){
				$btn = '<a href="'.route('admin.season.edit',$row['id']).'" class="mr-2"><i class="fa fa-edit"></i></a>';
				$btn .= '<a href="'.route('admin.season.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
				$btn .= '<a href="'.route('admin.season.destroy', $row['id']).'" data-url="season" data-id="'.$row["id"].'" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="'.csrf_token().'" ><i class="fa fa-trash"></i></a>';
				return $btn;
			})
            ->editColumn('season', function ($row){
                return $row['season'];
            })
            ->addColumn('content_name', function ($row) {
                if($row->content_data->name){
                    $content_name = strlen($row->content_data->name) > 50 ? substr($row->content_data->name,0,50)."..." : $row->content_data->name;
                    return  '<a target="_blank" href="'.route('admin.content.show',$row->content_data->id).'" class="mr-2">'.$content_name.'</a>';
                }else{
                    return '';
                }
            })
			->rawColumns([ 'action', 'content_name'])
            ->make(true);
       }
       else
       {
           $columns = [
            ['data' => 'id','name' => 'id','title' => "Id"], 
            ['data' => 'content_name','name' => 'content_name', 'title' => __("Content Name")],
            ['data' => 'season','name' => 'season', 'title' => __("Name")],
            ['data' => 'action','name' => 'action', 'title' => "Action",'searchable'=>false,'orderable'=>false]];
           $params['dateTableFields'] = $columns;
           $params['dateTableUrl'] = route('admin.season.index');
           $params['dateTableTitle'] = "Season";
           $params['dataTableId'] = time();
           $params['addUrl'] = route('admin.season.create');
           return view('admin.pages.season.index',$params);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
	{
        $contentRecord = Content::select('id', 'name')->where('content_type',2)->get();
        $params['contentRecord'] = $contentRecord;
		$params['pageTittle'] = "Add Season";
		$params['backUrl'] = route('admin.season.index');
		return view('admin.pages.season.post',$params);
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
	{	
		$request->validate([
            'content_id' => 'required',
			'name_en' => 'required',
            'name_hi' => 'required',
            'name_fr' => 'required',
            'name_md' => 'required',
            'name_sp' => 'required'
		]);

		$category = Season::create([
			'content_id' => $request->content_id,
			'season' => [
                'en' => $request->name_en,
                'hi' => $request->name_hi,
                'fr' => $request->name_fr,
                'md' => $request->name_md,
                'sp' => $request->name_sp
            ]
		]);

		// redirect
		return redirect()->route('admin.season.index')->with('success','Season created successfully.');
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
	{
		$params['pageTittle'] = "View Season" ;
		$seasonContentValue = Season::with(['episode','content_data'])->where(['id' => $id])->first();
		$params['backUrl'] = route('admin.season.index');
		return view('admin.pages.season.view',$params)->with('seasonContentValue', $seasonContentValue);
	}


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
	{
		$params['pageTittle'] = "Edit Season";
		$seasonValue = Season::where(['id' => $id])->with('content_data')->first();
        $params['contentRecord'] = Content::select('id', 'name')->where('content_type',2)->get();
		$params['backUrl'] = route('admin.artist.index');
		return view('admin.pages.season.put',$params)->with('seasonValue', $seasonValue);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
	{
		$request->validate([
            'content_id' => 'required',
			'name_en' => 'required',
            'name_hi' => 'required',
            'name_fr' => 'required',
            'name_md' => 'required',
            'name_sp' => 'required'
		]);
		$contentValue['content_id'] = $request->content_id;
		$contentValue['season'] = array(
                'en' => $request->name_en,
                'hi' => $request->name_hi,
                'fr' => $request->name_fr,
                'md' => $request->name_md,
                'sp' => $request->name_sp
            );
        
		Season::whereId($id)->update($contentValue);
		return redirect()->route('admin.season.index')->with('success','Season updated successfully');
	}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
	{
        $model = Season::find($id);
        $model->delete();
        
        Video::where('season_id',$id)->delete();

        return redirect()->route('admin.season.index')->with('success','Season deleted successfully');
	}
}

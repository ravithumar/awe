<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Helpers\CommonHelper;
use App\Models\CastTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\ContactUs;
use DataTables;

class CastTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

       if ($request->ajax())
       {
            $data = CastTypes::get();
            return Datatables::of($data)
            ->editColumn('name', function ($row){
                return $row['name'];
            })
            ->editColumn('action', function ($row){
				$btn = '<a href="'.route('admin.cast.edit',$row['id']).'" class="mr-2"><i class="fa fa-edit"></i></a>';
				$btn .= '<a href="'.route('admin.cast.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
				$btn .= '<a href="'.route('admin.cast.destroy', $row['id']).'" data-url="cast" data-id="'.$row["id"].'" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="'.csrf_token().'" ><i class="fa fa-trash"></i></a>';
				return $btn;
			})
			->rawColumns([ 'action'])
            ->make(true);
       }
       else
       {
           $columns = [
            ['data' => 'id','name' => 'id','title' => "Id"], 
            ['data' => 'name','name' => 'name', 'title' => __("Name")],
            ['data' => 'action','name' => 'action', 'title' => "Action",'searchable'=>false,'orderable'=>false]];
           $params['dateTableFields'] = $columns;
           $params['dateTableUrl'] = route('admin.cast.index');
           $params['dateTableTitle'] = "Cast Type";
           $params['dataTableId'] = time();
           $params['addUrl'] = route('admin.cast.create');
           return view('admin.pages.cast.index',$params);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
	{
		$params['pageTittle'] = "Add Cast Type" ;
		$params['backUrl'] = route('admin.cast.index');
		return view('admin.pages.cast.post',$params);
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
	{	
		$request->validate([
            'name_en' => 'required',
            'name_hi' => 'required',
            'name_fr' => 'required',
            'name_md' => 'required',
            'name_sp' => 'required'
        ]);
         

		$category = CastTypes::create([
			'name' => [
                'en' => $request->name_en,
                'hi' => $request->name_hi,
                'fr' => $request->name_fr,
                'md' => $request->name_md,
                'sp' => $request->name_sp
            ]
		]);

		// redirect
		return redirect()->route('admin.cast.index')->with('success','Cast type created successfully.');
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
	{
		$params['pageTittle'] = "View Cast Type" ;
		$params['casttype'] = CastTypes::find($id);
		$params['backUrl'] = route('admin.cast.index');
		return view('admin.pages.cast.view',$params);
	}


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
	{
		$params['pageTittle'] = "Edit Cast Type";
		$params['casttype'] = CastTypes::find($id);
		$params['backUrl'] = route('admin.cast.index');
		return view('admin.pages.cast.put',$params);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
	{
		$request->validate([
			'name_en' => 'required',
            'name_hi' => 'required',
            'name_fr' => 'required',
            'name_md' => 'required',
            'name_sp' => 'required'
		]);
		$casttype['name'] = array(
                'en' => $request->name_en,
                'hi' => $request->name_hi,
                'fr' => $request->name_fr,
                'md' => $request->name_md,
                'sp' => $request->name_sp
            );
        
		CastTypes::whereId($id)->update($casttype);
		return redirect()->route('admin.cast.index')->with('success','Cast type updated successfully');
	}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
	{	
        CastTypes::whereId($id)->delete();
        return redirect()->route('admin.cast.index')->with('success','Cast Type deleted successfully');
	}
}

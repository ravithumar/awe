<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Models\Feedback;
use DataTables;
use Illuminate\Support\Facades\Auth;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
            
       if ($request->ajax())
       {
            $data = Feedback::with(['user','faq'])->get();

            return Datatables::of($data)
            ->editColumn('user_name', function ($row){
                return '<a href="'.route('admin.user.show', $row['user']['id']) . '" class="mr-2">'.$row['user']['first_name']." ".$row['user']['last_name'].'</a>';
            })
            ->editColumn('question', function ($row){

                $out = strlen($row['faq']['question']) > 50 ? substr($row['faq']['question'],0,50)."..." : $row['faq']['question'];
                
                return '<a href="'.route('admin.faq.show',$row['faq']['id']).'" class="mr-2">'.$out.'</a>'; 
            })
            ->editColumn('description', function ($row){

                $description = strlen($row['description']) > 50 ? substr($row['description'],0,50)."..." : $row['description'];
                
                return $description; 
            })
            ->editColumn('action', function ($row){
                $btn = '<a href="'.route('admin.feedback.show',$row['id']).'" class="mr-2"><i class="fa fa-eye"></i></a>';
                $btn .= '<a href="'.route('admin.feedback.destroy', $row['id']).'" data-url="feedback" data-id="'.$row["id"].'" data-popup="tooltip" onclick="delete_notiflix(this);return false;" data-token="'.csrf_token().'" ><i class="fa fa-trash"></i></a>';
                
                return $btn;
            })
            ->rawColumns(['action','id','user_name','question','description'])
            ->make(true);
       }
       else
       {
           $columns = [
               ['data' => 'id','name' => 'id','title' => "Id"],
               ['data' => 'user_name', 'name' => 'user_name','title' => __("User Name"),'searchable'=>true ], 
               ['data' => 'question', 'name' => 'question','title' => __("Question"),'searchable'=>true ], 
               ['data' => 'description', 'name' => 'description','title' => __("Description"),'searchable'=>true ], 
               ['data' => 'action','name' => 'action', 'title' => "Action",'searchable'=>false,'orderable'=>false]];
           $params['dateTableFields'] = $columns;
           $params['dateTableUrl'] = route('admin.feedback.index');
           $params['dateTableTitle'] = "Feedback Management";
           $params['dataTableId'] = time();
           return view('admin.pages.feedback.index',$params);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function show($id)
    {
        $params['pageTittle'] = "View Feedback";
        $params['feedback'] = Feedback::with(['user','faq'])->find($id);
        $params['backUrl'] = route('admin.feedback.index');

        return view('admin.pages.feedback.view',$params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        Feedback::whereId($id)->delete();
        return redirect()->route('admin.feedback.index')
                        ->with('success','Feedback deleted successfully');
    }


}

<?php

namespace App\Http\Controllers\Api;



use App\Http\Controllers\Controller;

use App\Models\AppVersion;

use App\Models\Devices;

use App\Models\Banner;

use App\Models\Orders;

use App\Models\User;

use App\Models\UsersGroup;

use App\Traits\ApiResponser;
use App\Models\Notification;

use CommonHelper;
use NotificationHelper;

use Illuminate\Auth\Events\Registered;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Password;

use Illuminate\Support\Facades\Validator;

use Carbon\Carbon;

use App\Http\Resources\UserResource;

use Mail;
use App\Mail\RegistrationMail;
use App\Mail\OTPMail;
use App\Models\Plan;

use DB;

class AuthController extends Controller {

    use ApiResponser;



    public function login(Request $request) {

        $validator = Validator::make(request()->all(), [
            'email' => 'required',
            'password' => 'required',
            'device_name' => 'required',
            'device_token' => 'required',
            'device_type' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'language_id' => '',
        ]);

        if (!$validator->fails()) {

            // Added for email-phone login  Start
            $login = request()->input('email');

            if(is_numeric($login)){
                $field = 'phone';
            } elseif (filter_var($login, FILTER_VALIDATE_EMAIL)) {
                $field = 'email';
            } else{
                return response([
                    'status' => false,
                    'message' =>  __('message.api.email_phone_incorrect_format')
                ]);
            }

            // Added for email-phone login  End

            $user_data = array($field => $request->email, 'password' => $request->password);

            if (!auth()->attempt($user_data)) {
                return $this->errorResponse(__('message.api.account_not_exist'));
            }

            $user = auth()->user();

            if($user->isActive()){

                if (in_array($user->hasRole(), [4])) {

                    //$user->revokeAllTokens();

                    $accessToken = auth()->user()->createToken('authToken')->accessToken;
                    $user->token = $accessToken;

                    $map_key = CommonHelper::ConfigGet('map_key');
                    $geocode = @file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$request->latitude.','.$request->longitude.'&sensor=true&key='.$map_key.'');

                    if($geocode === FALSE){
                        return ([
                                'status' => false,
                                'message' => __('message.something_went_wrong')
                            ]);
                    }

                    $data = json_decode($geocode);
                    if(empty($data->results)){
                        return ([
                                'status' => false,
                                'message' => __('message.something_went_wrong')
                            ]);
                    }else{

                        $add_array  = $data->results;
                        $add_array = $add_array[0];
                        $add_array = $add_array->address_components;
                        $country = "";
                        $state = ""; 
                        $city = "";

                            foreach ($add_array as $key) {
                              if($key->types[0] == 'administrative_area_level_2')
                              {
                                $city = $key->long_name;
                              }
                              if($key->types[0] == 'administrative_area_level_1')
                              {
                                $state = $key->long_name;
                              }
                              if($key->types[0] == 'country')
                              {
                                $country = $key->long_name;
                              }
                            }
                    }


                    $device = Devices::where('user_id', $user->id)->where('token', $request->device_token)->where('name', $request->device_name)->where('type', $request->device_type)->first();

                    if(isset($request->language_id) && $request->language_id != ''){
                        $update_data = array('language_id' => $request->language_id);
                        User::whereId($user->id)->update($update_data);
                        $user->language_id = $request->language_id;
                    }

                    if(!isset($device)){
                        $devices = Devices::insert([
                            'user_id' => $user->id,
                            'name' => $request->device_name,
                            'token' => $request->device_token,
                            'type' => $request->device_type,
                            'city' => $city,
                            'state' => $state,
                            'country' => $country,
                            'latitude' => $request->latitude,
                            'longitude' => $request->longitude,
                        ]);
                    }else{
                        $device->city = $city;
                        $device->state = $state;
                        $device->country = $country;
                        $device->latitude = $request->latitude;
                        $device->longitude = $request->longitude;
                        $device->save();
                    }

                    return (new UserResource($user))->additional([
                        'status' => true,
                        'message' =>  __('message.api.login_success')
                    ]);

                }else {

                    return $this->errorResponse(__('Entered email address or phone number does not match with the records'));

                }

            }else{
                return $this->errorResponse(__('message.api.account_deactivated'));
            }
        }

        return $this->errorResponse($validator->messages(), true);

    }



    public function register(Request $request) {


        $validator = Validator::make(request()->all(), [
            'first_name' => 'required|max:25',
            'last_name' => 'required|max:25',
            'password' => 'required|confirmed|min:8',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|unique:users,phone|min:10|max:10',            
            'device_name' => 'required',
            'device_token' => 'required',
            'device_type' => 'required'
        ],
        [
            'email.unique' => 'This email address is already registered',
            'email.required' => 'Please enter email',
            'email.email' => 'Please enter a valid email'
        ]);

        if (!$validator->fails()) {

            $user_data = [];
            $user_data['password'] = Hash::make($request->password);
            $user_data['first_name'] = $request->first_name;
            $user_data['last_name'] = $request->last_name;
            $user_data['email'] = $request->email;
            $user_data['phone'] = $request->phone;
            $user_data['phone_verified'] = !empty($request->phone)?1:0;
            $user_data['status'] = 1;
            $user_data['reference_code'] = "AWE".CommonHelper::random_number();
        
            
            /*$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $randomString = substr(str_shuffle(str_repeat($pool, 5)), 0, 64);
            $remember_token = config('app.url').'account-activation/'.$randomString;
            $user_data['remember_token'] = $randomString;
            Mail::to($request->email)->send(new RegistrationMail('Account Activation', $remember_token, config('app.address1'), config('app.address2')));*/


            $user_id = User::insertGetId($user_data); 

            UsersGroup::create([
                'user_id' => $user_id,
                'group_id' => '4',
            ]);

            
            if(isset($request->reference_code) && !empty($request->reference_code)){

                $refer_user = User::where('reference_code',$request->reference_code)->where('status',1)->first();
                
                if(isset($refer_user) && !empty($refer_user) && count($refer_user)>0){

                    if(!empty($refer_user->plan_id)){
                        
                        $update_refer_user['plan_id'] = $refer_user->plan_id;
                        $update_refer_user['expiration_date'] = Carbon::createFromFormat('Y-m-d', $refer_user->expiration_date)->addDays(30);

                    }else{

                        $plan = Plan::where('status',1)->first();

                        $update_refer_user['plan_id'] = $plan->id;
                        $update_refer_user['expiration_date'] = Carbon::now()->addDays(30);
                        
                    }

                    $refer_user_update = User::where('id',$refer_user->id)->update($update_refer_user);  

                    $data = User::select('users.*','devices.token')
                    ->join('devices', 'users.id', '=', 'devices.user_id')
                    ->where('users.id', $refer_user->id)->get();

                    if(!empty($data) && count($data)>0){

                        $type = 'referral_accepted';
                        $notification_arr['type'] = $type;

                        $title = "Referral Accepted";
                        $description = "Congratulations! ".$request->first_name." ".$request->last_name." accepted your referral code. Your current plan is extended by 30 days.";

                        $notification_data = array();
                        foreach ($data as $key => $value) {

                            if($value->token != '' && $value->notification == 1){
                                NotificationHelper::send($value->token, $title, $description, $type, $notification_arr);
                            }
                            
                            $notification_data[] = array(
                                'user_id' => $value->id,
                                'title' => $title,
                                'message' => $description,
                                'type' => $type
                            );
                        }

                        Notification::insert($notification_data);
                    }

                }
                
            }

            Devices::create([
                 'user_id' => $user_id,
                 'name' => $request->device_name,
                 'token' => $request->device_token,
                 'type' => $request->device_type,
            ]);

            $user = User::find($user_id);
            $accessToken = $user->createToken('authToken')->accessToken;
            $user->token = $accessToken;

            event(new Registered($user));

            // $user->sendEmailVerificationNotification();

            // return $this->successResponse($user, __("Please verify your account from your registered email address"));

            // return $this->successResponse($user, __("Registered successfully"));
            return (new UserResource($user))->additional([
                'status' => true,
                // 'token' => $accessToken,
                'message' => __('message.api.register_success')
            ]);

        }

        return $this->errorResponse($validator->messages(), true);

    }



    public function forgot_password(Request $request) {

        $validator = Validator::make(request()->all(), [
            'email' => 'required',
        ]);

        if (!$validator->fails()) {

            $login = request()->input('email');

            if(is_numeric($login)){
                $field = 'phone';
                $user = User::wherePhone($request->email)->first();
            
                if($user){
                    
                    $data['otp'] = rand(1000,9999);
                    $message = "Your AWE otp is ".$data['otp'];
                    $result = NotificationHelper::send_sms($message, $user->phone);
                    if($result['status'] == TRUE){
                        $data['user_id'] = $user->id;
                        return response([
                            'status' => true,
                            'message' =>  __('message.api.otp_sent_success'),
                            'data' => $data
                        ]);
                    }else{
                        return response([
                            'status' => false,
                            'message' => __('message.invalid_phone'),
                            'data' => $result
                        ]);
                    }

                }else{
                    return response([
                        'status' => false,
                        'message' => __('message.phone_not_registered'),
                    ]);
                }
                
            } elseif (filter_var($login, FILTER_VALIDATE_EMAIL)) {
                $field = 'email';
                $user = User::whereEmail($request->email)->first();

                if($user){
                    $status = Password::sendResetLink(
                        $request->only('email')
                    );

                    if ($status == Password::RESET_LINK_SENT) {
                        return response([
                            'status' => true,
                            'message' => __('message.change_password_link_sent')
                        ]);
                    }else{
                        return $this->errorResponse(__(__('message.email_address_not_registered')));
                    }
                }else{
                    return response([
                        'status' => false,
                        'message' => __('message.email_address_not_registered'),
                    ]);
                }
                

            } else{
                return response([
                    'status' => false,
                    'message' => __('message.api.email_phone_incorrect_format'),
                ]);
            }

        }

        return $this->errorResponse($validator->messages(), true);

    }


    public function init($type = "", $app_version = "",Request $request) {

        if(isset($request->user_id) && !empty($request->user_id)){
            $lan_data = User::select('language.id','language.type','language.image')
                ->join('language', 'users.language_id', '=', 'language.id')
                ->where('users.id', $request->user_id)->where('users.status', 1)->first();
            
            if(!empty($lan_data)){
                $lan_data = $lan_data;
            }else{
                $lan_data = '';
            }
        }else{
            $lan_data = '';
        }

        if ($app_version != "" && $type != "") {

            $system_version = AppVersion::whereType($type)->first();

            $current_version = (Int) str_replace('.', '', $system_version->version);

            $app_version = (Int) str_replace('.', '', $app_version);

            $data['force_update'] = false;
            if ($system_version->maintenance == 1) {

                $data['status'] = false;
                $data['update'] = false;
                $data['maintenance'] = true;
                $data['lan_data'] = $lan_data;
                $data['message'] = __('Server under maintenance, please try again after some time');

                return response($data);

            } else if ($app_version < $current_version && $system_version->force_update == 0) {

                $data['status'] = false;
                $data['update'] = true;
                $data['force_update'] = false;
                $data['maintenance'] = false;
                $data['lan_data'] = $lan_data;
                $data['message'] = __('App new version available , please upgrade your application');

                return response($data);

            } else if ($app_version < $current_version && $system_version->force_update == 1) {

                $data['status'] = false;
                $data['update'] = true;
                $data['force_update'] = true;
                $data['maintenance'] = false;
                $data['lan_data'] = $lan_data;
                $data['message'] = __('App new version available , please upgrade your application');

                return response($data);

            } else {

                $data['status'] = true;
                $data['update'] = false;
                $data['maintenance'] = false;
                $data['lan_data'] = $lan_data;
                $data['message'] = __('Success');

                return response($data);

            }

        }

        return $this->errorResponse(__('Missing App version & Type...'));

    }

    public function send_otp(Request $request)
    {
        $validator = Validator::make(request()->all(), [
           // 'email' => 'email:rfc,dns|unique:users,email',
            'phone' => 'required|numeric|unique:users,phone'
        ]);

        if (!$validator->fails()) {

            $data['otp'] = rand(1000,9999);
            $message = "Your AWE otp is ".$data['otp'];

            $result = NotificationHelper::send_sms($message, $request['phone']);

            return response([
                'status' => true,
                'message' => __('message.api.otp_sent_success'),
                'data' => $data
            ]);

        }

        return $this->errorResponse($validator->messages(), true);

    }

    public function send_otp_email(Request $request)
    {
        $validator = Validator::make(request()->all(), [
           'email' => 'email:rfc,dns|unique:users,email',
        ]);

        if (!$validator->fails()) {

            $data['otp'] = rand(1000,9999);

            $subject = "Verify Your OTP : ".$data['otp'];
            $message = $data['otp'];

            Mail::to($request['email'])->send(new OTPMail($subject, $message, config('app.address1'), config('app.address2')));

            return response([
                'status' => true,
                'message' => __('message.api.otp_sent_success'),
                'data' => $data
            ]);

        }

        return $this->errorResponse($validator->messages(), true);

    }


    public function test_upload(Request $request) {

        if ($request->hasFile('profile_picture')) {
            $dir = "images/users";
            $profile_picture = CommonHelper::s3Upload($request->file('profile_picture'), $dir);
            $update_data['profile_picture'] = $profile_picture;
        }

    }

}


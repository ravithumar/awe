<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Plan;
use App\Models\PlanAttribute;
use CommonHelper;
use Validator;
use App;
use Lang;
use App\Http\Resources\Api\PlanResource;

class PlanController extends Controller
{
	public function plan(Request $request){
		
		$plan = Plan::with(['attributes'])->where('status', 1)->get();
		$app_string  = Lang::get('message.plan');
		return PlanResource::collection($plan)->additional([
            'status' => true,
            'message' => '',
            'app_string' => $app_string
        ]);

	}
}
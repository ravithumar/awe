<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\UserAgeRestriction;
use App\Models\UserRestrictedContent;
use App\Models\AgeRatings;
use App\Models\Content;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Api\AgeRatingsResource;
use App\Http\Resources\Api\UserRestrictedContentResource;
use App\Http\Resources\Api\ContentSearchResource;
use App\Http\Resources\Api\PopularSearchResource;

class UserAgeRestrictionController extends Controller {


    public function index(Request $request)
    {
        $age_ratings = AgeRatings::all();
        $user_content = UserRestrictedContent::with(['content'])->where('user_id',$request->user()->id)->get();

        $data['age_ratings'] = AgeRatingsResource::collection($age_ratings);
        $data['user_content'] = UserRestrictedContentResource::collection($user_content);

        return [
            'data' => $data,
            'status' => true,
            'message' => '',
        ];
    }


    public function search(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'filter_string' => 'required|min:2'
        ]);

         if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }

        $content_ids = UserRestrictedContent::where('user_id',$request->user()->id)->get()->pluck('content_id')->toArray();
        
        $user_age_restriction_ids = UserAgeRestriction::where('user_id',$request->user()->id)->get()->pluck('age_rating_id')->toArray();

        $content = Content::where('name','LIKE','%'.$request->filter_string.'%')->whereNotIn('id',$content_ids)->whereNotIn('age_rating_id',$user_age_restriction_ids)->where('status','!=',2)->get();
        
        return ContentSearchResource::collection($content)->additional([
            'status' => true,
            'message' => ''
        ]);
    }
 
    public function popular_search(Request $request)
    {
        
        $content_ids = UserRestrictedContent::where('user_id',$request->user()->id)->get()->pluck('content_id')->toArray();

        $user_age_restriction_ids = UserAgeRestriction::where('user_id',$request->user()->id)->get()->pluck('age_rating_id')->toArray();

        $content = Content::with(['genres'])->select('id','name','is_free','poster','trailer')->whereNotIn('id',$content_ids)->whereNotIn('age_rating_id',$user_age_restriction_ids)->where('status','!=',2)->where('search','!=',0)->orderBy('search','desc')->limit(3)->get();
        
        return PopularSearchResource::collection($content)->additional([
            'status' => true,
            'message' => ''
        ]);

    }


    public function add(Request $request) {
        $validator = Validator::make(request()->all(), [
            'user_id' => 'required|integer'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }

        if (User::where('id',$request->user_id)->where('status',1)->first()) {

            
            if($request['age_rating_id'] == ''){
                UserAgeRestriction::where('user_id',$request->user_id)->delete();
            }else{
                $age_ratings = explode(",", $request['age_rating_id']);
                $age_rating_insert_array = array();

                $age_ratings_data = AgeRatings::whereIn('id',$age_ratings)->get();
                if(count($age_ratings_data) == count($age_ratings)){

                    foreach ($age_ratings as $key => $value) {
                        $age_rating_insert_array[$key] = array(
                            'user_id' => $request->user_id,
                            'age_rating_id' => $value
                        );
                    }

                    UserAgeRestriction::where('user_id',$request->user_id)->delete();
                    UserAgeRestriction::insert($age_rating_insert_array);

                }else{
                    return response([
                        'status' => false,
                        'message' => __('message.title_restiction_not_found')
                    ]);
                }

            }
                
            if($request['content_id'] == ''){
                UserRestrictedContent::where('user_id',$request->user_id)->delete();
            }else{
                $content_ids = array_unique(explode(",", $request['content_id']));
                $age_rastricted_content_array = array();

                $contents = Content::whereIn('id',$content_ids)->where('status', '!=', '2')->get();
                if(count($contents) == count($content_ids)){

                    foreach ($content_ids as $key => $value) {
                        $age_rastricted_content_array[$key] = array(
                            'user_id' => $request->user_id,
                            'content_id' => $value
                        );
                    }

                    UserRestrictedContent::where('user_id',$request->user_id)->delete();
                    UserRestrictedContent::insert($age_rastricted_content_array);

                }else{
                    return response([
                        'status' => false,
                        'message' => __('message.movie_tvshow_not_found')
                    ]);
                }

            }

            return response([
                'status' => true,
                'message' => __('message.user_age_restriction_success')
            ]);

        }else{
            
            return response([
                'status' => true,
                'message' => __('message.user_not_found')
            ]);

        }

    }



}

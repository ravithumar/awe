<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Models\FavoriteVideo;
use App\Models\ActivityLog;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FavoriteVideoController extends Controller {

    use ApiResponser;

    public function index(Request $request) {

        return response([
            'status' => true,
            'message' => '',
            'data' => '',
        ]);

    }

    /**
     *  makes a service provider favourite or removes him from favourite
     */


    public function like(Request $request) {

        $validator = Validator::make(request()->all(), [
            'content_id' => 'required',
            'status' => 'required',
        ]);

        if (!$validator->fails()) {
            if (Content::whereId($request->content_id)->where('status','!=','2')->first()) {
                if($request->status == '1'){

                    if (FavoriteVideo::whereContentId($request->content_id)->whereUserId($request->user()->id)->first()) {

                        FavoriteVideo::whereContentId($request->content_id)->whereUserId($request->user()->id)->update(['status'=>1]);
                        
                        $total_user_view = ActivityLog::where('content_id',$request->content_id)->count();
                        $total_user_like = FavoriteVideo::where('content_id',$request->content_id)->where('status', 1)->count();

                        if($total_user_like > 0 && $total_user_view > 0){
                            $data['likes'] = round((100*$total_user_like)/$total_user_view);    
                        }else{
                            $data['likes'] = 0;
                        }

                        return response([
                            'status' => true,
                            'data' => $data,
                            'message' => __('message.favorite_video_success'),
                        ]);

                    } else {
                        $favorite_video = new FavoriteVideo;
                        $favorite_video->user_id = $request->user()->id;
                        $favorite_video->content_id = $request->content_id;
                        $favorite_video->status = 1;
                        $favorite_video->save();

                        $total_user_view = ActivityLog::where('content_id',$request->content_id)->count();
                        $total_user_like = FavoriteVideo::where('content_id',$request->content_id)->where('status', 1)->count();

                        if($total_user_like > 0 && $total_user_view > 0){
                            $data['likes'] = round((100*$total_user_like)/$total_user_view);    
                        }else{
                            $data['likes'] = 0;
                        }

                        return response([
                            'status' => true,
                            'data' => $data,
                            'message' => __('message.favorite_video_success'),
                        ]);

                    }
                }else{

                    FavoriteVideo::whereContentId($request->content_id)->whereUserId($request->user()->id)->where('status',1)->delete();

                    $total_user_view = ActivityLog::where('content_id',$request->content_id)->count();
                    $total_user_like = FavoriteVideo::where('content_id',$request->content_id)->where('status', 1)->count();

                    if($total_user_like > 0 && $total_user_view > 0){
                        $data['likes'] = round((100*$total_user_like)/$total_user_view);    
                    }else{
                        $data['likes'] = 0;
                    }
                    
                    return response([
                        'status' => true,
                        'data' => $data,
                        'message' => __('message.favorite_video_remove')
                    ]);

                }
            } else {
                return response([
                    'status' => false,
                    'message' => __('message.video_not_found')
                ]);
            }
        }
        return $this->errorResponse($validator->messages(), true);

    }
    

    public function dislike(Request $request) {

        $validator = Validator::make(request()->all(), [
            'content_id' => 'required',
            'status' => 'required',
        ]);

        if (!$validator->fails()) {
            if (Content::whereId($request->content_id)->where('status','!=','2')->first()) {
                if($request->status == '1'){
                    
                    if (FavoriteVideo::whereContentId($request->content_id)->whereUserId($request->user()->id)->first()) {
                        
                        FavoriteVideo::whereContentId($request->content_id)->whereUserId($request->user()->id)->update(['status'=>2]);

                        $total_user_view = ActivityLog::where('content_id',$request->content_id)->count();
                        $total_user_like = FavoriteVideo::where('content_id',$request->content_id)->where('status', 1)->count();

                        if($total_user_like > 0 && $total_user_view > 0){
                            $data['likes'] = round((100*$total_user_like)/$total_user_view);    
                        }else{
                            $data['likes'] = 0;
                        }

                        return response([
                            'status' => true,
                            'data' => $data,
                            'message' => __('message.favorite_video_remove'),
                        ]);
                    } else {
                        $favorite_video = new FavoriteVideo;
                        $favorite_video->user_id = $request->user()->id;
                        $favorite_video->content_id = $request->content_id;
                        $favorite_video->status = 2;
                        $favorite_video->save();

                        $total_user_view = ActivityLog::where('content_id',$request->content_id)->count();
                        $total_user_like = FavoriteVideo::where('content_id',$request->content_id)->where('status', 1)->count();

                        if($total_user_like > 0 && $total_user_view > 0){
                            $data['likes'] = round((100*$total_user_like)/$total_user_view);    
                        }else{
                            $data['likes'] = 0;
                        }

                        return response([
                            'status' => true,
                            'data' => $data,
                            'message' => __('message.favorite_video_remove'),
                        ]);

                    }

                }else{

                    FavoriteVideo::whereContentId($request->content_id)->whereUserId($request->user()->id)->where('status',2)->delete();
                    
                    $total_user_view = ActivityLog::where('content_id',$request->content_id)->count();
                    $total_user_like = FavoriteVideo::where('content_id',$request->content_id)->where('status', 1)->count();

                    if($total_user_like > 0 && $total_user_view > 0){
                        $data['likes'] = round((100*$total_user_like)/$total_user_view);    
                    }else{
                        $data['likes'] = 0;
                    }

                    return response([
                        'status' => true,
                        'data' => $data,
                        'message' => __('message.favorite_video_remove'),
                    ]);

                }
            } else {
                return response([
                    'status' => false,
                    'message' => __('message.video_not_found'),
                ]);
            }
        }
        return $this->errorResponse($validator->messages(), true);

    }

}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Cart;
use App\Models\CartProducts;
use App\Models\StoresProduct;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Validator;
use CommonHelper;

class CartController extends Controller

{

    use ApiResponser;

    public function index(Request $request)
    {        

        $user_id = request()->user()->id;
        $cart = Cart::whereUserId($user_id)->whereOrderId('0')->first();

        if($cart){

            $cart_products = CartProducts::whereCartId($cart->id)->with(['product', 'variant'])->get();
            $store = User::find($cart->store_id);

            $sub_total = '0';
            if(auth()->user()->user_type == '1'){
                foreach ($cart_products as $key => $value) {
                    if($value->product->business_discount == 0){
                        $product_total = $value->qty * $value->variant->business_price;
                    }else{
                        $product_total = $value->qty * $value->variant->business_discount_amount;
                    }

                    $sub_total += $product_total;
                    $product_total = number_format((float)$product_total, 2, '.', '');
                    $cart_products[$key]->product_total = $product_total;
                }
            }else{

                foreach ($cart_products as $key => $value) {
                    
                    if($value->product->discount == 0){
                        $product_total = $value->qty * $value->variant->price;
                    }else{
                        $product_total = $value->qty * $value->variant->discount_amount;
                    }

                    $sub_total += $product_total;
                    $product_total = number_format((float)$product_total, 2, '.', '');
                    $cart_products[$key]->product_total = $product_total;
                }
            }

            $data['sub_total'] = number_format((float)$sub_total, 2, '.', '');
            $tax = CommonHelper::ConfigGet('tax');
            if($tax > 0){
                $tax_amount = ($tax / 100) * $data['sub_total'];
                $data['tax'] = number_format((float)$tax_amount, 2, '.', '');
                $total = $data['sub_total'] + $tax_amount;
                $data['total'] = number_format((float)$total, 2, '.', '');
            }else{
                $data['total'] = $data['sub_total'];
                $data['tax'] = '0';
            }
            
            $data['id'] = $cart->id;
            $data['store'] = $store;
            $data['cart_products'] = $cart_products;
            $data['cart_products_count'] = count($cart_products);
            return response([
                'status' => true,
                'message' => 'successfully',
                'data' => $data
            ]);

        }else{
            return response([
                'status' => false,
                'message' => 'No any product in cart',
            ]);
        }
        

    }

    protected function add(Request $request)
    {        
        $validator = Validator::make($request->all(),[
            'store_id' => 'required|numeric|not_in:0',
            'product_id' => 'required|numeric|not_in:0',
            'variant_id' => 'required|numeric|not_in:0',
            'qty' => 'numeric',
            'is_case' => 'boolean'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ], 200);
        }

        if(isset($request->is_case) && $request->is_case == '1'){
            $request->qty = 12;
        }else{
            if(!isset($request->qty) || $request->qty == '0'){
                return ([
                    'status' => false,
                    'message' => 'Quantity is required if not case.'
                ]);
            }
        }

        $user_id = request()->user()->id;

        $store_product = StoresProduct::whereUserId($request->store_id)->whereProductId($request->product_id)->whereVariantId($request->variant_id)->where('stock', '>=', $request->qty)->first();

        if($store_product){
            $cart_data = array(
                'user_id' => $user_id,
                'store_id' => $request->store_id,
                'order_id' => 0
            );
            $cart = Cart::firstOrCreate($cart_data);

            $cart_product_data = array(
                'cart_id' => $cart->id,
                'product_id' => $request->product_id,
                'variant_id' => $request->variant_id
            );
            $cart_product = CartProducts::firstOrCreate($cart_product_data);
            $cart_product->qty = $cart_product->qty + $request->qty;
            $cart_product->save();

            return response([
                'status' => true,
                'message' => 'Added in cart successfully.',
                'data' => $store_product,
            ]);
        }else{
            return response([
                'status' => false,
                'message' => 'Out of stock'
            ]);
        }

    }

    protected function qty_update(Request $request)
    {        
        $validator = Validator::make($request->all(),[
            'cart_product_id' => 'required|numeric|not_in:0',
            'update' => 'boolean'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ], 200);
        }

        $cart_product = CartProducts::whereId($request->cart_product_id)->with(['cart'])->first();

        if($cart_product){

            $message = 'Quantity updated';
            $data['cart_empty'] = FALSE;
            $data['sub_total'] = '0';
            $data['cart_products_count'] = 0;
            $cart_id = $cart_product->cart_id;

            if($request->update == '1'){

                $check_stock_count = $cart_product->qty + 1;

                $store_product = StoresProduct::whereUserId($cart_product->cart->store_id)->whereProductId($cart_product->product_id)->whereVariantId($cart_product->variant_id)->where('stock', '>=', $check_stock_count)->first();

                if($store_product){
                    $cart_product->qty = $cart_product->qty + 1;
                    $cart_product->save();
                }else{
                    return response([
                        'status' => false,
                        'message' => 'Out of stock'
                    ]);
                }

                
            }else{
                if($cart_product->qty == '1'){
                    
                    $cart_product->forceDelete();
                    $message = 'Product removed from cart'; 

                    $cart_product_other_count = CartProducts::whereCartId($cart_id)->count();
                    if($cart_product_other_count == 0){
                        Cart::find($cart_product->cart_id)->delete();
                        $data['cart_empty'] = TRUE;
                    }
                }else{
                    $cart_product->qty = $cart_product->qty - 1;
                    $cart_product->save();
                }
            }

            $cart_product->product_total = '0';

            if($data['cart_empty'] == FALSE){
                $cart_products = CartProducts::whereCartId($cart_id)->with(['product', 'variant'])->get();
                $data['cart_products_count'] = count($cart_products);
                $sub_total = 0;
                if(auth()->user()->user_type == '1'){
                    foreach ($cart_products as $key => $value) {
                        if($value->product->business_discount == 0){
                            $product_total = $value->qty * $value->variant->business_price;
                        }else{
                            $product_total = $value->qty * $value->variant->business_discount_amount;
                        }

                        $sub_total += $product_total;

                        if($value->id == $cart_product->id){
                            $cart_product->product_total = number_format((float)$product_total, 2, '.', '');
                        }
                    }
                }else{
                    foreach ($cart_products as $key => $value) {

                        if($value->product->discount == 0){
                            $product_total = $value->qty * $value->variant->price;
                        }else{
                            $product_total = $value->qty * $value->variant->discount_amount;
                        }

                        $sub_total += $product_total;

                        if($value->id == $cart_product->id){
                            $cart_product->product_total = number_format((float)$product_total, 2, '.', '');
                        }
                    }
                }

                $data['sub_total'] = number_format((float)$sub_total, 2, '.', '');
                $tax = CommonHelper::ConfigGet('tax');
                if($tax > 0){
                    $tax_amount = ($tax / 100) * $data['sub_total'];
                    $data['tax'] = number_format((float)$tax_amount, 2, '.', '');
                    $total = $data['sub_total'] + $tax_amount;
                    $data['total'] = number_format((float)$total, 2, '.', '');
                }else{
                    $data['total'] = $data['sub_total'];
                    $data['tax'] = '0';
                }

            }


            $data['cart_product'] = $cart_product;

            return response([
                'status' => true,
                'data' => $data,
                'message' => $message
            ]);
        }else{
            return response([
                'status' => false,
                'message' => 'Cart data not found.'
            ]);
        }

        

    }

    protected function remove_product(Request $request)
    {        
        $validator = Validator::make($request->all(),[
            'cart_product_id' => 'required|numeric|not_in:0',            
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ], 200);
        }

        $cart_product = CartProducts::find($request->cart_product_id);                
        if($cart_product) 
        {
            $data['sub_total'] = $data['total'] = $data['tax'] = $sub_total = '0';
            $data['cart_empty'] = FALSE;
            $data['cart_products_count'] = 0;

            $cart_products = CartProducts::whereCartId($cart_product->cart_id)->get();

            if(count($cart_products) == 1) 
            {
                Cart::where('id', $cart_product->cart_id)->delete();
                $cart_product->forceDelete();
                $data['cart_empty'] = TRUE;
            } else {
                $cart_product->forceDelete();

                $cart_products = CartProducts::whereCartId($cart_product->cart_id)->with(['product', 'variant'])->get();
                $data['cart_products_count'] = count($cart_products);
                if(auth()->user()->user_type == '1'){
                    foreach ($cart_products as $key => $value) {
                        if($value->product->business_discount == 0){
                            $sub_total += $value->qty * $value->variant->business_price;
                        }else{
                            $sub_total += $value->qty * $value->variant->business_discount_amount;
                        }
                    }
                }else{
                    foreach ($cart_products as $key => $value) {

                        if($value->product->discount == 0){
                            $sub_total += $value->qty * $value->variant->price;
                        }else{
                            $sub_total += $value->qty * $value->variant->discount_amount;
                        }
                    }
                }

                $data['sub_total'] = number_format((float)$sub_total, 2, '.', '');

                $tax = CommonHelper::ConfigGet('tax');
                if($tax > 0){
                    $tax_amount = ($tax / 100) * $data['sub_total'];
                    $data['tax'] = number_format((float)$tax_amount, 2, '.', '');
                    $total = $data['sub_total'] + $tax_amount;
                    $data['total'] = number_format((float)$total, 2, '.', '');
                }else{
                    $data['total'] = $data['sub_total'];
                    $data['tax'] = '0';
                }

            }

            return response([
                'status' => true,
                'data' => $data,
                'message' => 'Product removed successfully',
            ]);
        } else {
            return response([
                'status' => false,
                'message' => 'Cart data not found.'
            ]);
        }

        

    }

    protected function clear(Request $request)
    {        
        $user_id = request()->user()->id;

        $cart = Cart::whereUserId($user_id)->whereOrderId('0')->first();
        if($cart){
            $cart->delete();

            return response([
                'status' => true,
                'data' => [],
                'message' => 'Cart products removed successfully',
            ]);
        }  else {
            return response([
                'status' => false,
                'message' => 'Cart data not found.'
            ]);
        }        
        

    }

}

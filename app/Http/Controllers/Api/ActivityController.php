<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ActivityLog;
use App\Models\Content;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ActivityController extends Controller {

    use ApiResponser;



    public function add(Request $request) {

        $validator = Validator::make(request()->all(), [
            'content_id' => 'required|integer',
            'video_id' => 'required|integer',
        ]);


        if (!$validator->fails()) {
            if (Content::whereId($request->content_id)->where('status','!=','2')->first() && User::whereId($request->user()->id)->where('status',1)->first()) {
                
                ActivityLog::where('user_id',$request->user()->id)->where('content_id',$request->content_id)->where('video_id',$request->video_id)->delete();

                $activity_log = new ActivityLog;
                $activity_log->user_id = $request->user()->id;
                $activity_log->content_id = $request->content_id;
                $activity_log->video_id = $request->video_id;
                $activity_log->save();

                return response([
                    'status' => true,
                    'message' => __('message.activity_log_added_success'),
                ]);
               
            } else {
                return response([
                    'status' => false,
                    'message' => __('message.enter_valid_user_or_content'),
                ]);
            }
        }
        return $this->errorResponse($validator->messages(), true);

    }




}

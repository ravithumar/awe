<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Models\Faq;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Api\FaqResource;
use Lang;

class FaqController extends Controller {

    use ApiResponser;


    public function index(Request $request) {

        $faq = Faq::where('status',1)->get();
        $app_string  = Lang::get('message.faq');

        if(count($faq) > 0){
            return FaqResource::collection($faq)->additional([
                'status' => true,
                'message' => '',
                'app_string' => $app_string
            ]);
        }else{
            return response([
                'status' => false,
                'message' => __('message.no_data_found'),
                'app_string' => $app_string
            ]);
        }


    }

}

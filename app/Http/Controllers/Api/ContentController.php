<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Content;
use App\Models\Genre;
use App\Models\ContentGenres;
use App\Models\FavoriteVideo;
use App\Models\SystemConfig;
use App\Models\Video;
use App\Models\UserGenres;
use App\Models\UserRestrictedContent;
use App\Models\UserAgeRestriction;
use CommonHelper;
use Validator;
use App;
use App\Http\Resources\Api\ContentResource;
use App\Http\Resources\Api\VideoResource;

class ContentController extends Controller
{
	public function index(Request $request){

		$validator = Validator::make(request()->all(), [
            'content_type' => 'required|digits_between:1,2',
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }
        
        $adv_content_data = CommonHelper::ConfigGet('adv_content_id');

        $restricted_content_ids = UserRestrictedContent::where('user_id',$request->user()->id)->get()->pluck('content_id')->toArray();
        $user_age_restriction_ids = UserAgeRestriction::where('user_id',$request->user()->id)->get()->pluck('age_rating_id')->toArray();

        $content_type = $request->content_type;

			$main = Content::with(['genres'])
	        ->select('id','poster','is_free')
	        ->whereHas('genres', function($q) use($request) {
	       		$q->where('genre.status', 1);
			})
			->whereNotIn('id',$restricted_content_ids)
			->whereNotIn('age_rating_id',$user_age_restriction_ids)
	        ->where('status','!=',2)
	        ->orderBy('content.id','desc');

        if(isset($adv_content_data) && !empty($adv_content_data)){
			$content_data = Content::find($adv_content_data);

			if($content_data->content_type == $content_type){
				$main = $main->where('id',$content_data->id)->where('content_type',$content_type);
			}else{
				$main = $main->where('content_type',$content_type);
			}
        }else{
        	$main = $main->where('content_type',$content_type);
        }

        $content['main'] = $main->first();

		$genre_data = Genre::with(['content' => function($q) use($content_type,$restricted_content_ids,$user_age_restriction_ids){
							    $q->whereNotIn('content.id',$restricted_content_ids)->whereNotIn('age_rating_id',$user_age_restriction_ids)->where('content.content_type', $content_type)->orderBy('content.id','desc');
							}])->with(['user_genres' => function($q) use($request){
							    $q->where('user_genres.user_id', $request->user()->id)->orderBy('user_genres.id','asc');
							}])->whereHas('content', function($q){
					       		$q->where('status','!=',2);
							})->where('status', 1)->get();

	
		if(count($genre_data)>0){
			
			$content['category'] = array();

			$preferred_languages_array = $general_array = $temp_preferred_languages_array = array();

			foreach ($genre_data as $key => $value) {

					$content['category'][$key]['id'] = $value['id'];
					$content['category'][$key]['name'] = $value['name'];
					
					if(!empty($value->user_genres) && $value->user_genres->genre_id == $value['id']){

						$temp_preferred_languages_array['id'] = $value['id'];
						$temp_preferred_languages_array['name'] = $value['name'];
						$temp_preferred_languages_array['content'] = array();
						$temp_preferred_languages_array['view_more'] = FALSE;

						if(!empty($value['content']) && count($value['content'])>0){

							$temp_content_array = $content_array = array();

							foreach ($value['content'] as $ckey => $cvalue) {

							
								if(count($content_array) <= 4) {
									
									$temp_content_array['id'] = $cvalue['id'];
									$temp_content_array['poster'] = $cvalue['poster'];
									$temp_content_array['is_free'] = $cvalue['is_free'];

									array_push($content_array,$temp_content_array);
								}else{
									$temp_preferred_languages_array['view_more'] = TRUE;
								}


							}
							$temp_preferred_languages_array['content'] = $content_array;

							array_push($preferred_languages_array,$temp_preferred_languages_array);

						}else{
							unset($temp_preferred_languages_array);
						}

					}else{
						$temp_preferred_languages_array['id'] = $value['id'];
						$temp_preferred_languages_array['name'] = $value['name'];
						$temp_preferred_languages_array['content'] = array();
						$temp_preferred_languages_array['view_more'] = FALSE;


						if(!empty($value['content']) && count($value['content'])>0){

							$temp_content_array = $content_array = array();

							foreach ($value['content'] as $ckey => $cvalue) {

							
								if(count($content_array) <= 4) {
									
									$temp_content_array['id'] = $cvalue['id'];
									$temp_content_array['poster'] = $cvalue['poster'];
									$temp_content_array['is_free'] = $cvalue['is_free'];

									array_push($content_array,$temp_content_array);
								}else{
									$temp_preferred_languages_array['view_more'] = TRUE;
								}


							}
							$temp_preferred_languages_array['content'] = $content_array;
							
							array_push($general_array,$temp_preferred_languages_array);

						}else{
							unset($temp_preferred_languages_array);
						}
						
					}
			}
		}		
		$content['data'] = array_merge($preferred_languages_array,$general_array);
		

		if($content){
			return response([
                'status' => true,
                'message' => '',
                'data' => $content,
                'app_string' => __('message.home')
            ]);
        }else{
            return response([
                'status' => false,
                'message' => __('message.no_data_found'),
                'app_string' => __('message.home')
            ]);
        }

	}

	public function view_all(Request $request){
		
		$validator = Validator::make(request()->all(), [
            'genres_id' => 'required',
            'content_type' => 'required|digits_between:1,2'
        ]);

		if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }

		$content_type = $request->content_type;
		
		$restricted_content_ids = UserRestrictedContent::where('user_id',$request->user()->id)->get()->pluck('content_id')->toArray();
		$user_age_restriction_ids = UserAgeRestriction::where('user_id',$request->user()->id)->get()->pluck('age_rating_id')->toArray();

        $content = Content::with(['genres'])
        ->select('id','poster','is_free')
        ->whereHas('genres', function($q) use($request) {
       		$q->where('genre.id', $request->genres_id);
       		$q->where('genre.status', 1);
		})
		->whereNotIn('id',$restricted_content_ids)
		->whereNotIn('age_rating_id',$user_age_restriction_ids)
        ->where('status','!=',2)
        ->where('content_type',$content_type)
        ->orderBy('content.id','desc')
        ->paginate(16);

		if($content){
			return response([
                'status' => true,
                'message' => '',
                'data' => $content
            ]);
        }else{
            return response([
                'status' => false,
                'message' => __('message.no_data_found')
            ]);
        }

	}

	public function category_vise_content(Request $request){
		
		$validator = Validator::make(request()->all(), [
            'genres_id' => 'required'
        ]);

		if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }

        $genre = Genre::find($request->genres_id);
		$adv_content_data = $genre->content_id;

		$restricted_content_ids = UserRestrictedContent::where('user_id',$request->user()->id)->get()->pluck('content_id')->toArray();
		$user_age_restriction_ids = UserAgeRestriction::where('user_id',$request->user()->id)->get()->pluck('age_rating_id')->toArray();

		if(isset($request->is_kid) && !empty($request->is_kid) && $request->is_kid == 1){

			$main = Content::with(['genres','ageratings'])
		        ->select('id','poster','is_free')
		        ->whereHas('genres', function($q) use($request) {
		       		$q->where('genre.status', 1);
				})
				->whereHas('ageratings', function($q) use($request) {
		       		$q->where('age','<=', 14);
				})
				->whereNotIn('id',$restricted_content_ids)
				->whereNotIn('age_rating_id',$user_age_restriction_ids)
		        ->where('status','!=',2);

			if(isset($adv_content_data) && !is_null($adv_content_data)){
		        $main = $main->where('id', $adv_content_data);
	        }

	        $main = $main->orderBy('id','desc')->first();

	        $data['main'] = $main;

	        $data['recommendation'] = Content::with(['genres','ageratings'])
	        ->select('id','poster','is_free')
	        ->whereHas('genres', function($q) use($request) {
	       		$q->where('genre.id', $request->genres_id);
	       		$q->where('genre.status', 1);
			})
			->whereHas('ageratings', function($q) use($request) {
		    	$q->where('age','<=', 14);
			})
			->whereNotIn('id',$restricted_content_ids)
			->whereNotIn('age_rating_id',$user_age_restriction_ids)
	        ->where('status','!=',2)
	        ->orderBy('content.id','desc')
        	->get();
        	

		}else{

			$main = Content::with(['genres'])
		        ->select('id','poster','is_free')
		        ->whereHas('genres', function($q) use($request) {
		       		$q->where('genre.status', 1)->where('genre.id', $request->genres_id);
				})
				->whereNotIn('id',$restricted_content_ids)
				->whereNotIn('age_rating_id',$user_age_restriction_ids)
		        ->where('status','!=',2);

	        if(isset($adv_content_data) && !is_null($adv_content_data)){
		        $main = $main->where('id', $adv_content_data);
	        }
	        
	        $main = $main->orderBy('id','desc')->first();

	        $data['main'] = $main;

	        $data['recommendation'] = Content::with(['genres'])
	        ->select('id','poster','is_free')
	        ->whereHas('genres', function($q) use($request) {
	       		$q->where('genre.id', $request->genres_id);
	       		$q->where('genre.status', 1);
			})
			->whereNotIn('id',$restricted_content_ids)
			->whereNotIn('age_rating_id',$user_age_restriction_ids)
	        ->where('status','!=',2)
	        ->orderBy('content.id','desc')
        	->get();

		}

		
		return response([
                'status' => true,
                'message' => '',
                'data' => $data,
                'app_string' => __('message.home')
            ]);

	}

	public function details($id,Request $request){
		
		$restricted_content_ids = UserRestrictedContent::where('user_id',$request->user()->id)->get()->pluck('content_id')->toArray();
		$user_age_restriction_ids = UserAgeRestriction::where('user_id',$request->user()->id)->get()->pluck('age_rating_id')->toArray();

		$content = Content::with(['ageratings','video.video_subtitle','content_cast.artist','content_genres.genre','content_tags.tags'])->where('id',$id)->whereNotIn('id',$restricted_content_ids)->whereNotIn('age_rating_id',$user_age_restriction_ids)->where('status','!=',2)->first();

		if(isset($request->search) && $request->search == 1){
			if(!empty($content)){
				$content->search = $content->search+1;
				$content->save();
			}
		}
		
		$content_data = $temp_content_array = array();
		if(isset($content) && !empty($content->content_genres)){
			
			$genre_ids = array(); $content_id = '';
			foreach ($content->content_genres as $key => $value) {
				$genre_ids[] = $value->genre_id;
				$content_id = $value->content_id;
			}

			$more_like_this_data = ContentGenres::with(['content' => function($q) use($restricted_content_ids,$user_age_restriction_ids){ $q->where('content.status', '!=', 2)->whereNotIn('content.id',$restricted_content_ids)->whereNotIn('age_rating_id',$user_age_restriction_ids)->orderBy('content.id','desc');
							}])->whereIn('genre_id',$genre_ids)->where('content_id','!=',$content_id)->groupBy('content_id')->orderBy('content_id','desc')->limit(9)->get()->toArray();
			
			$result = array();
			if(!empty($more_like_this_data) && count($more_like_this_data)>0){
				$i=0;
				foreach ($more_like_this_data as $key => $value) {
					if(!empty($value['content']) && count($value['content'])>0){
						$result[$i]['id'] = $value['content']['id'];
						$result[$i]['poster'] = $value['content']['poster'];
						$result[$i]['is_free'] = $value['content']['is_free'];	

						$i++;
					}
				}
			}
			
			return (new ContentResource($content))
                ->additional([
                'status' => true,
	            'message' => '',
	            'more_like_this' => $result,
	            'app_string' => __('message.content_detail')
            ]);

		}else{
			return response([
                'status' => false,
                'message' => __('message.no_data_found')
            ]);
		}
		
	}

	public function kids(Request $request){

        $restricted_content_ids = UserRestrictedContent::where('user_id',$request->user()->id)->get()->pluck('content_id')->toArray();
        $user_age_restriction_ids = UserAgeRestriction::where('user_id',$request->user()->id)->get()->pluck('age_rating_id')->toArray();

        $child_adv_content = CommonHelper::ConfigGet('child_adv_content_id');

			$main = Content::select('id','poster','is_free','age_rating_id')->with(['ageratings'])
	        ->whereHas('ageratings', function($q) use($request) {
	       		$q->where('age','<=', 14);
			})
			->whereNotIn('id',$restricted_content_ids)
			->whereNotIn('age_rating_id',$user_age_restriction_ids)
	        ->where('status','!=',2)
	        ->orderBy('content.id','desc');

	     if(!in_array($child_adv_content,$restricted_content_ids)){
	    	if(isset($child_adv_content) && !empty($child_adv_content)){
				$content_data = Content::find($child_adv_content);
				$main = $main->where('id',$content_data->id);
	        } 	
	     }

        $content['main'] = $main->first();
        
        
		$genre_data = Genre::with(['content.ageratings','content' => function($q) use($restricted_content_ids,$user_age_restriction_ids){
					    $q->whereNotIn('content.id',$restricted_content_ids)->whereNotIn('age_rating_id',$user_age_restriction_ids)->orderBy('content.id','desc');
					}])->whereHas('content', function($q){
					       		$q->where('status','!=',2);
							})
					->with(['user_genres' => function($q) use($request){
							    $q->where('user_genres.user_id', $request->user()->id)->orderBy('user_genres.id','asc');
							}])
					->where('status', 1)->get();

		if(count($genre_data)>0){
			
			$content['category'] = array();

			$preferred_languages_array = $general_array = $temp_preferred_languages_array = $temp_general_languages_array = array();

			foreach ($genre_data as $key => $value) {

					$content['category'][$key]['id'] = $value['id'];
					$content['category'][$key]['name'] = $value['name'];
					
					if(!empty($value->user_genres) && $value->user_genres->genre_id == $value['id']){

						$temp_preferred_languages_array['id'] = $value['id'];
						$temp_preferred_languages_array['name'] = $value['name'];
						$temp_preferred_languages_array['content'] = array();
						$temp_preferred_languages_array['view_more'] = FALSE;

						if(!empty($value['content']) && count($value['content'])>0){

							$temp_content_array = $content_array = array();

							foreach ($value['content'] as $ckey => $cvalue) {

								if($cvalue->ageratings->age <= 14){
									if(count($content_array) <= 4) {
										
										$temp_content_array['id'] = $cvalue['id'];
										$temp_content_array['poster'] = $cvalue['poster'];
										$temp_content_array['is_free'] = $cvalue['is_free'];

										array_push($content_array,$temp_content_array);
									}else{
										$temp_preferred_languages_array['view_more'] = TRUE;
									}
								}

							}
							$temp_preferred_languages_array['content'] = $content_array;
							
							if(empty($temp_preferred_languages_array['content'])){
								unset($temp_preferred_languages_array);
							}else{
								array_push($preferred_languages_array,$temp_preferred_languages_array);
							}
							
						}else{
							unset($temp_preferred_languages_array);
						}

					}else{
					

						$temp_general_languages_array['id'] = $value['id'];
						$temp_general_languages_array['name'] = $value['name'];
						$temp_general_languages_array['content'] = array();
						$temp_general_languages_array['view_more'] = FALSE;


						if(!empty($value['content']) && count($value['content'])>0){

							$temp_content_array = $content_array = array();
							foreach ($value['content'] as $ckey => $cvalue) {

								if($cvalue->ageratings->age <= 14){
									
									if(count($content_array) <= 4) {
										
										$temp_content_array['id'] = $cvalue['id'];
										$temp_content_array['poster'] = $cvalue['poster'];
										$temp_content_array['is_free'] = $cvalue['is_free'];

										array_push($content_array,$temp_content_array);
									}else{
										$temp_general_languages_array['view_more'] = TRUE;
									}
								}

							}

							
							$temp_general_languages_array['content'] = $content_array;
							
							if(empty($temp_general_languages_array['content'])){
								unset($temp_general_languages_array);
							}else{
								array_push($general_array,$temp_general_languages_array);	
							}

						}else{
							unset($temp_general_languages_array);
						}
						
						
					}	
			}

		}	

		$content['data'] = array_merge($preferred_languages_array,$general_array);

		if($content){
			return response([
                'status' => true,
                'message' => '',
                'data' => $content,
                'app_string' => __('message.home')
            ]);
        }else{
            return response([
                'status' => false,
                'message' => __('message.no_data_found'),
                'app_string' => __('message.home')
            ]);
        }

	}



	public function kids_view_all(Request $request){
		
		$validator = Validator::make(request()->all(), [
            'genres_id' => 'required',
        ]);

		if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }
        
        $restricted_content_ids = UserRestrictedContent::where('user_id',$request->user()->id)->get()->pluck('content_id')->toArray();
        $user_age_restriction_ids = UserAgeRestriction::where('user_id',$request->user()->id)->get()->pluck('age_rating_id')->toArray();

        $content = Content::with(['genres','ageratings'])
        ->select('id','poster','is_free','age_rating_id')
        ->whereHas('genres', function($q) use($request) {
       		$q->where('genre.id', $request->genres_id);
       		$q->where('genre.status', 1);
		})
		->whereHas('ageratings', function($q) use($request) {
	       		$q->where('age','<=', 14);
		})
		->whereNotIn('id',$restricted_content_ids)
		->whereNotIn('age_rating_id',$user_age_restriction_ids)
        ->where('status','!=',2)
        ->orderBy('content.id','desc')
        ->paginate(16);

		if($content){
			return response([
                'status' => true,
                'message' => '',
                'data' => $content
            ]);
        }else{
            return response([
                'status' => false,
                'message' => __('message.no_data_found')
            ]);
        }

	}


}
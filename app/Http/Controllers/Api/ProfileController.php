<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Devices;
use App\Models\Orders;
use App\Models\User;
use App\Models\PreferredLanguage;
use App\Models\Language;
use App\Traits\ApiResponser;
use App\Models\Notification;
use CommonHelper;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\Api\ChangePasswordRequest;
use App\Http\Resources\UserResource;
use App\Http\Resources\Api\AccountListResource;

class ProfileController extends Controller {

    use ApiResponser;

    public function profile_view_edit(Request $request) {

        
        $validator = Validator::make(request()->all(), [
            'profile_picture' => 'image|mimes:jpeg,png,jpg,heic|max:5120',
            'user_id' => 'required',
            'first_name' => 'min:2|max:30',                  
            'last_name' => 'min:2|max:30',                  
            'is_child' => '',                  
            'phone' => 'min:10|max:11|gt:0',
        ], [
            'profile_picture.image' => 'Accepted file formats: jpg, jpeg, png,heic',
            'profile_picture.max' => 'Maximum file size should be 5 MB',
        ]);

        if (!$validator->fails()) {

            $user = User::where('id', $request->user_id)->first();
            if (isset($user)) {
                if (in_array($user->hasRole(), [4])) {

                    if ($request->hasFile('profile_picture')) {
                        $dir = env('AWS_S3_MODE')."/users";
                        $profile_picture = CommonHelper::s3Upload($request->file('profile_picture'), $dir);

                        //$dir = "images/users";
                        //$profile_picture = CommonHelper::imageUpload($request->profile_picture, $dir);

                        $update_data['profile_picture'] = $profile_picture;

                    }

                    if (isset($request->first_name) && $request->first_name != '') {
                        $update_data['first_name'] = $request->first_name;
                    }

                    if (isset($request->last_name) && $request->last_name != '') {
                        $update_data['last_name'] = $request->last_name;
                    }

                    if (isset($request->phone) && $request->phone != '') {

                        $count = User::where('phone', $request->phone)->where('id', '!=', $user->id)->where('parent_id', '!=', $user->id)->count();

                        if($count > 0){
                            return $this->errorResponse('Phone already in use');
                        }
                        $update_data['phone'] = $request->phone;
                        $update_data['phone_verified'] = 1;
                    }

                    if (isset($request->is_child)) {
                        $update_data['is_child'] = $request->is_child;
                    }

                    if (isset($update_data) && !empty($update_data)) {
                        User::whereId($request->user_id)->update($update_data);
                        $user = User::where('id', $request->user_id)->first();
                    }

                    $accessToken = $user->createToken('authToken')->accessToken;
                    $user->token = $accessToken;

                    return (new UserResource($user))->additional([
                        'status' => true,
                        'message' => 'Profile Updated Successfully'
                    ]);

                } else {
                    return $this->errorResponse(__('message.api.user_not_found'));
                }

            } else {
                return $this->errorResponse(__('message.api.user_not_found'));
            }

        }
        return $this->errorResponse($validator->messages(), true);

    }



    /**

     *  changes the password of the user

     */

    public function change_password(Request $request)
    {

        $validator = Validator::make(request()->all(), [
            'current_password' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        ]);

        if (!$validator->fails()) {

            $get_user = request()->user();

            if($get_user->parent_id == 0){
                $user = request()->user();
            }else{
                $user = User::find($get_user->parent_id);
            }

            if (!(Hash::check($request->get('current_password'), $user->password))) {

                // The passwords dont match


                return response()->json([

                    'status' => false,

                    'message' =>  __('Your current password is incorrect')

                ]);

            }

            $user->password = bcrypt($request->password);

            $user->update();

            return response()->json([

                'status' => true,

                'message' => __('Password changed successfully')

            ]);
        }
        
        return $this->errorResponse($validator->messages(), true);

    }

    public function change_password_otp(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'user_id' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        ]);

        if (!$validator->fails()) {
            if (User::where('id', $request->user_id)->first()){
                
                User::where('id', $request->user_id)->update([
                    'password' => bcrypt($request->password)
                ]);

                return response()->json([
                    'status' => true,
                    'message' => __('Password changed successfully')
                ]);
            } else
            {
                return response()->json([
                    'status' => false,
                    'message' => __('User not found')
                ]);
            }
        }
        return $this->errorResponse($validator->messages(), true);
    }

    public function accounts() {

        $user = User::where('status', 1)->where('id', request()->user()->id)->first();

        if($user->parent_id == 0){
            $users = User::where('status', 1)
                    ->where(function($q) {
                      $q->where('id', request()->user()->id)
                        ->orWhere('parent_id', request()->user()->id);
                    })->get();
        }else{
            $users = User::where('status', 1)
                    ->where(function($q)  use ($user) {
                      $q->where('id', $user->parent_id)
                        ->orWhere('parent_id', $user->parent_id);
                    })->get();
        }

        if (count($users) > 0){

            return AccountListResource::collection($users)->additional([
                'status' => true,
                'message' => ''
            ]);

        }else{
            return $this->errorResponse(__('message.api.user_not_found'));
        }

    }

    public function preferred_languages() {


        $language = Language::all();

        if(isset($language) && count($language) > 0){

            if(!is_null(Auth::user()))
            {
                $user_id = Auth::user()->id;
                $preferred_languages = PreferredLanguage::where('user_id', $user_id)->whereIn('language_id', $language->pluck('id'))->get();

                foreach ($language as $l_key => $l_value) {
                    $language[$l_key]['preferred'] = FALSE;
                    foreach ($preferred_languages as $key => $value) {
                        if($value->language_id == $l_value->id){
                            $language[$l_key]['preferred'] = TRUE;
                        }
                    }
                }
            }else{
                foreach ($language as $key => $value) {
                    $language[$key]['preferred'] = FALSE;
                }
            }

        }

        $response['status'] = TRUE;
        $response['data'] = $language;
        return $response;

    }

    public function add_preferred_languages(Request $request) {

        $validator = Validator::make(request()->all(), [
            'language_id' => 'required'
        ]);

        if($validator->fails()){
           return response([
                'status' => false,
                'message' => $validator->errors()->all(),
            ]);
        }
        
        $language_ids = explode(',', $request->language_id);
        $languages = Language::whereIn('id', $language_ids)->get();

        if(count($languages) == count($language_ids)){
            $user_id = request()->user()->id;
            PreferredLanguage::where('user_id', $user_id)->delete();
            $insert_data = array();
            foreach ($language_ids as $key => $value) {
                $insert_data[] = array(
                    'user_id' => $user_id,
                    'language_id' => $value
                );
            }
            PreferredLanguage::insert($insert_data);
            return response([
                'status' => true,
                'message' => 'Preferred Language updated'
            ]);

        }else{
            return response([
                'status' => false,
                'message' => __('message.api.no_data_found')
            ]);
        }
    }


    public function change_password_(Request $request) {

        $validator = Validator::make(request()->all(), [

            // 'user_id' => 'required',

            'password' => 'required',

        ]);

        if (!$validator->fails()) {

            $user = User::where('id', request()->user()->id)->first();

            if (isset($user)) {

                if (in_array($user->hasRole(), [4])) {

                    $update_data['password'] = Hash::make($request->password);

                    User::whereId(request()->user()->id)->update($update_data);

                    return $this->successResponse($user, __('Password Changed Successfully'));

                } else {

                    return $this->errorResponse(__('User not found'));

                }

            } else {

                return $this->errorResponse(__('User not found'));

            }

        }

        return $this->errorResponse($validator->messages(), true);

    }



    public function children_list(Request $request) {

        $validator = Validator::make(request()->all(), [

            // 'user_id' => 'required',

        ]);

        if (!$validator->fails()) {

            $childrens = User::where('parent_id', request()->user()->id)->where('status', 1)->get();



            if (count($childrens) > 0) {

                $i = 0;

                foreach ($childrens as $value) {

                    $orders = Orders::whereCustomerId($value->id)->whereStatus(1)->first();

                    $childrens[$i]->delete = 1;

                    if (!empty($orders)) {

                        $childrens[$i]->delete = 0;

                    }

                    $i++;

                }

                return $this->successResponse($childrens, __('List of Childrens'));

            } else {

                return $this->errorResponse(__('Children not found'));

            }

        }

        return $this->errorResponse($validator->messages(), true);

    }



    public function delete_children(Request $request) {

        $validator = Validator::make(request()->all(), [

            // 'user_id' => 'required',

        ]);

        if (!$validator->fails()) {

            $children = User::where('id', request()->user()->id)->where('parent_id', '!=', 0)->first();

            if (isset($children)) {

                User::where('id', request()->user()->id)->where('parent_id', '!=', 0)->delete();

                return $this->successResponse([], __('Children Deleted Successfully'));

            } else {

                return $this->errorResponse(__('Children not found'));

            }

        }

        return $this->errorResponse($validator->messages(), true);

    }



    public function logout(Request $request) {

        $validator = Validator::make(request()->all(), [

            'user_id' => 'required',

        ]);

        if (!$validator->fails()) {

            $user = User::where('id', $request->user_id)->first();

            // echo "<pre>";

            // print_r($user->findForPassport($request->user_id));exit;

            if (isset($user)) {

                $devices = Devices::where('user_id', $request->user_id)->update([

                    'name' => '',

                    'token' => '',

                    'type' => '',

                ]);

                // echo "<pre>";

                $user->tokens->each(function ($token, $key) {

                    // print_r($token);

                    // $token->delete();

                });

                // exit;

                return $this->successResponse([], __('Log Out Successfully'));

            } else {

                return $this->errorResponse(__('User not found'));

            }

        }

        return $this->errorResponse($validator->messages(), true);

        $user = request()->user();

        $user->device_token = null;

        $user->update();

        //remove the device details

        $user->currentAccessToken()->delete();

        //revoke current token



        return response()->json([

            'status' =>  true,

            'message' => __('Logged out successfully')

        ]);

    }



    public function notifications(Request $request) {

        $validator = Validator::make(request()->all(), [

            // 'user_id' => 'required',

        ]);

        if (!$validator->fails()) {



            $notifications = Notification::whereUserId($request->user()->id)->orderBy('id','desc')->get()->toArray();

            if(empty($notifications))

            {
                
                return $this->successResponse($notifications, __('No notifications found'));    

            }
           
            return $this->successResponse($notifications, __(''));

        } else {

            return $this->errorResponse(__('User not found'));

        }

    }



    public function notifications_status_change(Request $request) {

        $validator = Validator::make(request()->all(), [
            'status' => 'required',
        ]);

        if (!$validator->fails()) {

            $post['notification'] = $request->status;
            User::whereId($request->user()->id)->update($post);            
            return response([
                'status' => true,
                'message' => __('message.api.notification_status_update'),
            ]);

        } 

        return $this->errorResponse($validator->messages(), true);

    }



    public function notifications_delete(Request $request)

    {

        $validator = Validator::make(request()->all(), [

            // 'user_id' => 'required',

        ]);

        if (!$validator->fails()) {

            Notification::whereUserId(request()->user()->id)->delete();

            return $this->successResponse([], __('Notifications deleted successfully'));

        } else {

            return $this->errorResponse(__('User not found'));

        }

    }



}


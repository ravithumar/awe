<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Language;

class VideoSubtitleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    
    public function toArray($request)
    {   
        $language = Language::find($this->language_id);
        return [
            'id' => $this->id,
            'video_id' => $this->video_id,
            'language_id' => $this->language_id,
            'language' => $language->type,
            'file' => $this->file
        ];
    }
}

<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class VideoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'content_id' => $this->content_id,
            'season_id' => $this->season_id,
            'episode_name' => $this->episode_name,
            'episode_synopsis' => $this->episode_synopsis,
            'video' => $this->video,
            'duration' => $this->duration,
            'thumbnail_image' => !empty($this->thumbnail_image)?env('AWS_S3_URL').'video/'.$this->thumbnail_image:'',
            'video_subtitle'=>VideoSubtitleResource::collection($this->video_subtitle)
        ];
    }
}

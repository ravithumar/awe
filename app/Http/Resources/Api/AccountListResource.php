<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class AccountListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {   

        return [
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'reference_code' => !empty($this->reference_code)?$this->reference_code:'',
            'email' => $this->email,
            'email_verified_at' => !empty($this->email_verified_at)?$this->email_verified_at:'',
            'profile_picture' => $this->profile_picture,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'language' => $this->language,
            'business_name' => $this->business_name,
            'full_name' => $this->full_name,
            'address' => !empty($this->address)?$this->address:'',
            'zipcode' => $this->zipcode,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'user_type' => $this->user_type,
            'social_type' => !empty($this->social_type)?$this->social_type:'',
            'social_id' => !empty($this->social_id)?$this->social_id:'',
            'delivery_type' => $this->delivery_type,
            'phone' => $this->phone,
            'gender' => $this->gender,
            'dob' => !empty($this->dob)?$this->dob:'',
            'department' => $this->department,
            'grade' => $this->grade,
            'school' => $this->school,
            'status' => $this->status,
            'class' => $this->class,
            'is_diet_station' => $this->is_diet_station,
            'is_api' => $this->is_api,
            'notification' => $this->notification,
            'start_time' => !empty($this->start_time)?$this->start_time:'',
            'end_time' => !empty($this->end_time)?$this->end_time:'',
            'phone_verified' => $this->phone_verified,
            'is_child' => $this->is_child,
            'autoplay' => $this->autoplay,
            'autoplay_previews' => $this->autoplay_previews,
            'lock_password' => !empty($this->lock_password)?(string)$this->lock_password:'',
            'plan_id' => !empty($this->plan_id)?$this->plan_id:'',
            'expiration_date' => !empty($this->expiration_date)?$this->expiration_date:'',
            'group' => $this->group,
            'phone_formatted' => $this->phone_formatted,
            'profile_picture_full_url' => $this->profile_picture_full_url,

        ];
    }
}

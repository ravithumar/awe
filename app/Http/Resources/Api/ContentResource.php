<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Video;
use App\Models\MyList;
use App\Models\FavoriteVideo;
use App\Models\ActivityLog;

class ContentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {   
        $total_user_view = ActivityLog::where('content_id',$this->id)->count();
        $total_user_like = FavoriteVideo::where('content_id',$this->id)->where('status', 1)->count();

        if($total_user_like > 0 && $total_user_view > 0){
            $likes_count = round((100*$total_user_like)/$total_user_view);    
        }else{
            $likes_count = 0;
        }
        
        $user_like = FavoriteVideo::where('content_id',$this->id)->where('user_id',request()->user()->id)->first();

        $mylist = MyList::where('content_id',$this->id)->where('user_id',request()->user()->id)->count();

        if(isset($user_like)) {
            if($user_like->status == 1){
                $is_likes = TRUE;
                $is_disliked = FALSE;
            }else{
                $is_likes = FALSE;
                $is_disliked = TRUE;
            }
        }else{
            $is_likes = FALSE;
            $is_disliked = FALSE;
        }

        if($this->content_type != 1){
            $content_seasons = ContentSeasonResource::collection($this->season); 
        }else{
            $content_seasons = array();
        }
        
        return [
            'id' => $this->id,
            'name' => $this->name,
            'is_free' => $this->is_free,
            'age_rating_id' => $this->age_rating_id,
            'age_rating_name' => $this->ageratings->name,
            'age' => $this->ageratings->age,
            'children' => $this->ageratings->children,
            'likes' => $likes_count,
            'logo' => $this->logo,
            'year' => $this->year,
            'poster' => $this->poster,
            'synopsis' => $this->synopsis,
            'trailer' => $this->trailer,
            'content_type' => $this->content_type,
            'status' => $this->status,
            'liked' => $is_likes,
            'disliked' => $is_disliked,
            'my_list' => (isset($mylist) && $mylist > 0)?TRUE:FALSE,
            'content_cast' => ContentCastResource::collection($this->content_cast),
            'content_genres' => ContentGenresResource::collection($this->content_genres),
            'content_tags' => ContentTagsResource::collection($this->content_tags),
            'content_seasons' => $content_seasons,
            'video' => VideoResource::collection($this->episode)
        ];

    }
}

<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\UserAgeRestriction;

class AgeRatingsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {   
        $user_id = request()->user()->id;
        $age_rating_ids = UserAgeRestriction::select('age_rating_id')->where('user_id',$user_id)->get()->pluck('age_rating_id')->toArray();

        if(in_array($this->id, $age_rating_ids)){
            $selected = true;
        }else{
            $selected = false;
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'age' => $this->age,
            'children' => $this->children,
            'selected' => $selected
        ];
    }
}

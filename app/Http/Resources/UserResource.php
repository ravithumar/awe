<?php
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource

{
    /**

     * Transform the resource into an array.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable

     */

    public function toArray($request)

    {

        return [
            'id' => $this->id,
            'full_name' => $this->first_name.' '.$this->last_name,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'phone' => $this->phone,
            'profile_picture_full_url' => $this->profile_picture_full_url,
            'is_child' => $this->is_child,
            'social_id' => isset($this->social_id) ? $this->social_id : '',
            'social_type' => isset($this->social_type) ? $this->social_type : '',
            'autoplay' => $this->autoplay,
            'autoplay_previews' => $this->autoplay_previews,
            'notification' => $this->notification,
            'plan_id' => $this->plan_id,
            'language_id' => $this->language_id,
            'expiration_date' => $this->expiration_date,
            'lock_password' => !empty($this->lock_password)?(string)$this->lock_password:'',
            'phone_verified' => $this->phone_verified,
            'email_verified' => !empty($this->email_verified_at)?$this->email_verified_at:'',
            'reference_code' => !empty($this->reference_code)?$this->reference_code:'',
            'token' => $this->when( $request->path() != 'api/profile/view/edit', $this->token)
        ];

    }

}
<?php

use Illuminate\Support\Facades\Route;

// use App\Http\Controllers\Admin\UserController;
// use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\NotificationController;
use App\Http\Controllers\Admin\SendMailController;
use App\Http\Controllers\Admin\CMSController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */


Route::get('education-outreach', 'Api\DocumentController@education_outreach');
Route::get('about-us', 'Api\DocumentController@about_us');
Route::get('contact-us', 'Api\DocumentController@contact_us');
Route::get('internet-based-ads', 'Api\DocumentController@internet_based_ads');
Route::get('privacy-notice', 'Api\DocumentController@privacy_notice');
Route::get('term-of-service', 'Api\DocumentController@term_of_service');

Route::post('add-channel-id', 'CommonController@add_channel_id');
Route::get('account-activation/{id}', 'CommonController@account_activation');
Route::any('create/password/{id}/{token}', 'CommonController@craete_password')->name('create.password');
Route::get('auth-account-activation/{id}', 'CommonController@auth_account_activation');

Route::post('forgot/password', 'CommonController@forgot_password')->name('forgot_password');
Route::get('store-forgot-password', 'CommonController@store_forgot_password')->name('store.forgot');
Route::get('board-forgot-password', 'CommonController@board_forgot_password')->name('board.forgot');


Route::get('add-channel-id', 'CommonController@add_channel_id');
Route::get('terms-conditions', 'CommonController@terms_conditions');
Route::get('privacy-policy', 'CommonController@privacy_policy');
Route::get('logs/{id}', 'CommonController@activity_log');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth', 'admin']], function () {

    Route::get('profile', 'ProfileController@view')->name('profile');
    Route::post('profile-update/{id}', 'ProfileController@update')->name('profile.update');

    Route::post('change-password/{id}', 'ProfileController@changePassword')->name('change.password');
    
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('users', 'UserController@index')->name('users.index');
    Route::get('users/show/{id}', 'UserController@show')->name('user.show');
    Route::get('users/edit/{id}', 'UserController@edit')->name('user.edit');
    Route::put('users/update/{id}', 'UserController@update')->name('user.update');
    Route::get('users/destroy/{id}', 'UserController@edit')->name('user.destroy');
    Route::post('active_deactive','UserController@active_deactive')->name('active_deactive');

    Route::post('active_deactive_school','SchoolController@active_deactive_school')->name('active_deactive_school');

    Route::post('active_deactive_genre','CategoryController@active_deactive_genre')->name('active_deactive_genre');
      
    Route::resource('category',CategoryController::class);
    Route::resource('season',SeasonController::class);
    Route::resource('ratings',AgeRatingController::class);
    Route::resource('tags',TagController::class);
    Route::resource('artist',ArtistController::class);
    Route::resource('genre',GenreController::class);
    Route::resource('cast',CastTypeController::class);
    Route::resource('plan',PlanController::class);  
    Route::resource('attribute',AttributeController::class);  
    Route::resource('measurement',MeasurementController::class);    
    Route::resource('contactus',ContactUsController::class);    
    Route::resource('issue',IssueController::class);    
    Route::resource('grade',GradeController::class);    
    Route::resource('orders',OrderController::class);  
    Route::resource('content',ContentController::class);    
    Route::resource('video',VideoController::class);    
    Route::resource('faq',FaqController::class);    
    Route::resource('feedback',FeedbackController::class);    
    
    Route::any('adv_content_change', 'ContentController@adv_content_change')->name('adv_content_change');
    Route::any('child_adv_content_change', 'ContentController@child_adv_content_change')->name('child_adv_content_change');
    Route::get('get_season_data', 'VideoController@get_season_data')->name('get_season_data');   
    Route::post('trailer_file_upload', 'ContentController@trailer_file_upload')->name('trailer_file_upload');   
    Route::post('chunk_file_upload', 'VideoController@chunk_file_upload')->name('chunk_file_upload');   
    Route::post('active_deactive_faq','FaqController@active_deactive_faq')->name('active_deactive_faq');

    Route::get('plan/manageattr/{id}', 'PlanController@manageattr')->name('plan.manageattr');   
    Route::post('plan/attr_update/{id}', 'PlanController@attr_update')->name('plan.attr_update');   
    
    Route::post('product/addstore/{id}', 'ProductController@addstore')->name('product.addstore');   
    Route::get('remove_product_store/{id}', 'ProductController@remove_product_store')->name('remove_product_store');   
    Route::post('save_product_store', 'ProductController@save_product_store')->name('save_product_store');   
    Route::post('active_deactive_product','ProductController@active_deactive_product')->name('active_deactive_product');
    Route::post('get_image','ProductController@get_image')->name('get_image');

    Route::post('order_notification_customer','OrderController@order_notification_customer')->name('order_notification_customer');
    
    Route::get('reported/issue','IssueController@reported_issue_index')->name('reported.issue');
    Route::get('bulk/notification',[NotificationController::class,'index'])->name('bulk.index');
    Route::post('bulk/notification',[NotificationController::class,'store'])->name('bulk.store');
    
    Route::get('system/config','SystemConfigController@index')->name('system.config');
    Route::post('system/config/submit','SystemConfigController@update')->name('system.config.submit');
    Route::post('system/config/backup','SystemConfigController@update')->name('system.config.backup');
    Route::post('system/config/download','SystemConfigController@update')->name('system.config.download');
    Route::post('system/config/delete_backup','SystemConfigController@update')->name('system.config.delete_backup');
    
    Route::get('app/config','SystemConfigController@app')->name('app.config');
    Route::post('app/config/submit','SystemConfigController@submit_app')->name('app.config.submit');

    Route::get('report/all_report','ReportController@index')->name('report.all');

    Route::get('order/all_orders','OrderController@all_orders')->name('order.all');

    Route::get('order/create_order','OrderController@create_order')->name('order.create');

    Route::post('order/get_user','OrderController@get_user')->name('order.get_user');
    Route::post('order/get_meal','OrderController@get_meal')->name('order.get_meal');
    Route::post('order/get_meal_details','OrderController@get_meal_details')->name('order.get_meal_details');
    Route::post('order/book_meal','OrderController@book_meal')->name('order.book_meal');

    Route::get('order/refund','OrderController@refund')->name('order.refund');
    Route::get('order/all_orders/view/{id}','OrderController@all_orders_view')->name('order.all.view');
    Route::post('refund','OrderController@refund_test')->name('refund');

    Route::any('send-mail-customer',[SendMailController::class,'customer'])->name('mail.customer');
    Route::any('send-notification-customer',[SendMailController::class,'customer_notification'])->name('notification.customer');

    Route::any('terms-conditions',[CMSController::class,'terms_conditions'])->name('terms_conditions');
    Route::any('privacy-policy',[CMSController::class,'privacy_policy'])->name('privacy_policy');
});
Route::group(['namespace' => 'Board', 'prefix' => 'board', 'as' => 'board.', 'middleware' => ['auth', 'board']], function () {
    
    Route::get('profile', 'ProfileController@view')->name('profile');
    Route::post('profile-update/{id}', 'ProfileController@update')->name('profile.update');
    
    Route::post('change-password/{id}', 'ProfileController@changePassword')->name('change.password');
    
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    Route::post('active_deactive_category','CategoryController@active_deactive_category')->name('active_deactive_category');
    Route::resource('category', CategoryController::class);
    Route::resource('store', StoreController::class);
    Route::resource('banner', BannerController::class);
    Route::resource('product', ProductController::class);
    Route::resource('orders',OrderController::class);
    Route::resource('meal', MealController::class);
    Route::post('order_notification_customer','OrderController@order_notification_customer')->name('order_notification_customer');

    Route::get('report/all_report','ReportController@all_report')->name('report.all');
    
    Route::get('order/all','OrderController@all')->name('order.all');
    Route::get('order/all/view/{id}','OrderController@all_orders_view')->name('order.all.view');

    Route::get('order/inprocess','OrderController@inprocess')->name('order.inprocess');
    Route::get('order/inprocess/view/{id}','OrderController@inprocess_view')->name('order.inprocess.view');
    Route::get('order/inprocess/edit/{id}','OrderController@inprocess_edit')->name('order.inprocess.edit');
    Route::post('order/inprocess/item/update','OrderController@item_update')->name('order.item.update');
    Route::post('order/inprocess/date/update','OrderController@date_update')->name('order.date.update');

     Route::post('active_deactive_product','ProductController@active_deactive_product')->name('active_deactive_product');
     Route::post('active_deactive_meal','MealController@active_deactive_meal')->name('active_deactive_meal');

    Route::post('pending_order','OrderController@pending_order')->name('pending_order');

    Route::get('order/completed','OrderController@completed')->name('order.completed');
    Route::get('order/completed/view/{id}','OrderController@completed_view')->name('order.completed.view');

    Route::get('order/fail','OrderController@fail')->name('order.fail');
    Route::get('order/fail/view/{id}','OrderController@fail_view')->name('order.fail.view');


});

require __DIR__ . '/auth.php';

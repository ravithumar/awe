<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\ProfileController;
use App\Http\Controllers\Api\PlanController;
use App\Http\Controllers\Api\FavoriteVideoController;
use App\Http\Controllers\Api\ContentController;
use App\Http\Controllers\Api\MyListController;
use App\Http\Controllers\Api\FaqController;
use App\Http\Controllers\Api\FeedbackController;
use App\Http\Controllers\Api\RatingController;
use App\Http\Controllers\Api\UserAgeRestrictionController;
use App\Http\Controllers\Api\PlayBackController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\NotificationController;
use App\Http\Controllers\Api\ActivityController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*

|--------------------------------------------------------------------------

| API Routes

|--------------------------------------------------------------------------

|

| Here is where you can register API routes for your application. These

| routes are loaded by the RouteServiceProvider within a group which

| is assigned the "api" middleware group. Enjoy building your API!

|

 */

Route::get('/init/{type?}/{app_version?}', [AuthController::class, 'init'])->middleware('localization');

Route::post('/login', [AuthController::class, 'login'])->middleware('localization');

Route::post('/register', [AuthController::class, 'register'])->middleware('localization');

Route::post('/change_password_otp', [ProfileController::class, 'change_password_otp'])->middleware('localization');

Route::post('/forgot_password', [AuthController::class, 'forgot_password'])->middleware('localization');

Route::post('social-login', [LoginController::class, 'socialLogin'])->middleware('localization');
Route::get('/appstring/main', [LoginController::class, 'main_screens_strings'])->middleware('localization');

Route::get('apple-details/{id}', [LoginController::class, 'appleDetails'])->middleware('localization');

Route::post('/test_upload', [AuthController::class, 'test_upload']);

Route::post('/check_email', [AuthController::class, 'check_email']);


Route::post('payment', 'Api\PaymentController@payment');

Route::get('payment/success', 'Api\PaymentController@success');

Route::get('payment/fail', 'Api\PaymentController@fail');

Route::get('payment/success_test', 'Api\PaymentController@success_test');

Route::middleware('auth:api')->get('/user', function (Request $request) {

    return $request->user();

});


Route::group(['middleware' => ['auth:api','localization']], function () {

    Route::get('/user', function (Request $request) {

        return $request->user();

    });

    Route::get('/plan', [PlanController::class, 'plan']);
    Route::get('/accounts', [ProfileController::class, 'accounts']);
    
    Route::post('/preferred/languages/update', [ProfileController::class, 'add_preferred_languages']);

    Route::post('/activity/add', [ActivityController::class, 'add']);
    Route::get('/notification/list', [NotificationController::class, 'index']);
    Route::get('/content/kids', [ContentController::class, 'kids']);
    Route::post('/content/kids/view/all', [ContentController::class, 'kids_view_all']);
    Route::post('/subscription/plan', [UserController::class, 'subscription_plan']);
    Route::post('/disable/pin', [UserController::class, 'disable_pin']);
    Route::post('/change/pin', [UserController::class, 'change_pin']);
    Route::post('/login/activity', [UserController::class, 'login_activity']);
    Route::get('/search/popular', [UserAgeRestrictionController::class, 'popular_search']);

    Route::post('/language/add', [UserController::class, 'save_language']);

    Route::get('/user/devices', [UserController::class, 'register_device_list']);
    Route::post('/user/device/delete', [UserController::class, 'deregister_user']);
    Route::post('/account/add', [UserController::class, 'add']);
    Route::post('/account/remove', [UserController::class, 'remove']);
    Route::post('/generate/token', [UserController::class, 'generate_passport_token']);
    Route::post('/verify/pin', [UserController::class, 'verify_lock']);
    Route::post('/add/pin', [UserController::class, 'add_lock']);
    Route::post('/category/add', [CategoryController::class, 'add']);
    Route::get('/category/list', [CategoryController::class, 'index']);
    Route::post('/playback/change', [PlayBackController::class, 'status_update']);
    Route::post('/useragerestriction/add', [UserAgeRestrictionController::class, 'add']);
    Route::get('/useragerestriction/list', [UserAgeRestrictionController::class, 'index']);
    Route::post('/useragerestriction/search', [UserAgeRestrictionController::class, 'search']);
    Route::post('/rating/add', [RatingController::class, 'add']);
    Route::post('/feedback/add', [FeedbackController::class, 'add']);
    Route::get('/faq', [FaqController::class, 'index']);
    Route::get('/mylist/list', [MyListController::class, 'index']);
    Route::post('/mylist/add', [MyListController::class, 'status_update']);
    Route::post('/favorite/like', [FavoriteVideoController::class, 'like']);
    Route::post('/favorite/dislike', [FavoriteVideoController::class, 'dislike']);
    Route::post('/content', [ContentController::class, 'index']);
    Route::get('/content/details/{id}', [ContentController::class, 'details']);
    Route::post('/content/view/all', [ContentController::class, 'view_all']);
    Route::post('/category/content', [ContentController::class, 'category_vise_content']);
    Route::post('/change_password', [ProfileController::class, 'change_password']);   
    

    Route::post('/profile/view/edit', [ProfileController::class, 'profile_view_edit']);

    Route::post('/delete_children', [ProfileController::class, 'delete_children']);

    Route::post('/notifications', [ProfileController::class, 'notifications']);

    Route::post('/notifications/update', [ProfileController::class, 'notifications_status_change']); 

    Route::post('/notifications/delete', [ProfileController::class, 'notifications_delete']);


});

Route::post('/send_otp', [AuthController::class, 'send_otp']);
Route::post('/send_otp_email', [AuthController::class, 'send_otp_email']);

Route::get('/preferred/languages', [ProfileController::class, 'preferred_languages']);
Route::post('/logout', [ProfileController::class, 'logout']);

Route::get('send_push_notification/{id}', 'Api\TestingController@push_notification');
Route::post('test_update_order_status', 'Api\TestingController@test_update_order_status');
Route::get('send_web_push/{id}', 'Api\TestingController@send_web_push');
